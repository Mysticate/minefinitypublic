package me.mysticate.minefinity.hub;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.world.WorldHandler;

public class HubWorldHandler extends WorldHandler
{
	public HubWorldHandler(JavaPlugin plugin, World world)
	{
		super(plugin, world);

		BlockBreak = false;
		BlockPlace = false;
		BlockBurn = false;
		BlockChangeByEntity = false;
		BlockDamage = false;
		BlockIgnite = false;
		BlockFade = false;

		BucketEmpty = false;
		BucketFill = false;

		EntityDamage = false;
		EntityDamageByEntity = false;

		InventoryOpen = false;

		LiquidFlow = false;

		PaintingBreak = false;

		HungerChange = false;
		SaturationChange = false;

		ItemDrop = false;

		EntitySpawn = false;
	}

	@Override
	public void onJoin(Player player)
	{

	}

	@Override
	public void onQuit(Player player)
	{

	}

	@Override
	public Location getJoinLocation(Player player)
	{
		return getWorld().getSpawnLocation().clone().add(.5, 4, .5);
	}
}
