package me.mysticate.minefinity.hub;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.backpack.AquiredItem;
import me.mysticate.minefinity.core.chat.ChatEvent;
import me.mysticate.minefinity.core.common.misc.ArmorType;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.sector.Sector;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.core.sector.SectorWorld;
import me.mysticate.minefinity.core.settings.SettingPool;
import me.mysticate.minefinity.core.settings.SettingsManager;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.hub.gui.cosmetics.GuiLobbyCosmetics;

public class HubCosmeticManager extends Manager
{
	private HubManager _hubManager;

	private SectorManager _sectorManager;
	private SettingsManager _settingManager;
	
	private ChatColor[] _colors = new ChatColor[] { ChatColor.YELLOW, ChatColor.DARK_RED, ChatColor.RED, ChatColor.GOLD,
			ChatColor.GREEN, ChatColor.DARK_GREEN, ChatColor.AQUA, ChatColor.DARK_AQUA, };

	public HubCosmeticManager(JavaPlugin plugin, HubManager hubManager, SectorManager sectorManager,
			SettingsManager settingManager)
	{
		super(plugin, "Hub Cosmetics");

		_hubManager = hubManager;

		_sectorManager = sectorManager;
		_settingManager = settingManager;
	}

	public HubManager getHubManager()
	{
		return _hubManager;
	}

	@EventHandler
	public void onSecond(TimeEvent event)
	{
		if (event.getType() == UpdateType.SEC_01)
			return;

		for (SectorWorld world : _sectorManager.getWorlds(Sector.HUB))
		{
			for (Player player : world.getWorld().getPlayers())
			{
				String suffix = getSuffix(player);
				if (suffix == null || suffix.trim().equalsIgnoreCase(""))
				{
					HudManager.get(player).setSuffix("");
				} else
				{
					HudManager.get(player).setSuffix(C.purpleBold + " " + getSuffix(player));
				}
			}
		}
	}

	@EventHandler
	public void onChat(ChatEvent event)
	{
		if (_sectorManager.getSector(event.getPlayer().getWorld()) != Sector.HUB)
			return;

		ChatColor color = getColor(event.getPlayer());
		if (color == null)
			return;

		event.setNameColor(color);
	}

	public void setSuffix(Player player, String suffix)
	{
		_settingManager.getPlayer(player).set(SettingPool.HUB, "suffix", suffix);
	}

	public String getSuffix(Player player)
	{
		if (!_settingManager.getPlayer(player).hasSetting(SettingPool.HUB, "suffix"))
			return null;

		return _settingManager.getPlayer(player).getSetting(SettingPool.HUB, "suffix");
	}

	public void setNameColor(Player player, int id)
	{
		if (id < 0 || id >= _colors.length)
			return;

		_settingManager.getPlayer(player).set(SettingPool.HUB, "color", id + "");
	}

	public ChatColor getColor(Player player)
	{
		if (!_settingManager.getPlayer(player).hasSetting(SettingPool.HUB, "color"))
			return ChatColor.YELLOW;

		int id = -1;
		try
		{
			id = Integer.valueOf(_settingManager.getPlayer(player).getSetting(SettingPool.HUB, "color"));
		} catch (Exception ex)
		{
			return ChatColor.YELLOW;
		}

		if (id < 0 || id >= _colors.length)
			return ChatColor.YELLOW;

		return _colors[id];
	}

	public ChatColor[] getColors()
	{
		return _colors;
	}
	
	public void resetArmor(Player player, ArmorType armor)
	{
		_settingManager.getPlayer(player).set(SettingPool.HUB, armor.toString(), null);
		updateArmor(player);
		
		new GuiLobbyCosmetics(getPlugin(), player, _hubManager, _hubManager.getPerkManager(), _hubManager.getRankManager()).openInventory();
	}
	
	public void setArmor(Player player, ArmorType armor, AquiredItem item)
	{
		_settingManager.getPlayer(player).set(SettingPool.HUB, armor.toString(), item.getUUID().toString());
		updateArmor(player);
		
		new GuiLobbyCosmetics(getPlugin(), player, _hubManager, _hubManager.getPerkManager(), _hubManager.getRankManager()).openInventory();
	}
	
	public AquiredItem getArmor(Player player, ArmorType type)
	{
		try
		{ 
			String setting = _settingManager.getPlayer(player).getSetting(SettingPool.HUB, type.toString());
			
			if (setting == null)
				return null;
			
			UUID uuid = UUID.fromString(setting);
			
			if (uuid == null)
				return null;
			
			if (!_hubManager.getBackpackManager().hasUUID(uuid))
				return null;
			
			return _hubManager.getBackpackManager().getPlayer(player).getItem(uuid);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}		
	}
	
	public void updateArmor(Player player)
	{
		if (_sectorManager.getSector(player.getWorld()) != Sector.HUB)
			return;
		
		ItemStack[] items = new ItemStack[4];
		for (ArmorType type : ArmorType.values())
		{
			AquiredItem armor = getArmor(player, type);
			if (armor == null)
				continue;
			
			items[type.getArmorIndex()] = UtilItemStack.buildAquiredItem(armor);
		}
		player.getInventory().setArmorContents(items);
	}
}
