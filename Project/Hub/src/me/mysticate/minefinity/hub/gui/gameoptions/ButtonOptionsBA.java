package me.mysticate.minefinity.hub.gui.gameoptions;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.game.GameRunner;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;

public class ButtonOptionsBA implements Button
{
	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.IRON_CHESTPLATE, (byte) 0, 1, C.yellowBold + "Battle Arena",
				new String[] { " ", C.gray + "Click to access the", C.gray + "configuration page.", " ",
						C.green + "In this menu", C.gray + "- " + C.white + "Change kit selection",
						C.gray + "- " + C.white + "Upgrade kits", C.gray + "- " + C.white + "Purchase item renames",
						C.gray + "- " + C.white + "Purchase items", });
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		GameRunner.openGameOptions(player, "BA");
	}
}
