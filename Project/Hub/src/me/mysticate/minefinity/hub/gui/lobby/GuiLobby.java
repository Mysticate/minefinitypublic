package me.mysticate.minefinity.hub.gui.lobby;

import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.sector.Sector;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.core.sector.SectorWorld;

public class GuiLobby extends Gui
{
	private SectorManager _sectorManager;

	public GuiLobby(JavaPlugin plugin, SectorManager sectorManager, Player player)
	{
		super(plugin, player, "Lobby Select", 27);

		_sectorManager = sectorManager;
	}

	@EventHandler
	public void onChange(PlayerChangedWorldEvent event)
	{
		updateInventory();
	}

	@Override
	protected void addItems()
	{
		List<SectorWorld> worlds = _sectorManager.getWorlds(Sector.HUB);
		Collections.sort(worlds);

		borders(true);

		for (int i = 0; i < worlds.size(); i++)
		{
			final int slot = i;
			Bukkit.getScheduler().runTaskLater(getPlugin(), new Runnable()
			{
				@Override
				public void run()
				{
					addItem(new ButtonJoinLobby(worlds.get(slot)));
					updateInventory();
				}
			}, i * 5);
		}

	}

	@Override
	protected void onOpen()
	{

	}

	@Override
	protected void onClose()
	{

	}
}
