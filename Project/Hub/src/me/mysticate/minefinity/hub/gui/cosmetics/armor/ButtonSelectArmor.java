package me.mysticate.minefinity.hub.gui.cosmetics.armor;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.backpack.AquiredItem;
import me.mysticate.minefinity.core.backpack.ItemPool;
import me.mysticate.minefinity.core.common.misc.ArmorType;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.hub.HubCosmeticManager;
import me.mysticate.minefinity.hub.HubManager;

public class ButtonSelectArmor implements Button
{
	private HubManager _hubManager;
	private PerkManager _perkManager;
	private RankManager _rankManager;
	private HubCosmeticManager _manager;
	
	private ArmorType _part;
	
	public ButtonSelectArmor(HubManager hubManager, PerkManager perkManager, RankManager rankManager, HubCosmeticManager manager, ArmorType part)
	{
		_hubManager = hubManager;
		_perkManager = perkManager;
		_rankManager = rankManager;
		_manager = manager;
		
		_part = part;
	}
	
	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		AquiredItem item = _manager.getArmor(player, _part);
		
		ItemStack stack = item == null ? UtilItemStack.create(Material.STAINED_CLAY, (byte) 14, 1, "") : UtilItemStack.buildAquiredItem(item);
		stack = UtilItemStack.setLore(stack, new String[] 
				{
						" ",
						C.gray + "Equipped " + C.white + (_manager.getArmor(player, _part) == null ? "None" : C.yellow + ChatColor.translateAlternateColorCodes('&', item.getDisplayName())),
						" ", 
						C.gray + "Select your armor that", C.gray + "will be displayed in hubs.", 
						" ",
						C.white + "Lobby armor is housed in", C.white + "your backpack, and can be",
						C.white + "found in game loot, or purchased", C.white + "online." 
				});
		stack = UtilItemStack.setName(stack, C.yellowBold + _part.toString().toUpperCase());

		return stack;
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		new GuiSelectArmorPiece(MinefinityPlugin.getPlugin(), _hubManager, _perkManager, _rankManager, _manager, new ArrayList<AquiredItem>(IM.backpack().getPlayer(player).getItems(_part.toString(), ItemPool.HUB)), _part, 1, player).openInventory();
	}
}
