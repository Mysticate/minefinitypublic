package me.mysticate.minefinity.hub.gui.gameselect;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.gui.Gui;

public class GuiGameSelect extends Gui
{
	public GuiGameSelect(JavaPlugin plugin, Player player)
	{
		super(plugin, player, "Game Select", 27);
	}

	@Override
	protected void addItems()
	{
		setItem(13, new ButtonSelectBA());
		borders(true);
	}

	@Override
	protected void onOpen()
	{

	}

	@Override
	protected void onClose()
	{

	}
}
