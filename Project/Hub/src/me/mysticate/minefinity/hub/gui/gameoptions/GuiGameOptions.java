package me.mysticate.minefinity.hub.gui.gameoptions;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.gui.Gui;

public class GuiGameOptions extends Gui
{
	public GuiGameOptions(JavaPlugin plugin, Player player)
	{
		super(plugin, player, "Game Options", 27);
	}

	@Override
	protected void addItems()
	{
		setItem(13, new ButtonOptionsBA());
		borders(true);
	}

	@Override
	protected void onOpen()
	{

	}

	@Override
	protected void onClose()
	{

	}
}