package me.mysticate.minefinity.hub.gui.gameselect;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.game.GameRunner;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;

public class ButtonSelectBA implements Button
{
	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.IRON_CHESTPLATE, (byte) 0, 1, C.yellowBold + "Battle Arena",
				new String[] { " ", C.gray + "Fight to the death in this", C.gray + "solo deathmatch. The person",
						C.gray + "with most kills after 10 minutes", C.gray + "wins! Upgrade or select your kit",
						C.gray + "in the game options menu.", " ", C.green + "Click to view lobbies" });
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		// player.closeInventory();
		GameRunner.openGameJoiner(player, "BA");
	}
}
