package me.mysticate.minefinity.hub.gui.cosmetics;

import java.util.LinkedList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.chat.ChatManager;
import me.mysticate.minefinity.core.chat.reader.ChatReader;
import me.mysticate.minefinity.core.chat.reader.ChatReaderResponse;
import me.mysticate.minefinity.core.chat.reader.IChatReaderCallback;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.common.util.UtilText;
import me.mysticate.minefinity.core.currency.CurrencyManager;
import me.mysticate.minefinity.core.currency.CurrencyPlayer;
import me.mysticate.minefinity.core.currency.CurrencyType;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.confirmation.ConfirmationPage;
import me.mysticate.minefinity.core.gui.confirmation.ConfirmationResponse;
import me.mysticate.minefinity.core.gui.confirmation.IConfirmationCallback;
import me.mysticate.minefinity.core.shop.Price;
import me.mysticate.minefinity.hub.HubCosmeticManager;

public class ButtonSelectSuffix implements Button
{
	private HubCosmeticManager _manager;
	private CurrencyManager _currencyManager;
	private ChatManager _chatManager;

	public ButtonSelectSuffix(HubCosmeticManager manager, CurrencyManager currencyManager, ChatManager chatManager)
	{
		_manager = manager;
		_currencyManager = currencyManager;
		_chatManager = chatManager;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		CurrencyPlayer currencyPlayer = _currencyManager.getPlayer(player);

		String suffix = _manager.getSuffix(player);
		if (suffix == null)
			suffix = C.white + "None";

		LinkedList<String> lore = new LinkedList<String>();

		lore.add(" ");
		lore.add(C.gray + "Current " + C.yellow + suffix);
		lore.add(" ");
		lore.add(C.gray + "Please refrain from crude");
		lore.add(C.gray + "or vulgar suffixes.");
		lore.add(" ");

		for (String string : F.cost("Hub Suffix", 0, CurrencyType.GEMS, Price.HUB_SUFFIX_CHANGE,
				currencyPlayer.getCurrency(CurrencyType.GEMS)))
			lore.add(string);

		return UtilItemStack.create(Material.BOOK, (byte) 0, 1, C.yellowBold + "Hub Suffix",
				lore.toArray(new String[0]));
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		CurrencyPlayer currencyPlayer = _currencyManager.getPlayer(player);

		if (currencyPlayer.canAfford(CurrencyType.GEMS, Price.HUB_SUFFIX_CHANGE))
		{
			new ConfirmationPage(gui.getPlugin(), player, new IConfirmationCallback()
			{
				@Override
				public void run(ConfirmationResponse response)
				{
					if (response == ConfirmationResponse.CONFIRM)
					{
						player.closeInventory();

						if (!_chatManager.addChatReader(new ChatReader(player, 20000, new IChatReaderCallback()
						{
							@Override
							public void onRead(ChatReaderResponse response, String message)
							{
								if (response == ChatReaderResponse.SUCCES)
								{
									message.trim();

									if (!UtilText.validate(message, 1, 12, true))
									{
										player.sendMessage(F.manager("Shop", "Invalid name! Please try again."));
									} else
									{
										if (currencyPlayer.removeCurrency(CurrencyType.GEMS, Price.HUB_SUFFIX_CHANGE))
										{
											_manager.setSuffix(player, message);
											gui.openInventory();
										} else
										{
											player.sendMessage(
													F.manager("Shop", "You don't have enough money to purchase that!"));
										}
									}
								}
							}
						}), "What do you want your suffix to be?"))
						{
							player.sendMessage(
									F.manager("Shop", "Failed to open a new chat reader. Please type \"cancel.\""));
						}
					} else
					{
						gui.openInventory();
					}
				}
			}, "Suffix Change", Price.HUB_SUFFIX_CHANGE + " " + CurrencyType.GEMS.getName(), false).openInventory();
		}
	}
}
