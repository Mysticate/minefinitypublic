package me.mysticate.minefinity.hub.gui.cosmetics.color;

import java.util.LinkedList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilColor;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.hub.HubCosmeticManager;

public class ButtonSelectNameColor implements Button
{
	private HubCosmeticManager _hubCosmeticManager;
	private PerkManager _perkManager;
	private RankManager _rankManager;

	private int _id;
	private ChatColor _color;

	public ButtonSelectNameColor(HubCosmeticManager hubCosmeticManager, PerkManager perkManager,
			RankManager rankManager, int id, ChatColor color)
	{
		_hubCosmeticManager = hubCosmeticManager;
		_perkManager = perkManager;
		_rankManager = rankManager;

		_id = id;
		_color = color;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		boolean selected = _color == _hubCosmeticManager.getColor(player);
		boolean has = (_color == ChatColor.YELLOW || _rankManager.isOwned(player, Rank.CHAMPION)
				|| _perkManager.getPlayer(player).hasPerk(F.perk("hub", "color", "" + _id)));

		LinkedList<String> lore = new LinkedList<String>();

		lore.add(has ? (C.green + "Unlocked") : (C.red + "Locked"));
		lore.add(" ");
		lore.add(C.gray + "Color " + _color + "[+]");
		lore.add(" ");
		lore.add(C.gray + "Select the color your name");
		lore.add(C.gray + "will show as whenever you");
		lore.add(C.gray + "talk in chat.");
		lore.add(" ");

		if (has)
		{
			if (selected)
			{
				lore.add(C.dGreenBold + "Selected");
			} else
			{
				lore.add(C.greenBold + "Click to Select");
			}
		} else
		{
			lore.add(C.redBold + "Purchase " + Rank.CHAMPION.buildDisplay(true, true) + C.redBold + " Online");
			lore.add(C.redBold + "Find in Game Loot");
		}

		ItemStack item = UtilItemStack.create(_color == ChatColor.YELLOW ? Material.BARRIER : Material.BANNER,
				(byte) (_color == ChatColor.YELLOW ? 0 : UtilColor.getBannerData(_color)), 1,
				C.yellowBold + "Hub Name Color", lore.toArray(new String[0]));

		if (selected && _color != ChatColor.YELLOW)
			item.addEnchantment(UtilItemStack.getGlow(), 1);

		return item;
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		boolean selected = _color == _hubCosmeticManager.getColor(player);
		boolean has = (_color == ChatColor.YELLOW || _rankManager.isOwned(player, Rank.CHAMPION)
				|| _perkManager.getPlayer(player).hasPerk(F.perk("hub", "color", "" + _id)));

		if (selected || !has)
			return;

		_hubCosmeticManager.setNameColor(player, _id);
		gui.updateInventory();
	}
}
