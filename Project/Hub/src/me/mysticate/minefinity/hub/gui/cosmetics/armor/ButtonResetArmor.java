package me.mysticate.minefinity.hub.gui.cosmetics.armor;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.misc.ArmorType;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.hub.HubCosmeticManager;

public class ButtonResetArmor implements Button
{
	private HubCosmeticManager _manager;
	private ArmorType _armor;
	
	public ButtonResetArmor(HubCosmeticManager manager, ArmorType armor)
	{
		_manager = manager;
		_armor = armor;
	}
	
	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.BARRIER, (byte) 0, 1, C.redBold + "\u2717");
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		_manager.resetArmor(player, _armor);
	}
}
