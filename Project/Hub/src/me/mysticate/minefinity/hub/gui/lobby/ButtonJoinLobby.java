package me.mysticate.minefinity.hub.gui.lobby;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.sector.SectorWorld;

public class ButtonJoinLobby implements Button
{
	private SectorWorld _world;

	public ButtonJoinLobby(SectorWorld world)
	{
		_world = world;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		boolean current = player.getWorld() == _world.getWorld();

		return UtilItemStack
				.create(current ? Material.BIRCH_DOOR_ITEM : Material.DARK_OAK_DOOR_ITEM, (byte) 0, _world.getId(),
						C.yellowBold + "Hub " + _world.getId(),
						new String[] { " ",
								C.gray + "Current Players: " + C.green + _world.getWorld().getPlayers().size(), " ",
								current ? (C.green + "You are here") : (C.red + "Click to join") });
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		if (_world.getWorld() == player.getWorld())
			return;

		_world.join(player);
	}
}
