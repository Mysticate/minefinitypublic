package me.mysticate.minefinity.hub.gui.cosmetics;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;

public class ButtonSelectGadget implements Button
{
	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.FIREWORK, (byte) 0, 1, C.yellowBold + "Gadgets",
				new String[] { C.red + "0" + C.white + "/" + C.aqua + "0" + C.green + " Owned", " ",
						C.gray + "Fizz, bang, pop!", C.gray + "Have fun in lobbies by",
						C.gray + "using these fun gadgets.", C.gray + "Or be annoying. Whichever.", " ",
						C.dRedBold + "COMING SOON" });
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{

	}
}
