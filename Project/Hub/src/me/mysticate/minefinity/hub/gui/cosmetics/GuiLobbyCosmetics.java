package me.mysticate.minefinity.hub.gui.cosmetics;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.common.misc.ArmorType;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.buttons.LinkButton;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.hub.HubManager;
import me.mysticate.minefinity.hub.gui.cosmetics.armor.ButtonSelectArmor;

public class GuiLobbyCosmetics extends Gui
{
	private HubManager _manager;
	private PerkManager _perkManager;
	private RankManager _rankManager;

	public GuiLobbyCosmetics(JavaPlugin plugin, Player player, HubManager manager, PerkManager perkManager,
			RankManager rankManager)
	{
		super(plugin, player, "Cosmetics", 54);

		_manager = manager;
		_perkManager = perkManager;
		_rankManager = rankManager;
	}

	@Override
	protected void addItems()
	{
		setItem(10, new ButtonSelectArmor(_manager, _perkManager, _rankManager, _manager.getHubCosmeticManager(), ArmorType.HELMET));
		setItem(19, new ButtonSelectArmor(_manager, _perkManager, _rankManager, _manager.getHubCosmeticManager(), ArmorType.CHESTPLATE));
		setItem(28, new ButtonSelectArmor(_manager, _perkManager, _rankManager, _manager.getHubCosmeticManager(), ArmorType.LEGGINGS));
		setItem(37, new ButtonSelectArmor(_manager, _perkManager, _rankManager, _manager.getHubCosmeticManager(), ArmorType.BOOTS));

		setItem(21, new ButtonSelectGadget());
		setItem(22, new ButtonSelectHalo());
		setItem(23, new ButtonSelectNameColor(_manager.getHubCosmeticManager(), _perkManager, _rankManager));
		setItem(24, new ButtonSelectSuffix(_manager.getHubCosmeticManager(), _manager.getCurrencyManager(),
				_manager.getChatManager()));

		setItem(40,
				new LinkButton(
						UtilItemStack.create(Material.BOOKSHELF, (byte) 0, 1, C.yellowBold + "Shop",
								new String[] { " ", C.gray + "Click to enter the online shop.",
										C.gray + "There, you can purchase ranks,", C.gray + "items, gadgets, and more!",
										" ", C.blue + C.underline + MinefinityPlugin.getWebsite() + "/shop" }),
				MinefinityPlugin.getWebsite() + "/shop"));

		borders(true);
	}

	@Override
	protected void onOpen()
	{

	}

	@Override
	protected void onClose()
	{

	}
}
