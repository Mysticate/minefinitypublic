package me.mysticate.minefinity.hub;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.backpack.BackpackManager;
import me.mysticate.minefinity.core.chat.ChatManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.cooldown.CooldownPool;
import me.mysticate.minefinity.core.currency.CurrencyManager;
import me.mysticate.minefinity.core.currency.CurrencyType;
import me.mysticate.minefinity.core.game.RequestPlayerOpenGameMenuEvent;
import me.mysticate.minefinity.core.game.RequestPlayerOpenOptionsMenuEvent;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.item.ItemManager;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.core.sector.ISectorHandler;
import me.mysticate.minefinity.core.sector.Sector;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.core.sector.SectorWorld;
import me.mysticate.minefinity.core.settings.SettingsManager;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.core.world.WorldManager;
import me.mysticate.minefinity.hub.gui.gameoptions.GuiGameOptions;
import me.mysticate.minefinity.hub.gui.gameselect.GuiGameSelect;
import me.mysticate.minefinity.hub.items.ItemFly;
import me.mysticate.minefinity.hub.items.ItemGameOptions;
import me.mysticate.minefinity.hub.items.ItemGameShop;
import me.mysticate.minefinity.hub.items.ItemLobbyCosmetics;
import me.mysticate.minefinity.hub.items.ItemLobbySelect;

public class HubManager extends Manager implements ISectorHandler
{
	private SectorManager _sectorManager;
	private ItemManager _itemManager;
	private WorldManager _worldManager;

	private RankManager _rankManager;
	private CurrencyManager _currencyManager;
	private SettingsManager _settingManager;
	private ChatManager _chatManager;
	private PerkManager _perkManager;
	private BackpackManager _backpackManager;

	private HubCosmeticManager _hubCosmeticManager;

	public HubManager(JavaPlugin plugin, SectorManager sectorManager, ItemManager itemManager,
			WorldManager worldManager, RankManager rankManager, CurrencyManager currencyManager,
			SettingsManager settingManager, ChatManager chatManager, PerkManager perkManager, 
			BackpackManager backpackManager)
	{
		super(plugin, "Hub");

		_sectorManager = sectorManager;
		_itemManager = itemManager;
		_worldManager = worldManager;

		_rankManager = rankManager;
		_currencyManager = currencyManager;
		_settingManager = settingManager;
		_chatManager = chatManager;
		_perkManager = perkManager;
		_backpackManager = backpackManager;

		_hubCosmeticManager = new HubCosmeticManager(plugin, this, _sectorManager, _settingManager);

		_sectorManager.registerSectorHandler(this);
	}

	public ItemManager getItemManager()
	{
		return _itemManager;
	}

	public CurrencyManager getCurrencyManager()
	{
		return _currencyManager;
	}

	public ChatManager getChatManager()
	{
		return _chatManager;
	}

	public HubCosmeticManager getHubCosmeticManager()
	{
		return _hubCosmeticManager;
	}
	
	public PerkManager getPerkManager()
	{
		return _perkManager;
	}
	
	public RankManager getRankManager()
	{
		return _rankManager;
	}
	
	public BackpackManager getBackpackManager()
	{
		return _backpackManager;
	}

	@EventHandler
	public void onWeather(WeatherChangeEvent event)
	{
		if (getSectorManager().getWorld(event.getWorld()).getSector() == Sector.HUB)
			if (event.toWeatherState())
				event.setCancelled(true);
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event)
	{
		if (_sectorManager.getSector(event.getPlayer().getWorld()) == Sector.HUB)
			if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
				event.setCancelled(true);
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event)
	{
		if (_sectorManager.getSector(event.getWhoClicked().getWorld()) == Sector.HUB)
			if (event.getWhoClicked().getGameMode() != GameMode.CREATIVE)
				if (event.getClickedInventory() != null
						&& event.getClickedInventory().equals(event.getWhoClicked().getInventory()))
					event.setCancelled(true);
	}

	@EventHandler
	public void openGameSelect(RequestPlayerOpenGameMenuEvent event)
	{
		new GuiGameSelect(getPlugin(), event.getPlayer()).openInventory();
	}

	@EventHandler
	public void onOptionsSelect(RequestPlayerOpenOptionsMenuEvent event)
	{
		new GuiGameOptions(getPlugin(), event.getPlayer()).openInventory();
	}

	@Override
	public SectorManager getSectorManager()
	{
		return _sectorManager;
	}

	@Override
	public Sector getSector()
	{
		return Sector.HUB;
	}

	@Override
	public void onSectorJoin(Player player)
	{
		_hubCosmeticManager.updateArmor(player);
		
		updateBoard(player);

		_itemManager.getPlayer(player).getItems("sector-hub").clear();
		_itemManager.getPlayer(player).getItems("sector-hub-fly").clear();

		_itemManager.setItem("sector-hub", player, 0, new ItemGameOptions());
		_itemManager.setItem("sector-hub", player, 1, new ItemGameShop());

		_itemManager.setItem("sector-hub-fly", player, 4, new ItemFly(_rankManager, _itemManager, player));

		_itemManager.setItem("sector-hub", player, 7, new ItemLobbyCosmetics(this, _rankManager, _perkManager));
		_itemManager.setItem("sector-hub", player, 8, new ItemLobbySelect(getPlugin(), _sectorManager));
	}

	@Override
	public void onSectorQuit(Player player)
	{
		_itemManager.getPlayer(player).getItems("sector-hub").clear();
		_itemManager.getPlayer(player).getItems("sector-hub-fly").clear();

		IM.cooldown().getPlayer(player).clear(CooldownPool.HUB);
	}

	@Override
	public void onSectorWorldChange(Player player, World to, World from)
	{
		_hubCosmeticManager.updateArmor(player);
		
		_itemManager.getPlayer(player).getItems("sector-hub").clear();
		_itemManager.getPlayer(player).getItems("sector-hub-fly").clear();

		_itemManager.setItem("sector-hub", player, 0, new ItemGameOptions());
		_itemManager.setItem("sector-hub", player, 1, new ItemGameShop());

		_itemManager.setItem("sector-hub-fly", player, 4, new ItemFly(_rankManager, _itemManager, player));

		_itemManager.setItem("sector-hub", player, 7, new ItemLobbyCosmetics(this, _rankManager, _perkManager));
		_itemManager.setItem("sector-hub", player, 8, new ItemLobbySelect(getPlugin(), _sectorManager));
	}

	@Override
	public void onWorldJoinSector(SectorWorld sector)
	{
		_worldManager.setHandler(sector.getWorld(), new HubWorldHandler(getPlugin(), sector.getWorld()));
	}

	@Override
	public void onWorldExitSector(SectorWorld sector)
	{
		_worldManager.removeHandler(sector.getWorld());
	}

	@EventHandler
	public void updateScoreboards(TimeEvent event)
	{
		if (event.getType() != UpdateType.SEC_HALF)
			return;

		for (Player player : _sectorManager.getPlayers(getSector()))
		{
			updateBoard(player);
		}
	}

	private void updateBoard(Player player)
	{
		String rank = _rankManager.getPlayer(player).getDominant().buildDisplay(true, true).toUpperCase();
		HudManager.get(player)
				.setPrefix(ChatColor.stripColor(rank).trim().equalsIgnoreCase("") ? "" : (rank + " " + C.reset));

		HudManager.get(player).scoreboardAddBlank();

		HudManager.get(player).scoreboardAddText(C.goldBold + "Player");
		HudManager.get(player).scoreboardAddText(player.getName());

		HudManager.get(player).scoreboardAddBlank();

		HudManager.get(player).scoreboardAddText(C.goldBold + "Rank");
		HudManager.get(player).scoreboardAddText(_rankManager.getPlayer(player).getDominant().getDisplayName());

		HudManager.get(player).scoreboardAddBlank();

		HudManager.get(player).scoreboardAddText(C.goldBold + "Currency");
		HudManager.get(player).scoreboardAddText(
				C.aqua + "Iron " + C.white + _currencyManager.getPlayer(player).getCurrency(CurrencyType.IRON));
		HudManager.get(player).scoreboardAddText(
				C.aqua + "Gold " + C.white + _currencyManager.getPlayer(player).getCurrency(CurrencyType.GOLD));
		HudManager.get(player).scoreboardAddText(
				C.aqua + "Gems " + C.white + _currencyManager.getPlayer(player).getCurrency(CurrencyType.GEMS));

		HudManager.get(player).scoreboardAddBlank();

		HudManager.get(player).scoreboardAddText(C.yellowBold + MinefinityPlugin.getWebsite());

		HudManager.get(player).scoreboardBuild();
	}
}
