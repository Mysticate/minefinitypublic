package me.mysticate.minefinity.hub.items;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.item.ItemCallback;
import me.mysticate.minefinity.core.item.UseableItem;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.hub.HubManager;
import me.mysticate.minefinity.hub.gui.cosmetics.GuiLobbyCosmetics;

public class ItemLobbyCosmetics extends UseableItem
{
	public ItemLobbyCosmetics(HubManager manager, RankManager rankManager, PerkManager perkManager)
	{
		super(Material.ENDER_CHEST, new ItemCallback()
		{
			@Override
			public void onClick(Player player, Action type)
			{
				new GuiLobbyCosmetics(MinefinityPlugin.getPlugin(), player, manager, perkManager, rankManager)
						.openInventory();
			}
		});

		setDroppable(false);
		UtilItemStack.name(this, C.green + "Hub Cosmetics");
	}
}
