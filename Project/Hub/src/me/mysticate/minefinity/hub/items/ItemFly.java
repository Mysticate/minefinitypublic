package me.mysticate.minefinity.hub.items;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.util.Vector;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.cooldown.CooldownPool;
import me.mysticate.minefinity.core.item.ItemCallback;
import me.mysticate.minefinity.core.item.ItemManager;
import me.mysticate.minefinity.core.item.UseableItem;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.core.rank.RankManager;

public class ItemFly extends UseableItem
{
	public ItemFly(RankManager rankManager, ItemManager itemManager, Player player)
	{
		super(Material.FEATHER, new ItemCallback()
		{
			@Override
			public void onClick(Player player, Action type)
			{
				if (!rankManager.isOwned(player, Rank.CHAMPION, true))
					return;

				if (!IM.cooldown().use(CooldownPool.BATTLE_ARENA, player, "Fly Boost", 500))
					return;

				if (player.getAllowFlight())
				{
					player.setAllowFlight(false);
					player.setFlying(false);
				} else
				{
					player.setVelocity(new Vector(0, .3, 0));

					player.setAllowFlight(true);
					player.setFlying(true);
				}

				itemManager.getPlayer(player).getItems("sector-hub-fly").clear();
				itemManager.setItem("sector-hub-fly", player, 4, new ItemFly(rankManager, itemManager, player));
			}
		});

		setDroppable(false);
		UtilItemStack.name(this,
				C.aquaBold + "Flying " + C.dGrayBold + "[" + F.trueFalse(player.getAllowFlight()) + C.dGrayBold + "]");
	}
}
