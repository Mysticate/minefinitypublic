package me.mysticate.minefinity.hub.items;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.item.ItemCallback;
import me.mysticate.minefinity.core.item.UseableItem;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.hub.gui.lobby.GuiLobby;

public class ItemLobbySelect extends UseableItem
{
	public ItemLobbySelect(JavaPlugin plugin, SectorManager manager)
	{
		super(Material.GHAST_TEAR, (byte) 0, new ItemCallback()
		{
			@Override
			public void onClick(Player player, Action type)
			{
				new GuiLobby(plugin, manager, player).openInventory();
			}
		});

		setDroppable(false);
		UtilItemStack.name(this, C.green + "Lobby Selector");
	}
}
