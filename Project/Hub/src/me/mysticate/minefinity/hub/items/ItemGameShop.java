package me.mysticate.minefinity.hub.items;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.item.ItemCallback;
import me.mysticate.minefinity.core.item.UseableItem;
import me.mysticate.minefinity.hub.gui.gameoptions.GuiGameOptions;

public class ItemGameShop extends UseableItem
{
	public ItemGameShop()
	{
		super(Material.EMERALD, (byte) 0, new ItemCallback()
		{
			@Override
			public void onClick(Player player, Action type)
			{
				new GuiGameOptions(MinefinityPlugin.getPlugin(), player).openInventory();
			}
		});

		setDroppable(false);
		UtilItemStack.name(this, C.green + "Game Options");
	}
}
