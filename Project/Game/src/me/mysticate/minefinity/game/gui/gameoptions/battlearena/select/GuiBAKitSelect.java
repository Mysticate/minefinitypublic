package me.mysticate.minefinity.game.gui.gameoptions.battlearena.select;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.buttons.BackButton;
import me.mysticate.minefinity.core.gui.buttons.BackButton.ButtonReturn;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameType;
import me.mysticate.minefinity.game.games.battlearena.BattleArenaKit;
import me.mysticate.minefinity.game.gui.gameoptions.ButtonSelectKit;
import me.mysticate.minefinity.game.gui.gameoptions.battlearena.GuiOptionsBA;

public class GuiBAKitSelect extends Gui
{
	private GameManager _manager;

	public GuiBAKitSelect(JavaPlugin plugin, Player player, GameManager manager)
	{
		super(plugin, player, "Kit Select", 27);

		_manager = manager;
	}

	@Override
	protected void addItems()
	{
		setItem(0, new BackButton(new ButtonReturn()
		{
			@Override
			public void onReturn(Gui gui, Player player, ClickType type)
			{
				new GuiOptionsBA(getPlugin(), player, _manager).openInventory();
			}
		}, "Options"));

		for (int i = 0; i < BattleArenaKit.values().length; i++)
		{
			BattleArenaKit kit = BattleArenaKit.values()[i];
			setItem(i + 10, new ButtonSelectKit(_manager, GameType.BATTLE_ARENA, kit));
		}

		borders(false);
	}

	@Override
	protected void onOpen()
	{

	}

	@Override
	protected void onClose()
	{

	}
}
