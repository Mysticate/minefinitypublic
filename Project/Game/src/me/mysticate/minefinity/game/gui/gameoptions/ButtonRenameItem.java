package me.mysticate.minefinity.game.gui.gameoptions;

import java.util.LinkedList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.chat.reader.ChatReader;
import me.mysticate.minefinity.core.chat.reader.ChatReaderResponse;
import me.mysticate.minefinity.core.chat.reader.IChatReaderCallback;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.common.util.UtilText;
import me.mysticate.minefinity.core.currency.CurrencyPlayer;
import me.mysticate.minefinity.core.currency.CurrencyType;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.confirmation.ConfirmationPage;
import me.mysticate.minefinity.core.gui.confirmation.ConfirmationResponse;
import me.mysticate.minefinity.core.gui.confirmation.IConfirmationCallback;
import me.mysticate.minefinity.core.settings.SettingsPlayer;
import me.mysticate.minefinity.core.shop.Price;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameType;

public class ButtonRenameItem implements Button
{
	private GameManager _manager;
	private GameType _game;

	private String _name;
	private String _displayName;

	private Material _mat;
	private byte _data;

	public ButtonRenameItem(GameManager manager, GameType game, String name, String displayName, Material mat,
			byte data)
	{
		_manager = manager;
		_game = game;

		_name = name;
		_displayName = displayName;

		_mat = mat;
		_data = data;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		SettingsPlayer settingsPlayer = _manager.getSettingsManager().getPlayer(player);
		CurrencyPlayer currencyPlayer = _manager.getCurrencyManager().getPlayer(player);

		String current = C.white + "None";

		if (settingsPlayer.hasSetting(_game.getSettings(), _name))
			current = C.yellow + settingsPlayer.getSetting(_game.getSettings(), _name);

		LinkedList<String> lore = new LinkedList<String>();

		lore.add(" ");
		lore.add(C.gray + "Current " + current);
		lore.add(" ");
		lore.add(C.gray + "Please refrain from crude");
		lore.add(C.gray + "or vulgar item names.");
		lore.add(" ");

		for (String string : F.cost("Item Rename", 0, CurrencyType.GEMS, Price.GAME_ITEM_RENAME,
				currencyPlayer.getCurrency(CurrencyType.GEMS)))
			lore.add(string);

		return UtilItemStack.create(_mat, _data, 1, C.yellowBold + _displayName, lore.toArray(new String[0]));
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		SettingsPlayer settingsPlayer = _manager.getSettingsManager().getPlayer(player);
		CurrencyPlayer currencyPlayer = _manager.getCurrencyManager().getPlayer(player);

		if (currencyPlayer.canAfford(CurrencyType.GEMS, Price.GAME_ITEM_RENAME))
		{
			new ConfirmationPage(gui.getPlugin(), player, new IConfirmationCallback()
			{
				@Override
				public void run(ConfirmationResponse response)
				{
					if (response == ConfirmationResponse.CONFIRM)
					{
						player.closeInventory();

						if (!_manager.getChatManager()
								.addChatReader(new ChatReader(player, 20000, new IChatReaderCallback()
						{
							@Override
							public void onRead(ChatReaderResponse response, String message)
							{
								if (response == ChatReaderResponse.SUCCES)
								{
									message.trim();

									if (!UtilText.validate(message, 2, 16, true))
									{
										player.sendMessage(F.manager("Shop", "Invalid name! Please try again."));
									} else
									{
										if (currencyPlayer.removeCurrency(CurrencyType.GEMS, Price.GAME_ITEM_RENAME))
										{
											settingsPlayer.set(_game.getSettings(), _name, message);
											gui.openInventory();
										} else
										{
											player.sendMessage(
													F.manager("Shop", "You don't have enough money to purchase that!"));
										}
									}
								}
							}
						}), "What do you want the name to be?"))
						{
							player.sendMessage(
									F.manager("Shop", "Failed to open a new chat reader. Please type \"cancel.\""));
						}
					} else
					{
						gui.openInventory();
					}
				}
			}, _displayName + " Rename", Price.GAME_ITEM_RENAME + " " + CurrencyType.GEMS.getName(), false)
					.openInventory();
		}
	}
}