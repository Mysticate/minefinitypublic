package me.mysticate.minefinity.game.gui.gameoptions.battlearena;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.game.GameRunner;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.buttons.BackButton;
import me.mysticate.minefinity.core.gui.buttons.BackButton.ButtonReturn;
import me.mysticate.minefinity.game.GameManager;

public class GuiOptionsBA extends Gui
{
	private GameManager _manager;

	public GuiOptionsBA(JavaPlugin plugin, Player player, GameManager manager)
	{
		super(plugin, player, "Options", 27);

		_manager = manager;
	}

	@Override
	protected void addItems()
	{
		setItem(0, new BackButton(new ButtonReturn()
		{
			@Override
			public void onReturn(Gui gui, Player player, ClickType type)
			{
				GameRunner.openOptionsMenu(player);
			}
		}, "Game Options"));

		borders(true);

		addItem(new ButtonOptionsSelectKit(_manager));
		addItem(new ButtonOptionsUpgradeKit(_manager));
		addItem(new ButtonOptionsRenameItem(_manager));
	}

	@Override
	protected void onOpen()
	{

	}

	@Override
	protected void onClose()
	{

	}
}
