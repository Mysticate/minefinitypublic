package me.mysticate.minefinity.game.gui.gameselect;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.game.GameRunner;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.buttons.BackButton;
import me.mysticate.minefinity.core.gui.buttons.BackButton.ButtonReturn;
import me.mysticate.minefinity.core.gui.buttons.ItemButton;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameType;

public class GuiGameSelect extends Gui
{
	private GameManager _manager;
	private GameType _game;

	private HashSet<Button> _buttons = new HashSet<Button>();

	public GuiGameSelect(JavaPlugin plugin, Player player, GameManager manager, GameType game)
	{
		super(plugin, player, "Join " + C.underline + game.getDisplay(), 54);

		_manager = manager;
		_game = game;
	}

	@EventHandler
	public void onHalfSecond(TimeEvent event)
	{
		if (event.getType() == UpdateType.SEC_HALF)
			updateGames();
	}

	private void updateGames()
	{
		for (Button button : _buttons)
			for (int i = 0; i < getButtons().length; i++)
				if (getButtons()[i] == button)
					setItem(i, null);

		_buttons.clear();

		List<Game> games = _manager.getGames(_game);
		Collections.sort(games, _gameIdComparator);

		for (Game game : games)
		{
			ButtonGameJoin button = new ButtonGameJoin(game);
			addItem(button);

			_buttons.add(button);
		}

		updateInventory();
	}

	@Override
	protected void addItems()
	{
		setItem(0, new BackButton(new ButtonReturn()
		{
			@Override
			public void onReturn(Gui gui, Player player, ClickType type)
			{
				GameRunner.openGameMenu(player);
			}
		}, "Game Select"));

		setItem(3,
				new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 5, 1, C.greenBold + "Green "
						+ C.white + "Joiner",
				new String[] { " ", C.gray + "There are slots available", C.gray + "Joinable: " + C.white + "Yes" })));

		setItem(4,
				new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 1, 1,
						C.goldBold + "Gold " + C.white + "Joiner", new String[] { " ", C.gray + "That game is full",
								C.gray + "Joinable: " + C.white + "Donators Only" })));

		setItem(5,
				new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 14, 1, C.redBold + "Red "
						+ C.white + "Joiner",
				new String[] { " ", C.gray + "That game is underway", C.gray + "Joinable: " + C.white + "No" })));

		borders(true);
	}

	@Override
	protected void onOpen()
	{
		updateGames();
	}

	@Override
	protected void onClose()
	{

	}

	private Comparator<Game> _gameIdComparator = new Comparator<Game>()
	{
		@Override
		public int compare(Game o1, Game o2)
		{
			if (o1.getId() == o2.getId())
				return 0;

			return o1.getId() < o2.getId() ? -1 : 1;
		}
	};
}
