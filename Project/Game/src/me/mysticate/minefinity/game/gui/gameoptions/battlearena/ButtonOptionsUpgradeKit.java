package me.mysticate.minefinity.game.gui.gameoptions.battlearena;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.gui.gameoptions.battlearena.upgrade.GuiBAKitUpgrade;

public class ButtonOptionsUpgradeKit implements Button
{
	private GameManager _manager;

	public ButtonOptionsUpgradeKit(GameManager manager)
	{
		_manager = manager;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.ANVIL, (byte) 0, 1, C.yellowBold + "Upgrades",
				new String[] { " ", C.gray + "Click to upgrade your kits" });
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		new GuiBAKitUpgrade(gui.getPlugin(), player, _manager).openInventory();
	}
}
