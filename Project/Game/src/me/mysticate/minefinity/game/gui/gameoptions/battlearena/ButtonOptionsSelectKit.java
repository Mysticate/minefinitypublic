package me.mysticate.minefinity.game.gui.gameoptions.battlearena;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.gui.gameoptions.battlearena.select.GuiBAKitSelect;

public class ButtonOptionsSelectKit implements Button
{
	private GameManager _manager;

	public ButtonOptionsSelectKit(GameManager manager)
	{
		_manager = manager;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.ARMOR_STAND, (byte) 0, 1, C.yellowBold + "Select Kit",
				new String[] { " ", C.green + "Available Kits", C.gray + "- " + C.white + "Knight",
						C.gray + "- " + C.white + "Sniper", C.gray + "- " + C.white + "Piggy", });
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		new GuiBAKitSelect(gui.getPlugin(), player, _manager).openInventory();
	}
}
