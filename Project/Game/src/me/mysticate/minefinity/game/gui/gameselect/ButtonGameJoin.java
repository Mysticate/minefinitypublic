package me.mysticate.minefinity.game.gui.gameselect;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameState;

public class ButtonGameJoin implements Button
{
	private Game _game;

	public ButtonGameJoin(Game game)
	{
		_game = game;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		int count = _game.getPlayerCount();
		byte color = _game.getState().getFlagData();

		if ((_game.getState() == GameState.LOBBY || _game.getState() == GameState.COUNTDOWN)
				&& count >= _game.getType().getMax())
			color = (byte) 14;

		List<String> lore = new ArrayList<String>();

		lore.add(" ");
		lore.add(C.gray + "Players: " + C.white + _game.getPlayerCount() + " / " + _game.getType().getMax());
		lore.add(C.red + "Available Donator Slots: " + Math.max(0,
				((_game.getType().getAbsMax() - _game.getType().getMax()) - (count - _game.getType().getMax()))));
		lore.add(" ");
		lore.add(C.gray + "Game: " + C.white + _game.getType().getDisplay());
		lore.add(C.gray + "Map: " + C.white + (_game.getMap() == null ? "Loading" : _game.getMap().getName()));
		lore.add(C.gray + "State: " + C.white + _game.getState().toString());

		if (_game.getState() == GameState.COUNTDOWN)
		{
			lore.add(" ");
			lore.add(C.gray + "Starting in " + C.white + _game.getTimeBeforeLobbyEnd());
		}

		return UtilItemStack.create(Material.BANNER, color, count, C.yellowBold + _game.getName(),
				lore.toArray(new String[0]));
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		if (_game.inLobby())
		{
			player.closeInventory();
			_game.attemptJoin(player);
		}
	}
}
