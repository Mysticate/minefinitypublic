package me.mysticate.minefinity.game.kit;

import java.util.HashMap;
import java.util.UUID;

import me.mysticate.minefinity.core.player.DataPlayer;
import me.mysticate.minefinity.game.GameType;

public class KitPlayer extends DataPlayer
{
	private HashMap<GameType, Integer> _kits = new HashMap<GameType, Integer>();

	public KitPlayer(UUID id)
	{
		super(id, PlayerActiveKitManager.getFilePath(id));
	}

	public void setKit(GameType game, Integer kit)
	{
		if (kit == null || kit == -1)
		{
			_kits.remove(game);
		} else
		{
			_kits.put(game, kit);
		}
	}

	public boolean hasKit(GameType game)
	{
		return getKit(game) != null && _kits.get(game) != -1;
	}

	public Integer getKit(GameType game)
	{
		return _kits.get(game);
	}

	@Override
	public void load()
	{
		for (GameType game : GameType.values())
		{
			try
			{
				String kit = read(game.getAbv(), null);
				if (kit == null)
					continue;

				Integer id = Integer.valueOf(kit);
				if (id == -1)
					continue;

				setKit(game, id);
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void save()
	{
		for (GameType game : _kits.keySet())
		{
			Integer kit = _kits.get(game);
			if (kit == null || kit == -1)
				continue;

			write(kit + "", game.getAbv());
		}
	}
}
