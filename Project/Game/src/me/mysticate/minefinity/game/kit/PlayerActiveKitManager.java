package me.mysticate.minefinity.game.kit;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.DataPlayerManager;
import me.mysticate.minefinity.core.file.DataFolder;
import me.mysticate.minefinity.core.file.FilePath;

public class PlayerActiveKitManager extends DataPlayerManager<KitPlayer>
{
	private static final String _filePath = "activekits";

	public PlayerActiveKitManager(JavaPlugin plugin)
	{
		super(plugin, "Active Kits");
	}

	public static FilePath getFilePath(UUID uuid)
	{
		return new FilePath(DataFolder.home(uuid), _filePath);
	}

	@Override
	protected KitPlayer newData(Player player)
	{
		return new KitPlayer(player.getUniqueId());
	}

	@Override
	protected KitPlayer newOfflineData(UUID uuid)
	{
		return new KitPlayer(uuid);
	}
}
