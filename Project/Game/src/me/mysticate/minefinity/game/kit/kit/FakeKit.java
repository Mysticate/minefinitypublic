package me.mysticate.minefinity.game.kit.kit;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameManager;

public class FakeKit extends Kit<Game>
{
	public FakeKit(GameManager manager, Game game, Player player)
	{
		super(manager, game, player, null);
	}

	@Override
	public void onEnable()
	{

	}

	@Override
	public void onDisable(KitDisableReason reason)
	{

	}

	@Override
	protected void giveItemsCustom()
	{

	}

	@Override
	public int getId()
	{
		return 999;
	}

	@Override
	public String getDisplayName()
	{
		return "None";
	}
}
