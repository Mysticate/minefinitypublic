package me.mysticate.minefinity.game.kit.kit;

import org.bukkit.Material;

import me.mysticate.minefinity.game.Game;

public interface IKit<G extends Game>
{
	public IKitBuilder<G> getKitBuilder();

	public int getId();

	public boolean isFree();

	public int getUnlockCost();

	public int getMaxLevel();

	public int getMaxMagic();

	public int getMagicPurchaseStart();

	public Material getDisplayMat();

	public byte getDisplayData();

	public String getDisplayName();

	public String[] getDisplayLore();
}
