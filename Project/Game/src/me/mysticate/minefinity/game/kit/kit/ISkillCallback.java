package me.mysticate.minefinity.game.kit.kit;

public interface ISkillCallback
{
	public void activateAbility();
}
