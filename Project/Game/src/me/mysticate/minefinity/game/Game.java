package me.mysticate.minefinity.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.util.Vector;

import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilInv;
import me.mysticate.minefinity.core.common.util.UtilMath;
import me.mysticate.minefinity.core.common.util.UtilPlayer;
import me.mysticate.minefinity.core.currency.CurrencyType;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.item.UseableItem;
import me.mysticate.minefinity.core.perk.PerkPlayer;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.core.sector.Sector;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.game.data.PlayerRewardSummary;
import me.mysticate.minefinity.game.event.GameStateChangeEvent;
import me.mysticate.minefinity.game.kit.KitPlayer;
import me.mysticate.minefinity.game.kit.kit.FakeKit;
import me.mysticate.minefinity.game.kit.kit.IKit;
import me.mysticate.minefinity.game.kit.kit.Kit;
import me.mysticate.minefinity.game.kit.kit.Kit.KitDisableReason;
import me.mysticate.minefinity.game.map.GameWorldHandler;
import me.mysticate.minefinity.game.map.Map;
import me.mysticate.minefinity.game.map.MapLocation;

public abstract class Game extends Manager
{
	private GameManager _manager;

	private int _id;

	private GameType _type;
	private GameState _state;

	private Map _map;
	private GameWorldHandler<?> _worldHandler;

	private boolean _enableDataTeleport = true;

	private ArrayList<Player> _players = new ArrayList<Player>();
	private HashMap<Player, Kit<?>> _playerKits = new HashMap<Player, Kit<?>>();

	private HashMap<Player, PlayerRewardSummary> _rewards = new HashMap<Player, PlayerRewardSummary>();

	private HashMap<GameState, IScoreboardHandler<?>> _scoreboardHandlers = new HashMap<GameState, IScoreboardHandler<?>>();

	private long _stateCount = 0;
	private long _stateTimestamp = 0;
	private boolean _forceStarted;

	private boolean _endUnderMin = true;

	protected final int LobbyEnd = 180;
	protected final int LobbyShortened = 150;

	private IKit<?>[] _enabledKits = new IKit<?>[0];

	public Game(GameManager manager, GameType game, int id)
	{
		super(manager.getPlugin(), game.getAbv() + id);

		_manager = manager;

		_id = id;

		_type = game;

		setState(GameState.CLEANING);
	}

	public GameManager getManager()
	{
		return _manager;
	}

	public final int getId()
	{
		return _id;
	}

	public GameType getType()
	{
		return _type;
	}

	public GameState getState()
	{
		return _state;
	}

	public void setState(GameState state)
	{
		if (getState() == state)
			return;

		final GameState previous = _state;

		_state = state;
		setStateCount(0);
		_stateTimestamp = System.currentTimeMillis();

		Bukkit.getPluginManager().callEvent(new GameStateChangeEvent(this, _state, previous));
	}

	public Map getMap()
	{
		return _map;
	}

	public void setMap(Map map)
	{
		if (getState() != GameState.CLEANING)
			return;

		if (_map != null)
			return;

		if (map == _map)
			return;

		if (_worldHandler != null)
		{
			_worldHandler.stop();
			_manager.getWorldManager().removeHandler(_worldHandler);
		}

		_map = map;

		_worldHandler = newWorldHandler(_map.getWorld());
		_manager.getWorldManager().setHandler(_map.getWorld(), _worldHandler);

		setState(GameState.LOBBY);
	}

	protected abstract GameWorldHandler<?> newWorldHandler(World world);

	public GameWorldHandler<?> getWorldHandler()
	{
		return _worldHandler;
	}

	public void setDataTeleport(boolean flag)
	{
		_enableDataTeleport = flag;
	}

	protected void setEndUnderMin(boolean flag)
	{
		_endUnderMin = flag;
	}

	public boolean endUnderMin()
	{
		return _endUnderMin;
	}

	public boolean inLobby()
	{
		return getState() == GameState.LOBBY || getState() == GameState.COUNTDOWN;
	}

	public boolean inTeleporting()
	{
		return getState() == GameState.TELEPORTING;
	}

	public boolean inBuffer()
	{
		return getState() == GameState.PREGAME || getState() == GameState.POSTGAME;
	}

	public boolean inGame()
	{
		return getState() == GameState.INGAME;
	}

	public boolean inCleaning()
	{
		return getState() == GameState.CLEANING;
	}

	public void addPlayer(Player player)
	{
		loadKit(player);

		if (!hasPlayer(player))
			getPlayers().add(player);

		_rewards.put(player, new PlayerRewardSummary());

		getManager().getEnergyManager().getPlayer(player).clear();
		getManager().getEnergyManager().getPlayer(player).setEnabled(true);
		getManager().getCombatManager().getPlayer(player).clearLog();
		getManager().getScoreManager().getPlayer(player).removeScores(getType());
	}

	public void removePlayer(Player player)
	{
		killKit(player, KitDisableReason.QUIT);
		getPlayers().remove(player);

		_rewards.remove(player);
		getManager().getScoreManager().getPlayer(player).removeScores(getType());

		getManager().getEnergyManager().getPlayer(player).clear();
		getManager().getEnergyManager().getPlayer(player).setEnabled(true);
		getManager().getCombatManager().getPlayer(player).clearLog();
		getManager().getScoreManager().getPlayer(player).removeScores(getType());
	}

	public boolean hasPlayer(Entity player)
	{
		if (player == null || !(player instanceof Player))
			return false;

		return getPlayers().contains(player);
	}

	public ArrayList<Player> getPlayers()
	{
		return _players;
	}

	public String formatName(Player player)
	{
		return getManager().getRankManager().getPlayer(player).getDominant().getColor() + player.getName();
	}

	public HashMap<Player, Kit<?>> getKits()
	{
		return _playerKits;
	}

	public void setKit(Player player, Kit<?> kit)
	{
		if (kit == null)
			return;

		killKit(player, KitDisableReason.SWITCH_KITS);
		_playerKits.put(player, kit);
	}

	public Kit<?> getKit(Player player)
	{
		return _playerKits.get(player);
	}

	public boolean hasKit(Player player)
	{
		return getKit(player) != null;
	}

	public void loadKit(Player player)
	{
		List<IKit<?>> defaults = new ArrayList<IKit<?>>();

		for (IKit<?> kit : _enabledKits)
		{
			PerkPlayer perkPlayer = getManager().getPerkManager().getPlayer(player);

			if (kit.isFree())
			{
				defaults.add(kit);

				if (!perkPlayer.hasPerk(Kit.getPerkName(getType(), kit.getId(), Kit.Level)))
					perkPlayer.buyPerk(Kit.getPerkName(getType(), kit.getId(), Kit.Level));
			}
		}

		KitPlayer kitPlayer = getManager().getKitManager().getPlayer(player);
		if (getManager().getKitManager().getPlayer(player).hasKit(getType()))
		{
			try
			{
				for (IKit<?> kit : _enabledKits)
				{
					if (kit.getId() == kitPlayer.getKit(getType()))
					{
						Kit<?> newInstance = newKitInstance(kit.getId(), player);
						if (newInstance != null)
						{
							if (newInstance.getLevel() > 0)
							{
								setKit(player, newInstance);
								sendMessage(player, C.green + "Successfully loaded your "
										+ getKit(player).getDisplayName() + " kit!");

								return;
							}
						}
					}
				}
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}

		if (!defaults.isEmpty())
		{
			Kit<?> newInstance = newKitInstance(UtilMath.getRandom(defaults).getId(), player);
			if (newInstance != null)
			{
				setKit(player, newInstance);
				sendMessage(player, C.red + "Could not locate your selected kit. You will recieve "
						+ getKit(player).getDisplayName() + ".");

				return;
			}
		}

		setKit(player, new FakeKit(getManager(), this, player));
		sendMessage(player, C.red + "Could not locate your selected kit. You will not recieve a kit.");
	}

	protected abstract Kit<?> newKitInstance(int kitId, Player player);

	public void killKit(Player player, KitDisableReason reason)
	{
		Kit<?> kit = _playerKits.remove(player);
		if (kit != null)
		{
			kit.onDisable(reason);
			HandlerList.unregisterAll(kit);
		}
	}

	protected void setKits(IKit<?>... kits)
	{
		_enabledKits = kits;
	}

	public int getPlayerCount()
	{
		return getPlayers().size();
	}

	public long getStateTimestamp()
	{
		return _stateTimestamp;
	}

	public long getStateCount()
	{
		return _stateCount;
	}

	public void setStateCount(long time)
	{
		_stateCount = time;
	}

	public void forceStart()
	{
		if (getState() == GameState.LOBBY)
		{
			_forceStarted = true;
			setState(GameState.COUNTDOWN);
		} else if (getState() == GameState.COUNTDOWN)
		{
			if (getStateCount() < LobbyShortened)
				setStateCount(LobbyShortened);
		}
	}

	public long getTimeBeforeLobbyEnd()
	{
		return LobbyEnd - getStateCount();
	}

	public void setScoreboardHandler(GameState state, IScoreboardHandler<?> handler)
	{
		_scoreboardHandlers.put(state, handler);
	}

	private void updateScoreboards(TimeEvent event)
	{
		IScoreboardHandler<?> handler = _scoreboardHandlers.get(getState());
		if (handler == null)
			return;

		for (Player player : getPlayers())
		{
			handler.update(event, player);
		}
	}

	public void sendMessage(String... message)
	{
		for (Player player : getPlayers())
		{
			for (String msg : message)
				player.sendMessage(msg);
		}
	}

	public void periodMessage(String... lines)
	{
		for (Player player : getPlayers())
		{
			periodMessage(player, lines);
		}
	}

	public void periodMessage(Player player, String... lines)
	{
		player.sendMessage("  ");
		player.sendMessage("  ");
		player.sendMessage("  ");
		player.sendMessage("  ");
		player.sendMessage(C.greenBold + C.strike + GameFormat.Line);
		player.sendMessage(C.whiteBold + "  " + getType().getDisplay());
		player.sendMessage(" ");

		for (String line : lines)
			player.sendMessage("  " + line);

		player.sendMessage(" ");
		player.sendMessage(C.greenBold + C.strike + GameFormat.Line);
	}

	protected void rewardsMessage()
	{
		for (Player player : getPlayers())
		{
			ArrayList<String> lines = new ArrayList<String>();

			PlayerRewardSummary summary = _rewards.get(player);
			if (summary != null)
			{
				for (CurrencyType currency : CurrencyType.values())
				{
					if (summary.getRewards(currency) > 0)
					{
						lines.add(C.gray + "Total " + currency.getFormatted() + C.gray + " earned: " + C.white
								+ summary.getRewards(currency));
					}
				}
			}

			if (lines.isEmpty())
			{
				periodMessage(player, C.gray + "No currency earned");
			} else
			{
				periodMessage(player, lines.toArray(new String[0]));
			}
		}
	}

	public boolean attemptJoin(Player player)
	{
		if (getManager().getGame(player) != null)
		{
			player.kickPlayer(C.red + "An error occured while connecting you to that game");
			return false;
		}

		if (!inLobby())
		{
			sendMessage(player, "That game is already started!");
			return false;
		}

		if (getPlayerCount() >= getType().getMax())
		{
			if (getPlayerCount() >= getType().getAbsMax())
			{
				sendMessage(player, "That game is full!");
				return false;
			}

			if (!getManager().getRankManager().getPlayer(player).isOwned(Rank.MASTER))
			{
				sendMessage(player,
						"Purchase " + Rank.MASTER.buildDisplay(true, true) + C.gray + " to join that game!");
				return false;
			}
		}

		getPlayers().add(player);

		player.teleport(getWorldHandler().getJoinLocation(player));

		return true;
	}

	@EventHandler
	public void onEnd(GameStateChangeEvent event)
	{
		if (event.getGame() != this)
			return;

		if (event.getState() == GameState.POSTGAME)
		{
			giveEndRewards();
		}
	}

	@EventHandler
	public void onTeleport(TimeEvent event)
	{
		if (event.getType() != UpdateType.TICK)
			return;

		if (!_enableDataTeleport)
			return;

		if (getMap() == null)
			return;

		for (Player player : getPlayers())
		{
			for (MapLocation loc : getMap().getData("teleport"))
			{
				Location mapLoc = loc.getLocation(getMap().getWorld());
				if (!mapLoc.getBlock().equals(player.getLocation().getBlock()))
					continue;

				Block block = mapLoc.clone().subtract(0, 2, 0).getBlock();
				if (block.getType() != Material.WALL_SIGN && block.getType() != Material.SIGN
						&& block.getType() != Material.SIGN_POST)
					break;

				if (!(block.getState() instanceof Sign))
					break;

				Sign sign = (Sign) (block.getState());
				MapLocation dest = getManager().getMapManager().parse(ChatColor.stripColor(sign.getLine(0)));
				if (dest == null)
					break;

				player.setVelocity(new Vector());

				Location to = dest.getLocation(getMap().getWorld());
				to.setPitch(player.getLocation().getPitch());
				to.setYaw(player.getLocation().getYaw());

				player.teleport(to);

				getMap().getWorld().spigot().playEffect(mapLoc, Effect.FIREWORKS_SPARK, 0, 0, .5F, 1F, .5F, 0F, 10, 10);
				getMap().getWorld().spigot().playEffect(to, Effect.FIREWORKS_SPARK, 0, 0, .5F, 1.25F, .5F, 0F, 10, 10);

				break;
			}
		}
	}

	protected abstract void giveEndRewards();

	protected abstract String[] getEndGameText();

	protected abstract void onJoin(Player player);

	protected abstract void onQuit(Player player);

	public void lobbyItems(Player player)
	{
		clearLobbyItems(player);
		giveLobbyItems(player);
	}

	protected abstract void giveLobbyItems(Player player);

	protected void giveLobbyItem(Player player, UseableItem item)
	{
		getManager().getItemManager().setItem("game-lobby", player, 0, item);
	}

	private void clearLobbyItems(Player player)
	{
		getManager().getItemManager().getPlayer(player).getItems("game-lobby").clear();
		UtilInv.clearInvAbs(player);
	}

	public void sendToHub(Player player)
	{
		if (!getManager().getSectorManager().sendToSector(player, Sector.HUB))
		{
			player.kickPlayer(F.kick(C.red + "Failed to connect you to a Hub!"));
		}
	}

	protected void addCurrency(Player player, CurrencyType currency, int amount, String reason)
	{
		getManager().getCurrencyManager().getPlayer(player).addCurrency(currency, amount);

		if (reason != null)
		{
			player.sendMessage(
					C.white + "+" + amount + " " + currency.getFormatted() + C.gray + " for " + C.white + reason);
		}

		if (_rewards.containsKey(player))
		{
			_rewards.get(player).addRewards(currency, amount);
		}
	}

	@EventHandler
	public void onStateCount(TimeEvent event)
	{
		updateScoreboards(event);

		if (event.getType() == UpdateType.SEC_01)
		{
			if (getStateCount() == -1)
				return;

			setStateCount(getStateCount() + 1);
			onCount();

			if (getState() == GameState.LOBBY)
			{
				onLobbyCount();
			} else if (getState() == GameState.COUNTDOWN)
			{
				onCountdownCount();
			} else if (getState() == GameState.TELEPORTING)
			{
				onTeleportingCount();
			} else if (getState() == GameState.PREGAME)
			{
				onPregameCount();
			} else if (getState() == GameState.INGAME)
			{
				onIngameCount();
			} else if (getState() == GameState.POSTGAME)
			{
				onEndingCount();
			}
		}
	}

	protected void onCount()
	{

	}

	protected void onLobbyCount()
	{
		if (getPlayerCount() >= getType().getMin())
		{
			setState(GameState.COUNTDOWN);

			if (getPlayerCount() >= getType().getMax())
			{
				setStateCount(LobbyShortened);
				sendMessage(F.manager(getName(), "Wait time shortened to " + C.yellow + "30 Seconds" + C.gray + "!"));
			}
		}
	}

	protected void onCountdownCount()
	{
		if (getPlayerCount() < getType().getMin() && !_forceStarted)
		{
			setState(GameState.LOBBY);
		} else if (getPlayerCount() >= getType().getMax() && getStateCount() < LobbyShortened)
		{
			setStateCount(LobbyShortened);
			sendMessage(F.manager(getName(), "Wait time shortened to " + C.yellow + "30 Seconds" + C.gray + "!"));
		} else if (getStateCount() >= LobbyEnd)
		{
			for (Player player : getPlayers())
			{
				clearLobbyItems(player);
				player.setVelocity(new Vector());

				getManager().getCombatManager().getPlayer(player).clearLog();
			}

			setState(GameState.TELEPORTING);
		}
	}

	protected void onTeleportingCount()
	{
		if (getStateCount() == 1)
		{
			int cur = 0;
			for (Player player : getPlayers())
			{
				player.setVelocity(new Vector());

				cur++;

				Bukkit.getScheduler().runTaskLater(getPlugin(), new Runnable()
				{
					@Override
					public void run()
					{
						if (!hasPlayer(player))
							return;

						player.setVelocity(new Vector());
						player.teleport(getMap().nextSpawn());
						UtilPlayer.reset(player, false);

						Kit<?> kit = getKit(player);
						if (kit != null)
						{
							HandlerList.unregisterAll(kit);
							Bukkit.getPluginManager().registerEvents(kit, getPlugin());

							kit.onEnable();
							kit.giveItems();
						}
					}
				}, cur * 20);
			}
		}

		if (getStateCount() > getPlayerCount() + 5)
		{
			setState(GameState.PREGAME);
		}
	}

	protected void onPregameCount()
	{
		for (Player player : getPlayers())
			HudManager.get(player).sendTitle(C.greenBold + "Starting",
					C.greenBold + "in " + C.whiteBold + (15 - getStateCount()) + C.greenBold + " seconds", 0, 40, 0);

		if (getStateCount() == 1)
		{
			periodMessage(
					C.dGreenBold + "Map " + C.gray + "- " + C.yellowBold + getMap().getName() + C.gray + " by "
							+ C.yellowBold + getMap().getCreator(),
					C.dGreenBold + "Playing " + C.gray + "- " + C.purple + getPlayerCount() + C.gold + "/" + C.purple
							+ getType().getAbsMax());
		}

		if (getStateCount() == 15)
		{
			setState(GameState.INGAME);
		}
	}

	protected void onIngameCount()
	{
		if (!_endUnderMin)
			return;

		if (getPlayerCount() >= _type.getAbsMin())
			return;

		sendMessage(C.red + "There are not enough players for the game to continue! Ending...");
		setState(GameState.POSTGAME);
	}

	protected void onEndingCount()
	{
		boolean shouldSay = false;

		if (getStateCount() == 1)
		{
			String[] text = getEndGameText();
			if (text != null)
			{
				periodMessage(text);
			}
		}

		if (getStateCount() == 8)
		{
			rewardsMessage();
		}

		if (getStateCount() == 14)
			shouldSay = true;

		if (getStateCount() >= 19 && getStateCount() != 24 && getStateCount() != 25)
			shouldSay = true;

		if (shouldSay)
		{
			periodMessage(C.red + "The game has ended.", C.whiteBold + "You will be returned to Hub in...",
					C.yellowBold + (24 - getStateCount()) + C.whiteBold + " Seconds.");
		}

		if (getStateCount() == 25)
		{
			setState(GameState.CLEANING);
			getManager().endGame(this, true);
		}
	}
}
