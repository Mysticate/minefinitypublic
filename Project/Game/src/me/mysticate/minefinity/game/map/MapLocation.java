package me.mysticate.minefinity.game.map;

import org.bukkit.Location;
import org.bukkit.World;

public class MapLocation
{
	private double _x;
	private double _y;
	private double _z;

	public MapLocation(double x, double y, double z)
	{
		_x = x;
		_y = y;
		_z = z;
	}

	public Location getLocation(World world)
	{
		return new Location(world, _x, _y, _z);
	}

	public double getX()
	{
		return _x;
	}

	public double getY()
	{
		return _y;
	}

	public double getZ()
	{
		return _z;
	}
}
