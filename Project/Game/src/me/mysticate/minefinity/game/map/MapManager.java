package me.mysticate.minefinity.game.map;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.common.util.UtilFile;
import me.mysticate.minefinity.core.common.util.UtilMath;
import me.mysticate.minefinity.core.file.YamlF;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.game.GameType;

public class MapManager extends Manager
{
	private SectorManager _sectorManager;

	private final String _path = "maps";
	private final String[] _required = new String[] { "name", "creator", "lobby", "data" };

	public MapManager(JavaPlugin plugin, SectorManager sectorManager)
	{
		super(plugin, "Map");

		_sectorManager = sectorManager;

		makeFolders();
		clearPreviousWorlds();
	}

	public Map getNextMap(GameType game)
	{
		makeFolders();

		if (game == null)
			return null;

		ArrayList<File> best = new ArrayList<File>();
		for (File folder : folder(game).listFiles())
		{
			if (!folder.isDirectory() || folder.listFiles().length < 2)
				continue;

			best.add(folder);
		}

		if (best.isEmpty())
			return null;

		File mapFolder = UtilMath.getRandom(best);
		if (mapFolder == null)
			return null;

		File data = null;
		File world = null;
		for (File file : mapFolder.listFiles())
		{
			if (file.getName().equals("data.yml"))
				data = file;

			if (file.getName().equals("world"))
				world = file;
		}

		if (data == null || world == null || data.isDirectory() || !world.isDirectory())
			return null;

		YamlConfiguration config = YamlConfiguration.loadConfiguration(data);

		Map map = buildMap(game, config, world);

		if (map == null)
			return null;

		_sectorManager.setSector(map.getWorld(), game.getSector());

		return map;
	}

	private void makeFolders()
	{
		File mapFolder = new File(_path);

		if (!mapFolder.exists())
			mapFolder.mkdirs();

		for (GameType type : GameType.values())
		{
			File gameFolder = folder(type);

			if (!gameFolder.exists())
				gameFolder.mkdirs();
		}
	}

	private void clearPreviousWorlds()
	{
		File file = new File(".");

		for (File cur : file.listFiles())
		{
			if (cur.getName().startsWith("Game_"))
			{
				UtilFile.delete(cur);
			}
		}
	}

	private Map buildMap(GameType game, YamlConfiguration config, File world)
	{
		for (String required : _required)
		{
			if (!config.contains(required))
				return null;
		}

		String name = config.getString("name");
		String creator = config.getString("creator");

		MapLocation lobby = parse(config.getString("lobby"));

		HashMap<String, List<MapLocation>> data = new HashMap<String, List<MapLocation>>();
		for (String point : config.getConfigurationSection("data").getKeys(false))
			data.put(point, new ArrayList<MapLocation>());

		for (String point : data.keySet())
		{
			List<String> keys = config.getStringList(YamlF.path("data", point));
			if (keys == null || keys.isEmpty())
				continue;

			for (String location : keys)
			{
				MapLocation map = parse(location);
				if (map == null)
					continue;

				data.get(point).add(map);
			}
		}

		if (name == null || creator == null || lobby == null || lobby == null)
			return null;

		World newWorld = createWorld(game, world, name);
		if (newWorld == null)
			return null;

		return new Map(name, creator, newWorld, lobby, data);
	}

	private File folder(GameType type)
	{
		return new File(_path, type.getAbv());
	}

	public MapLocation parse(String ser)
	{
		String[] split = ser.split(",");
		if (split.length != 3)
			return null;

		try
		{
			return new MapLocation(Double.valueOf(split[0]), Double.valueOf(split[1]), Double.valueOf(split[2]));
		} catch (Exception ex)
		{
			System.out.println("An error occured while parsing a map location.");
			ex.printStackTrace();
			return null;
		}
	}

	private World createWorld(GameType game, File source, String name)
	{
		try
		{
			String worldName = null;
			File target = null;
			for (int i = 0; i < 10000; i++)
			{
				worldName = "Game_" + game.getAbv() + i + "_" + name;
				target = new File(worldName);

				if (target.exists())
					continue;

				target.mkdir();
				break;
			}

			if (!target.exists())
			{
				System.out.println("Failed to generate a valid world folder for " + name + "!");
				return null;
			}

			UtilFile.copy(source, target);

			return Bukkit.createWorld(new WorldCreator(worldName).generateStructures(false).type(WorldType.FLAT));
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	public void killWorld(Map map)
	{
		File worldFolder = map.getWorld().getWorldFolder();
		Bukkit.unloadWorld(map.getWorld(), false);

		UtilFile.delete(worldFolder);
	}
}
