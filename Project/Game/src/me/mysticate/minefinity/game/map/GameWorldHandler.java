package me.mysticate.minefinity.game.map;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.combat.event.CustomDamageEvent;
import me.mysticate.minefinity.core.combat.event.CustomDeathEvent;
import me.mysticate.minefinity.core.world.WorldHandler;
import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameState;
import me.mysticate.minefinity.game.event.GameStateChangeEvent;

public abstract class GameWorldHandler<G extends Game> extends WorldHandler
{
	private G _game;

	public GameWorldHandler(JavaPlugin plugin, G game, World world)
	{
		super(plugin, world);

		_game = game;
	}

	public G getGame()
	{
		return _game;
	}

	@EventHandler
	public void onLobby(GameStateChangeEvent event)
	{
		if (event.getState() == GameState.INGAME)
		{
			EntityDamage = true;
			ItemDrop = true;
			ItemPickup = true;
			BlockBreak = true;
			BlockBurn = true;
			BlockDamage = true;
			BlockFade = true;
			BucketFill = true;
			BucketEmpty = true;
			EntityIgnite = true;
			PlayerIgnite = true;
			PlayerShootBow = true;
			EntityShootBow = true;
			HungerChange = true;
			PaintingBreak = true;
			LiquidFlow = true;
			SaturationChange = true;
			EntitySpawn = true;

			liveSettings();
		} else
		{
			EntityDamage = false;
			ItemDrop = false;
			ItemPickup = false;
			BlockBreak = false;
			BlockBurn = false;
			BlockDamage = false;
			BlockFade = false;
			BlockFade = false;
			BucketFill = false;
			BucketEmpty = false;
			EntityIgnite = false;
			PlayerIgnite = false;
			PlayerShootBow = false;
			EntityShootBow = false;
			HungerChange = false;
			PaintingBreak = false;
			LiquidFlow = false;
			SaturationChange = false;
			EntitySpawn = false;
		}
	}

	protected abstract void liveSettings();

	@EventHandler
	public void onLobbyDeath(CustomDeathEvent event)
	{
		if (getGame().getState() == GameState.INGAME)
			return;

		if (!getGame().hasPlayer(event.getKilled()))
			return;

		event.getEvent().setDroppedExp(0);

		event.getEvent().getDrops().clear();

		if (getGame().getState() == GameState.LOBBY)
		{
			getGame().lobbyItems(event.getKilled());
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onDamage(CustomDamageEvent event)
	{
		if (getGame().hasPlayer(event.getDamager()))
		{
			if (event.isPlayer() && !getGame().hasPlayer(event.getDamagee()))
			{
				event.setCancelled(true);
			}
		} else if (getGame().hasPlayer(event.getDamagee()))
		{
			if (event.byPlayer() && !getGame().hasPlayer(event.getDamager()))
			{
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onPrepareFreeze(PlayerMoveEvent event)
	{
		if (getGame().getState() != GameState.TELEPORTING && getGame().getState() != GameState.PREGAME)
			return;

		Location a = event.getFrom();
		Location b = event.getTo();

		if (a.getX() != b.getX() || a.getZ() != b.getZ())
		{
			event.getPlayer().teleport(a);
		}
	}
}
