package me.mysticate.minefinity.game.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;

public class Map
{
	private String _name;
	private String _creator;

	private World _world;

	private MapLocation _spawn;

	private HashMap<String, List<MapLocation>> _data = new HashMap<String, List<MapLocation>>();

	private List<MapLocation> _spawns;
	private Iterator<MapLocation> _spawnIterator;

	public Map(String name, String creator, World world, MapLocation spawn, HashMap<String, List<MapLocation>> data)
	{
		_name = name;
		_creator = creator;

		_world = world;

		_spawn = spawn;

		_data = data;
		_spawns = getData("spawns");
	}

	public String getName()
	{
		return _name;
	}

	public String getCreator()
	{
		return _creator;
	}

	public World getWorld()
	{
		return _world;
	}

	public Location getSpawn()
	{
		return _spawn.getLocation(getWorld());
	}

	public List<MapLocation> getData(String key)
	{
		if (!_data.containsKey(key))
			return new ArrayList<MapLocation>();

		return _data.get(key);
	}

	public List<MapLocation> getSpawns()
	{
		return _spawns;
	}

	public Location nextSpawn()
	{
		if (_spawnIterator == null || !_spawnIterator.hasNext())
			_spawnIterator = _spawns.iterator();

		if (!_spawnIterator.hasNext())
			return null;

		return _spawnIterator.next().getLocation(getWorld());
	}

	public void resetSpawnTeleporter()
	{
		_spawnIterator = null;
	}
}