package me.mysticate.minefinity.game.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameState;

public class GameStateChangeEvent extends Event
{
	private static HandlerList _handlers = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	private Game _game;
	private GameState _state;
	private GameState _previous;

	public GameStateChangeEvent(Game game, GameState state, GameState previous)
	{
		_game = game;
		_state = state;
		_previous = previous;
	}

	public Game getGame()
	{
		return _game;
	}

	public GameState getState()
	{
		return _state;
	}

	public GameState getPreviousState()
	{
		return _previous;
	}
}
