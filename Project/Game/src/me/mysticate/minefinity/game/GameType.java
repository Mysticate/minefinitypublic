package me.mysticate.minefinity.game;

import me.mysticate.minefinity.core.sector.Sector;
import me.mysticate.minefinity.core.settings.SettingPool;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.handlers.BattleArenaSectorHandler;

public enum GameType
{
	BATTLE_ARENA(new GameBuilder()
	{
		@Override
		public Game newInstance(GameManager manager, int id)
		{
			return new BattleArena(manager, id);
		}
	}, new GameSectorBuilder()
	{
		@Override
		public GameSectorHandler newInstance(GameManager manager)
		{
			return new BattleArenaSectorHandler(manager);
		}
	}, Sector.BATTLE_ARENA, SettingPool.BATTLE_ARENA, "Battle Arena", "BA", 2, 4, 12, 16),

	;

	private GameBuilder _builder;
	private GameSectorBuilder _sectorBuilder;

	private Sector _sector;
	private SettingPool _settings;

	private String _display;
	private String _abv;

	private int _absMin;
	private int _min;
	private int _max;
	private int _absMax;

	private GameType(GameBuilder builder, GameSectorBuilder sectorBuilder, Sector sector, SettingPool settings,
			String display, String abv, int absMin, int min, int max, int absMax)
	{
		_builder = builder;
		_sectorBuilder = sectorBuilder;

		_sector = sector;
		_settings = settings;

		_display = display;
		_abv = abv;

		_absMin = absMin;
		_min = min;
		_max = max;
		_absMax = absMax;
	}

	public Sector getSector()
	{
		return _sector;
	}

	public SettingPool getSettings()
	{
		return _settings;
	}

	public String getDisplay()
	{
		return _display;
	}

	public String getAbv()
	{
		return _abv;
	}

	public int getAbsMin()
	{
		return _absMin;
	}

	public int getMin()
	{
		return _min;
	}

	public int getMax()
	{
		return _max;
	}

	public int getAbsMax()
	{
		return _absMax;
	}

	public Game newInstance(GameManager manager, int id)
	{
		return _builder.newInstance(manager, id);
	}

	public GameSectorHandler newSectorHandler(GameManager manager)
	{
		return _sectorBuilder.newInstance(manager);
	}

	public static GameType getByAbv(String abv)
	{
		for (GameType game : GameType.values())
		{
			if (game.getAbv().equals(abv))
				return game;
		}
		return null;
	}

	private static interface GameBuilder
	{
		public Game newInstance(GameManager manager, int id);
	}

	private static interface GameSectorBuilder
	{
		public GameSectorHandler newInstance(GameManager manager);
	}
}
