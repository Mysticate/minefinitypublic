package me.mysticate.minefinity.game;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.updater.event.TimeEvent;

public abstract class IScoreboardHandler<G extends Game>
{
	private G _game;

	public IScoreboardHandler(G game)
	{
		_game = game;
	}

	public G getGame()
	{
		return _game;
	}

	public abstract void update(TimeEvent event, Player player);
}
