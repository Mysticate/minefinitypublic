package me.mysticate.minefinity.game.games.battlearena.kits.pig.entity;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_9_R1.CraftWorld;

import me.mysticate.minefinity.core.common.util.UtilEntity;
import me.mysticate.minefinity.core.entity.ISpawnable;
import net.minecraft.server.v1_9_R1.Entity;
import net.minecraft.server.v1_9_R1.EntityHuman;
import net.minecraft.server.v1_9_R1.EntityLightning;
import net.minecraft.server.v1_9_R1.EntityPig;
import net.minecraft.server.v1_9_R1.GenericAttributes;
import net.minecraft.server.v1_9_R1.MathHelper;

public class PigEntity extends EntityPig implements ISpawnable
{
	private int _magic;

	private boolean bx;
	private int bz;
	private int bA;

	public PigEntity(org.bukkit.World world, int magic)
	{
		super(((CraftWorld) world).getHandle());

		_magic = magic;
	}

	@Override
	protected void r()
	{

	}

	@Override
	protected void initAttributes()
	{
		super.initAttributes();

		getAttributeInstance(GenericAttributes.maxHealth).setValue(1.0D);
	}

	@Override
	public boolean cK()
	{
		return bt() instanceof EntityHuman;
	}

	public void g(float f, float f1)
	{
		Entity entity = bu().isEmpty() ? null : (Entity) bu().get(0);

		if (isVehicle() && cK())
		{
			lastYaw = yaw = entity.yaw;
			pitch = entity.pitch * 0.5F;
			setYawPitch(yaw, pitch);
			aO = aM = yaw;
			P = 1.0F;
			aQ = ck() * 0.1F;
			if (bx())
			{
				float f2 = (float) getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).getValue() * 0.225F;
				f2 += (20 / (10 - _magic)) + .2;

				if (bx)
				{
					if (bz++ > bA)
					{
						bx = false;
					}

					f2 += f2 * 1.15F * MathHelper.sin((float) bz / (float) bA * 3.1415927F);
				}

				l(f2);
				super.g(0.0F, 1.0F);
			} else
			{
				motX = 0.0D;
				motY = 0.0D;
				motZ = 0.0D;
			}

			aE = aF;
			double d0 = locX - lastX;
			double d1 = locZ - lastZ;
			float f3 = MathHelper.sqrt(d0 * d0 + d1 * d1) * 4.0F;

			if (f3 > 1.0F)
			{
				f3 = 1.0F;
			}

			aF += (f3 - aF) * 0.4F;
			aG += aF;

			entity.getWorld().getWorld().spigot().playEffect(getBukkitEntity().getLocation(), Effect.FLAME);
		}
	}

	@Override
	protected void dropEquipment(boolean flag, int i)
	{

	}

	@Override
	public void onLightningStrike(EntityLightning entitylightning)
	{

	}

	@Override
	public boolean da()
	{
		if (bx)
		{
			return false;
		} else
		{
			bx = true;
			bz = 0;
			bA = getRandom().nextInt(841) + 140;
			return true;
		}
	}

	@Override
	public void spawn(Location loc)
	{
		UtilEntity.registerEntity("Escape Pig", getClass());

		setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		getWorld().addEntity(this);
	}
}
