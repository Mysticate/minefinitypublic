package me.mysticate.minefinity.game.games.battlearena.handlers;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.map.GameWorldHandler;

public class BattleArenaWorldHandler extends GameWorldHandler<BattleArena>
{
	public BattleArenaWorldHandler(JavaPlugin plugin, BattleArena game, World world)
	{
		super(plugin, game, world);

		TimeSet = 6000;
	}

	@Override
	public Location getJoinLocation(Player player)
	{
		return getGame().getMap().getSpawn();
	}

	@Override
	public void onJoin(Player player)
	{

	}

	@Override
	public void onQuit(Player player)
	{

	}

	@Override
	protected void liveSettings()
	{
		RemoveArrows = true;
		EntityDamage = true;
		EntityDamageByEntity = true;
		ItemDrop = false;
		ItemPickup = false;
		BlockBreak = false;
		BlockBurn = false;
		BlockDamage = false;
		BlockFade = false;
		BucketFill = false;
		BucketEmpty = false;
		HungerChange = false;
		PaintingBreak = false;
		LiquidFlow = false;
		SaturationChange = false;
		EntitySpawn = false;
		PlayerIgnite = true;
	}

	@EventHandler
	public void onClick(InventoryClickEvent event)
	{
		if (event.getWhoClicked().getWorld() == getWorld())
			if (event.getSlotType() == SlotType.ARMOR)
				event.setCancelled(true);
	}

	@EventHandler
	public void onInteract(PlayerInteractAtEntityEvent event)
	{
		if (event.getPlayer().getWorld() == getWorld())
			event.setCancelled(true);
	}
}
