package me.mysticate.minefinity.game.games.battlearena;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.kit.kit.IKit;
import me.mysticate.minefinity.game.kit.kit.IKitBuilder;
import me.mysticate.minefinity.game.kit.kit.Kit;

public enum BattleArenaKit implements IKit<BattleArena>
{
	KNIGHT(new IKitBuilder<BattleArena>()
	{
		@Override
		public Kit<BattleArena> newInstance(GameManager manager, BattleArena game, Player player)
		{
			return new me.mysticate.minefinity.game.games.battlearena.kits.knight.KitKnight(manager, game, player);
		}
	}, 1, true, 0, 5, 3, 3, "Knight", new String[] { "After waiting hundreds of years", "for his love to come home,",
			"he got bored and decided to", "hit the ground running." }, Material.GOLD_SWORD, (byte) 0),

	SNIPER(new IKitBuilder<BattleArena>()
	{
		@Override
		public Kit<BattleArena> newInstance(GameManager manager, BattleArena game, Player player)
		{
			return new me.mysticate.minefinity.game.games.battlearena.kits.sniper.KitSniper(manager, game, player);
		}
	}, 2, false, 1000, 5, 4, 2, "Sniper", new String[] { "Alright everyone, let's", "make sure we're ready to go!",
			"Sword? Check.", "Armor, check!", "Bow...", "...", "CARL!!" }, Material.BOW, (byte) 0),

	PIG(new IKitBuilder<BattleArena>()
	{
		@Override
		public Kit<BattleArena> newInstance(GameManager manager, BattleArena game, Player player)
		{
			return new me.mysticate.minefinity.game.games.battlearena.kits.pig.KitPig(manager, game, player);
			// return null;
		}
	}, 3, false, 2500, 4, 3, 1,
			"Piggy", new String[] { "Racing through the fields,", "plains, forests, and wilderness,",
					"what could it be?", "Why, none other than the", "infamous pig escapist!" },
			Material.PORK, (byte) 0),

	;

	private IKitBuilder<BattleArena> _builder;
	private int _id;
	private boolean _isDefault;
	private int _cost;
	private int _maxLevel;
	private int _maxMagic;
	private int _magicStart;
	private String _display;
	private String[] _lore;
	private Material _mat;
	private byte _data;

	private BattleArenaKit(IKitBuilder<BattleArena> builder, int id, boolean isDefault, int cost, int maxLevel,
			int maxMagic, int magicStart, String display, String[] lore, Material mat, byte data)
	{
		_builder = builder;
		_id = id;
		_isDefault = isDefault;
		_cost = cost;
		_maxLevel = maxLevel;
		_maxMagic = maxMagic;
		_magicStart = magicStart;
		_display = display;
		_lore = lore;
		_mat = mat;
		_data = data;
	}

	@Override
	public IKitBuilder<BattleArena> getKitBuilder()
	{
		return _builder;
	}

	@Override
	public int getId()
	{
		return _id;
	}

	@Override
	public boolean isFree()
	{
		return _isDefault;
	}

	@Override
	public int getUnlockCost()
	{
		return _cost;
	}

	@Override
	public int getMaxLevel()
	{
		return _maxLevel;
	}

	@Override
	public int getMaxMagic()
	{
		return _maxMagic;
	}

	@Override
	public int getMagicPurchaseStart()
	{
		return _magicStart;
	}

	@Override
	public Material getDisplayMat()
	{
		return _mat;
	}

	@Override
	public byte getDisplayData()
	{
		return _data;
	}

	@Override
	public String getDisplayName()
	{
		return _display;
	}

	@Override
	public String[] getDisplayLore()
	{
		return _lore;
	}

	public static BattleArenaKit getById(int id)
	{
		for (BattleArenaKit kit : BattleArenaKit.values())
		{
			if (kit.getId() == id)
				return kit;
		}

		return null;
	}
}
