package me.mysticate.minefinity.game.games.battlearena.scoreboard;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.game.IScoreboardHandler;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.kit.kit.Kit;

public class BAScoreboardLobby extends IScoreboardHandler<BattleArena>
{
	private HashMap<Player, Integer> _counts = new HashMap<Player, Integer>();

	public BAScoreboardLobby(BattleArena game)
	{
		super(game);
	}

	@Override
	public void update(TimeEvent event, Player player)
	{
		if (!_counts.containsKey(player))
			_counts.put(player, 0);

		Kit<?> kit = getGame().getKit(player);

		HudManager.get(player).scoreboardAddText("           " + C.white + getGame().getPlayerCount() + C.gray + " / "
				+ C.white + getGame().getType().getMax());
		HudManager.get(player).scoreboardAddBlank();
		HudManager.get(player).scoreboardAddText(C.goldBold + "Selected Kit");
		HudManager.get(player).scoreboardAddText(C.white + kit.getDisplayName());
		HudManager.get(player).scoreboardAddBlank();
		HudManager.get(player).scoreboardAddText(C.goldBold + "Level");
		HudManager.get(player).scoreboardAddText(C.aqua + "Kit " + C.white + kit.getLevel());
		HudManager.get(player).scoreboardAddText(C.aqua + "Skill " + C.white + kit.getLevel(Kit.Magic));
		HudManager.get(player).scoreboardAddBlank();
		HudManager.get(player)
				.scoreboardAddText(C.yellowBold + "Waiting" + StringUtils.repeat('.', _counts.get(player)));

		HudManager.get(player).scoreboardBuild();

		HudManager.get(player).setPrefix("");
		HudManager.get(player).setSuffix("");

		if (event.getType() == UpdateType.SEC_HALF)
			_counts.put(player, _counts.get(player) == 3 ? 0 : _counts.get(player) + 1);
	}
}
