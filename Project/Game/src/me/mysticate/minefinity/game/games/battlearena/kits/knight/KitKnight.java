package me.mysticate.minefinity.game.games.battlearena.kits.knight;

import org.bukkit.Effect;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.cooldown.Cooldown;
import me.mysticate.minefinity.core.cooldown.CooldownEvent;
import me.mysticate.minefinity.core.cooldown.CooldownPool;
import me.mysticate.minefinity.core.energy.IEnergyDisplay;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.BattleArenaKit;
import me.mysticate.minefinity.game.games.battlearena.kits.knight.levels.Knight1;
import me.mysticate.minefinity.game.games.battlearena.kits.knight.levels.Knight2;
import me.mysticate.minefinity.game.games.battlearena.kits.knight.levels.Knight3;
import me.mysticate.minefinity.game.games.battlearena.kits.knight.levels.Knight4;
import me.mysticate.minefinity.game.games.battlearena.kits.knight.levels.Knight5;
import me.mysticate.minefinity.game.kit.kit.Kit;

public class KitKnight extends Kit<BattleArena> implements IEnergyDisplay
{
	private final long _active = 4000;
	private final long _recharge = 20000;

	public KitKnight(GameManager manager, BattleArena game, Player player)
	{
		super(manager, game, player, BattleArenaKit.KNIGHT);

		setLoadout(1, new Knight1(game, this));
		setLoadout(2, new Knight2(game, this));
		setLoadout(3, new Knight3(game, this));
		setLoadout(4, new Knight4(game, this));
		setLoadout(5, new Knight5(game, this));
	}

	@Override
	public void onEnable()
	{

	}

	@Override
	public void onDisable(KitDisableReason reason)
	{
		getManager().getItemManager().getPlayer(getPlayer()).getItems(getKeyIdentifier()).clear();
	}

	@Override
	protected void giveItemsCustom()
	{
		IM.cooldown().reset(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Recharge"));
		IM.cooldown().reset(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Active"));

		IM.cooldown().use(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Recharge"), _recharge);
		IM.cooldown().getPlayer(getPlayer()).getCooldown(CooldownPool.BATTLE_ARENA, recharge("Recharge"))
				.setName("Speed Cutter");
	}

	public void activeAbility()
	{
		if (!getGame().inGame())
			return;

		if (getMagic() < 1)
			return;

		if (IM.cooldown().getPlayer(getPlayer()).hasCooldown(CooldownPool.BATTLE_ARENA, recharge("Recharge")))
			return;

		IM.cooldown().use(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Active"),
				(long) (_active * (1 + (.2 * getMagic()))));
		getPlayer().setWalkSpeed(.4F);
	}

	@EventHandler
	public void onCooldown(CooldownEvent event)
	{
		if (event.getName().equals(recharge("Active")))
		{
			IM.cooldown().getPlayer(event.getPlayer()).forceAddCooldown(CooldownPool.BATTLE_ARENA, recharge("Recharge"),
					new Cooldown(_recharge));
			IM.cooldown().getPlayer(getPlayer()).getCooldown(CooldownPool.BATTLE_ARENA, recharge("Recharge"))
					.setName("Speed Cutter");

			event.getPlayer().setWalkSpeed(.2F);
		}
	}

	@EventHandler
	public void onTick(TimeEvent event)
	{
		if (event.getType() != UpdateType.TICK)
			return;

		if (!getGame().inGame())
			return;

		if (IM.cooldown().getPlayer(getPlayer()).hasCooldown(CooldownPool.BATTLE_ARENA, recharge("Active")))
		{
			getPlayer().getWorld().spigot().playEffect(getPlayer().getLocation().clone().add(0, .2, 0), Effect.LAVADRIP,
					0, 0, .5F, 0F, .5F, 0F, 3, 30);
		}
	}

	@Override
	public float getExpValue()
	{
		if (!getGame().inGame())
			return 0;

		if (IM.cooldown().getPlayer(getPlayer()).hasCooldown(CooldownPool.BATTLE_ARENA, recharge("Active")))
		{
			return (float) Math.min(.999, 1 - IM.cooldown().getPlayer(getPlayer())
					.getCooldown(CooldownPool.BATTLE_ARENA, recharge("Active")).asFloat());
		} else if (IM.cooldown().getPlayer(getPlayer()).hasCooldown(CooldownPool.BATTLE_ARENA, recharge("Recharge")))
		{
			return IM.cooldown().getPlayer(getPlayer()).getCooldown(CooldownPool.BATTLE_ARENA, recharge("Recharge"))
					.asFloat();
		}
		return .999F;
	}

	@Override
	public boolean shouldFlashWhenFull()
	{
		return true;
	}
}
