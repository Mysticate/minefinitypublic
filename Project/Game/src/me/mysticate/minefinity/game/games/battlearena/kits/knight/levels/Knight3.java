package me.mysticate.minefinity.game.games.battlearena.kits.knight.levels;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.item.InteractAction;
import me.mysticate.minefinity.core.item.ItemCallback;
import me.mysticate.minefinity.core.item.UseableItem;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.kits.knight.KitKnight;
import me.mysticate.minefinity.game.kit.kit.ILoadoutLevel;

public class Knight3 extends ILoadoutLevel<BattleArena, KitKnight>
{
	public Knight3(BattleArena game, KitKnight kit)
	{
		super(game, kit);
	}

	@Override
	public void giveItems()
	{
		String swordName = getSetting("name-sword");
		String bowName = getSetting("name-bow");

		ItemStack sword = UtilItemStack.create(Material.WOOD_SWORD, (byte) 0, 1, C.green + "Wooden Sword");

		if (getKit().getMagic() > 0)
		{
			UtilItemStack.name(sword, C.green + "Wooden Battle Sword" + C.gray + C.italic + " Right Click");
			UtilItemStack
					.setLore(sword,
							new String[] { " ",
									C.yellow + "Right Click" + C.white + " for " + C.green + "Speed Cutter" + C.white
											+ ".",
									" ", C.gray + "Engulf yourself in pure", C.gray + "energy, doubling your speed",
									C.gray + "whilst on the ground.", });
		}

		UtilItemStack.unbreakable(sword);

		if (swordName != null)
			UtilItemStack.name(sword, C.goldBold + swordName);

		UseableItem item = new UseableItem(sword, new ItemCallback()
		{
			@Override
			public void onClick(Player player, Action type)
			{
				getKit().activeAbility();
			}
		});

		item.setAllowInteract(true);
		item.setType(InteractAction.RIGHT);

		getGame().getManager().getItemManager().getPlayer(getPlayer()).addItem(getKit().getKeyIdentifier(), item);
		getGame().getManager().getEnergyManager().getPlayer(getPlayer()).add(item, getKit());

		ItemStack bow = UtilItemStack.create(Material.BOW, (byte) 0, 1, C.green + "Basic Longbow");
		UtilItemStack.unbreakable(bow);

		if (bowName != null)
			UtilItemStack.name(bow, C.goldBold + bowName);

		ItemStack boots = UtilItemStack.create(Material.GOLD_BOOTS, (byte) 0, 1, C.green + "Golden Boots");
		UtilItemStack.unbreakable(boots);

		ItemStack chestplate = UtilItemStack.create(Material.LEATHER_CHESTPLATE, (byte) 0, 1,
				C.green + "Leather Chestplate");
		UtilItemStack.unbreakable(chestplate);

		ItemStack helmet = UtilItemStack.create(Material.GOLD_HELMET, (byte) 0, 1, C.green + "Gold Helmet");
		UtilItemStack.unbreakable(helmet);

		setItem(0, sword);
		setItem(1, bow);
		setItem(9, new ItemStack(Material.ARROW, 3));

		setArmor(0, boots);
		setArmor(2, chestplate);
		setArmor(3, helmet);
	}
}
