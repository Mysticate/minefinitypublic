package me.mysticate.minefinity.game.games.battlearena.kits.sniper;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;

import me.mysticate.minefinity.core.combat.event.CustomDamageEvent;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.BattleArenaKit;
import me.mysticate.minefinity.game.games.battlearena.kits.sniper.levels.Sniper1;
import me.mysticate.minefinity.game.games.battlearena.kits.sniper.levels.Sniper2;
import me.mysticate.minefinity.game.games.battlearena.kits.sniper.levels.Sniper3;
import me.mysticate.minefinity.game.games.battlearena.kits.sniper.levels.Sniper4;
import me.mysticate.minefinity.game.games.battlearena.kits.sniper.levels.Sniper5;
import me.mysticate.minefinity.game.kit.kit.Kit;

public class KitSniper extends Kit<BattleArena>
{
	public KitSniper(GameManager manager, BattleArena game, Player player)
	{
		super(manager, game, player, BattleArenaKit.SNIPER);

		setLoadout(1, new Sniper1(game, this));
		setLoadout(2, new Sniper2(game, this));
		setLoadout(3, new Sniper3(game, this));
		setLoadout(4, new Sniper4(game, this));
		setLoadout(5, new Sniper5(game, this));
	}

	@Override
	public void onEnable()
	{

	}

	@Override
	public void onDisable(KitDisableReason reason)
	{

	}

	@Override
	protected void giveItemsCustom()
	{

	}

	@EventHandler(ignoreCancelled = true)
	public void onDamage(CustomDamageEvent event)
	{
		if (!event.byPlayer())
			return;

		if (event.getDamager() != getPlayer())
			return;

		if (!event.hasProjectile())
			return;

		if (!(event.getProjectile() instanceof Arrow))
			return;

		if (getLevel() < 2)
			return;

		if (event.getDamagee().getLocation().distance(event.getDamager().getLocation()) < getDistance())
			return;

		event.ignoreAllModifiers();
		event.setDamage(DamageModifier.BASE, 999);

		event.getDamagee().sendMessage(
				C.redBold + ">>" + C.yellow + event.getDamager().getName() + C.gray + " hit you with a headshot!");
		event.getDamager().sendMessage(C.redBold + ">>" + C.gray + " You dealt critical headshot damage to " + C.yellow
				+ event.getDamagee().getName() + C.gray + ".");
	}

	public int getDistance()
	{
		return 30 - (4 * getMagic());
	}
}
