package me.mysticate.minefinity.game.games.battlearena;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.combat.event.CustomDeathEvent;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.common.util.UtilMath;
import me.mysticate.minefinity.core.common.util.UtilPlayer;
import me.mysticate.minefinity.core.cooldown.CooldownPool;
import me.mysticate.minefinity.core.currency.CurrencyType;
import me.mysticate.minefinity.core.settings.SettingPool;
import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameState;
import me.mysticate.minefinity.game.GameType;
import me.mysticate.minefinity.game.data.GamePlayerDataManager.ScoreWrapper;
import me.mysticate.minefinity.game.games.battlearena.handlers.BattleArenaWorldHandler;
import me.mysticate.minefinity.game.games.battlearena.scoreboard.BAScoreboardCountdown;
import me.mysticate.minefinity.game.games.battlearena.scoreboard.BAScoreboardLive;
import me.mysticate.minefinity.game.games.battlearena.scoreboard.BAScoreboardLobby;
import me.mysticate.minefinity.game.games.battlearena.scoreboard.BAScoreboardPostGame;
import me.mysticate.minefinity.game.games.battlearena.scoreboard.BAScoreboardPreGame;
import me.mysticate.minefinity.game.games.battlearena.scoreboard.BAScoreboardTeleporting;
import me.mysticate.minefinity.game.kit.kit.Kit;
import me.mysticate.minefinity.game.lobbyitems.ItemSwitchBall;
import me.mysticate.minefinity.game.map.GameWorldHandler;

public class BattleArena extends Game
{
	public final int END_TIME = 300;

	public BattleArena(GameManager manager, int id)
	{
		super(manager, GameType.BATTLE_ARENA, id);

		setKits(BattleArenaKit.values());

		setScoreboardHandler(GameState.LOBBY, new BAScoreboardLobby(this));
		setScoreboardHandler(GameState.COUNTDOWN, new BAScoreboardCountdown(this));
		setScoreboardHandler(GameState.TELEPORTING, new BAScoreboardTeleporting(this));
		setScoreboardHandler(GameState.PREGAME, new BAScoreboardPreGame(this));
		setScoreboardHandler(GameState.INGAME, new BAScoreboardLive(this));
		setScoreboardHandler(GameState.POSTGAME, new BAScoreboardPostGame(this));

		this.setEndUnderMin(false);
	}

	@Override
	protected GameWorldHandler<?> newWorldHandler(World world)
	{
		return new BattleArenaWorldHandler(getPlugin(), this, world);
	}

	@Override
	protected Kit<?> newKitInstance(int kitId, Player player)
	{
		BattleArenaKit kit = BattleArenaKit.getById(kitId);
		if (kit == null)
			return null;

		return kit.getKitBuilder().newInstance(getManager(), this, player);
	}

	@Override
	protected void onJoin(Player player)
	{
		getManager().getScoreManager().getPlayer(player).addScore(getType(), "kills", 0);
	}

	@Override
	protected void onQuit(Player player)
	{
		IM.cooldown().getPlayer(player).clear(CooldownPool.BATTLE_ARENA);
	}

	@Override
	public void giveLobbyItems(Player player)
	{
		giveLobbyItem(player, new ItemSwitchBall());
	}

	@EventHandler
	public void onDeath(CustomDeathEvent event)
	{
		if (getState() != GameState.INGAME)
			return;

		if (!hasPlayer(event.getKilled()))
			return;

		event.getEvent().getDrops().clear();
		event.getEvent().setDroppedExp(0);

		UtilPlayer.reset(event.getKilled(), false);

		getKit(event.getKilled()).giveItems();

		event.getKilled().teleport(UtilMath.getRandom(getMap().getSpawns()).getLocation(getMap().getWorld()));

		Entity killer = event.getKillingEvent().getDamager();
		if (!(killer instanceof Player))
			return;

		if (UtilPlayer.validatePlayer((Player) killer) && hasPlayer(killer) && killer != event.getKilled())
		{
			Player playerKiller = (Player) killer;

			String weaponName = null;
			if (UtilItemStack.isBow(event.getKillingEvent().getItem()))
				weaponName = getManager().getSettingsManager().getPlayer(playerKiller)
						.getSetting(SettingPool.BATTLE_ARENA, "name-bow");

			if (UtilItemStack.isSword(event.getKillingEvent().getItem()))
				weaponName = getManager().getSettingsManager().getPlayer(playerKiller)
						.getSetting(SettingPool.BATTLE_ARENA, "name-sword");

			if (UtilItemStack.isAxe(event.getKillingEvent().getItem()))
				weaponName = getManager().getSettingsManager().getPlayer(playerKiller)
						.getSetting(SettingPool.BATTLE_ARENA, "name-axe");

			if (weaponName == null)
			{
				killer.sendMessage(C.dBlueBold + "Kill: " + C.white + "You killed " + C.yellow
						+ event.getKilled().getName() + C.white + " with " + C.purple
						+ UtilItemStack.getName(event.getKillingEvent().getItem().getType()) + C.white + ".");
				event.getKilled()
						.sendMessage(C.dBlueBold + "Death: " + C.white + "Killed by " + C.yellow + killer.getName()
								+ C.white + " with " + C.purple
								+ UtilItemStack.getName(event.getKillingEvent().getItem().getType()) + C.white + ".");
			} else
			{
				killer.sendMessage(
						C.dBlueBold + "Kill: " + C.white + "You killed " + C.yellow + event.getKilled().getName()
								+ C.white + " with " + C.goldBold + weaponName + " " + C.gray + C.italic
								+ UtilItemStack.getName(event.getKillingEvent().getItem().getType()) + C.white + ".");
				killer.sendMessage(C.dBlueBold + "Death: " + C.white + "Killed by " + C.yellow + killer.getName()
						+ C.white + " with " + C.goldBold + weaponName + " " + C.gray + C.italic
						+ UtilItemStack.getName(event.getKillingEvent().getItem().getType()) + C.white + ".");
			}

			UtilPlayer.addHealth(playerKiller, 6);

			addCurrency(playerKiller, CurrencyType.IRON, 5, C.gray + "killing " + C.gold + event.getKilled().getName());
			addCurrency(playerKiller, CurrencyType.GOLD, 1, C.gray + "killing " + C.gold + event.getKilled().getName());

			getManager().getScoreManager().getPlayer(playerKiller).addScore(getType(), "kills", 1);
		}
	}

	@Override
	public void onIngameCount()
	{
		super.onIngameCount();

		if (getStateCount() < END_TIME)
			return;

		setState(GameState.POSTGAME);
	}

	@Override
	protected void giveEndRewards()
	{
		List<ScoreWrapper> wrappers = getManager().getScoreManager().getOrderedScores(getType(), "kills", getPlayers());
		for (int i = 0; i < wrappers.size(); i++)
		{
			ScoreWrapper wrapper = wrappers.get(i);
			if (i == 0)
			{
				addCurrency(wrapper.Player, CurrencyType.IRON, 50, "1st Place");
				addCurrency(wrapper.Player, CurrencyType.GOLD, 10, "1st Place");
			}

			if (i == 1)
			{
				addCurrency(wrapper.Player, CurrencyType.IRON, 40, "2nd Place");
				addCurrency(wrapper.Player, CurrencyType.GOLD, 8, "2nd Place");
			}

			if (i == 2)
			{
				addCurrency(wrapper.Player, CurrencyType.IRON, 30, "3rd Place");
				addCurrency(wrapper.Player, CurrencyType.GOLD, 6, "3rd Place");
			}

			addCurrency(wrapper.Player, CurrencyType.IRON, 20, "Participating");
		}
	}

	@Override
	protected String[] getEndGameText()
	{
		List<String> lines = new ArrayList<String>();
		List<ScoreWrapper> wrappers = getManager().getScoreManager().getOrderedScores(getType(), "kills", getPlayers());
		for (int i = 0; i < wrappers.size(); i++)
		{
			ScoreWrapper wrapper = wrappers.get(i);
			if (i == 0)
				lines.add(C.gray + "1st: " + formatName(wrapper.Player) + C.gray + " - " + C.yellow
						+ ((int) wrapper.Score.value()));

			if (i == 1)
				lines.add(C.gray + "2nd: " + formatName(wrapper.Player) + C.gray + " - " + C.yellow
						+ ((int) wrapper.Score.value()));

			if (i == 2)
				lines.add(C.gray + "3rd: " + formatName(wrapper.Player) + C.gray + " - " + C.yellow
						+ ((int) wrapper.Score.value()));
		}

		return lines.toArray(new String[0]);
	}
}
