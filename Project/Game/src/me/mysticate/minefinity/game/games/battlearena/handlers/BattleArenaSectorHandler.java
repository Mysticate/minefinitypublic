package me.mysticate.minefinity.game.games.battlearena.handlers;

import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameSectorHandler;
import me.mysticate.minefinity.game.GameType;

public class BattleArenaSectorHandler extends GameSectorHandler
{
	public BattleArenaSectorHandler(GameManager manager)
	{
		super(manager, GameType.BATTLE_ARENA);
	}
}
