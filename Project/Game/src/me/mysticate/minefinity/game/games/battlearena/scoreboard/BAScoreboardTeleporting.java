package me.mysticate.minefinity.game.games.battlearena.scoreboard;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.game.IScoreboardHandler;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;

public class BAScoreboardTeleporting extends IScoreboardHandler<BattleArena>
{
	private HashMap<Player, Integer> _counts = new HashMap<Player, Integer>();

	public BAScoreboardTeleporting(BattleArena game)
	{
		super(game);
	}

	@Override
	public void update(TimeEvent event, Player player)
	{
		if (!_counts.containsKey(player))
			_counts.put(player, 0);

		HudManager.get(player).scoreboardAddBlank();
		HudManager.get(player)
				.scoreboardAddText(C.greenBold + "Teleporting" + StringUtils.repeat('.', _counts.get(player)));

		HudManager.get(player).scoreboardBuild();

		if (event.getType() == UpdateType.SEC_01)
			_counts.put(player, _counts.get(player) == 3 ? 0 : _counts.get(player) + 1);
	}
}
