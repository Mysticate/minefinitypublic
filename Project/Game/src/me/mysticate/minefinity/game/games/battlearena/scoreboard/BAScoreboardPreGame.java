package me.mysticate.minefinity.game.games.battlearena.scoreboard;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilTime;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.game.IScoreboardHandler;
import me.mysticate.minefinity.game.data.GamePlayerDataManager.ScoreWrapper;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;

public class BAScoreboardPreGame extends IScoreboardHandler<BattleArena>
{
	public BAScoreboardPreGame(BattleArena game)
	{
		super(game);
	}

	@Override
	public void update(TimeEvent event, Player player)
	{
		HudManager.get(player).scoreboardAddBlank();
		HudManager.get(player).scoreboardAddText(C.goldBold + "Pregame");
		HudManager.get(player)
				.scoreboardAddText(C.white + UtilTime.fromSecsToString((int) (15 - getGame().getStateCount())));
		HudManager.get(player).scoreboardAddBlank();

		HudManager.get(player).scoreboardAddText(C.goldBold + "Players");

		for (ScoreWrapper score : getGame().getManager().getScoreManager().getOrderedScores(getGame().getType(),
				"kills", getGame().getPlayers()))
			HudManager.get(player).scoreboardAddText(getGame().formatName(score.Player));

		HudManager.get(player).scoreboardBuild();
	}
}
