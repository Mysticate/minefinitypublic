package me.mysticate.minefinity.game.games.battlearena.kits.pig;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftEntity;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.combat.event.CustomDamageEvent;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilEntity;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.common.util.UtilMath;
import me.mysticate.minefinity.core.common.util.UtilPlayer;
import me.mysticate.minefinity.core.cooldown.CooldownEvent;
import me.mysticate.minefinity.core.cooldown.CooldownPool;
import me.mysticate.minefinity.core.item.InteractAction;
import me.mysticate.minefinity.core.item.ItemCallback;
import me.mysticate.minefinity.core.item.UseableItem;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.BattleArenaKit;
import me.mysticate.minefinity.game.games.battlearena.kits.pig.energy.AbilityDisplay;
import me.mysticate.minefinity.game.games.battlearena.kits.pig.energy.SpecialDisplay;
import me.mysticate.minefinity.game.games.battlearena.kits.pig.entity.PigEntity;
import me.mysticate.minefinity.game.games.battlearena.kits.pig.levels.Pig1;
import me.mysticate.minefinity.game.games.battlearena.kits.pig.levels.Pig2;
import me.mysticate.minefinity.game.games.battlearena.kits.pig.levels.Pig3;
import me.mysticate.minefinity.game.games.battlearena.kits.pig.levels.Pig4;
import me.mysticate.minefinity.game.kit.kit.ISkillCallback;
import me.mysticate.minefinity.game.kit.kit.Kit;
import net.minecraft.server.v1_9_R1.PacketPlayOutEntityVelocity;

public class KitPig extends Kit<BattleArena> implements ISkillCallback
{
	private Set<Pig> _pigs = new HashSet<Pig>();

	private final long _active = 2000;
	private final long _recharge = 15000;
	private final long _throw = 2000;

	private String[] _names = new String[] { "Dan", "Cornwell", "Mary", "Jeffington", "Rachel", "Gunsington", "Sharpay",
			"Tommy", "Scott", "Hunter", "Bacon" };

	public KitPig(GameManager manager, BattleArena game, Player player)
	{
		super(manager, game, player, BattleArenaKit.PIG);

		setLoadout(1, new Pig1(getGame(), this));
		setLoadout(2, new Pig2(getGame(), this));
		setLoadout(3, new Pig3(getGame(), this));
		setLoadout(4, new Pig4(getGame(), this));
	}

	@Override
	public void onEnable()
	{

	}

	@Override
	public void onDisable(KitDisableReason reason)
	{
		for (Pig pig : _pigs)
		{
			pig.remove();
		}

		if (getPlayer().getVehicle() instanceof Pig)
			getPlayer().getVehicle().remove();
	}

	@Override
	protected void giveItemsCustom()
	{
		IM.cooldown().reset(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Recharge"));
		IM.cooldown().reset(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Active"));
		IM.cooldown().reset(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Throw"));

		ItemStack wand = UtilItemStack.create(Material.BLAZE_ROD, (byte) 0, 1,
				C.green + "Conjuring Pigwand" + C.gray + C.italic + " Right Click",
				new String[] { " ",
						C.yellow + "Right Click" + C.white + " to " + C.green + "Summon Pig" + C.white + ".", " ",
						C.gray + "Summon a missile of " + C.white + "pure,", C.white + "firm, irreconcilable bacon",
						C.gray + "to rip through the bodies of", C.gray + "your enemies." });

		wand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, getLevel());

		UseableItem wandItem = new UseableItem(wand, new ItemCallback()
		{
			@Override
			public void onClick(Player player, Action type)
			{
				throwPig();
			}
		});

		wandItem.setType(InteractAction.RIGHT);
		getManager().getItemManager().setItem(getKeyIdentifier(), getPlayer(), 0, wandItem);
		getManager().getEnergyManager().getPlayer(getPlayer()).add(wandItem,
				new AbilityDisplay(getPlayer(), getGame()));

		if (getMagic() > 0)
		{
			ItemStack saddle = UtilItemStack.create(Material.SADDLE, (byte) 0, 1, C.green + "Pigus Escapus",
					new String[] { " ", C.yellow + "Click" + C.white + " to " + C.green + "Escape" + C.white + ".", " ",
							C.gray + "A fiery " + C.white + "pig steed is summoned",
							C.gray + "beneath your feet. A steed that", C.gray + "shall carry you away from all",
							C.gray + "struggles and strife! Ooooink!" });

			UseableItem saddleItem = new UseableItem(saddle, new ItemCallback()
			{
				@Override
				public void onClick(Player player, Action type)
				{
					activateAbility();
				}
			});

			getManager().getItemManager().setItem(getKeyIdentifier(), getPlayer(), 1, saddleItem);
			getManager().getEnergyManager().getPlayer(getPlayer()).add(saddleItem,
					new SpecialDisplay(getPlayer(), getGame()));
		}
	}

	@Override
	public void activateAbility()
	{
		if (!getGame().inGame())
			return;

		if (getMagic() < 1)
			return;

		if (IM.cooldown().getPlayer(getPlayer()).hasCooldown(CooldownPool.BATTLE_ARENA, recharge("Active")))
			return;

		if (IM.cooldown().getPlayer(getPlayer()).hasCooldown(CooldownPool.BATTLE_ARENA, recharge("Recharge")))
			return;

		getGame().getWorldHandler().EntitySpawnOverride = true;

		PigEntity pig = new PigEntity(getPlayer().getWorld(), getMagic());
		pig.spawn(getPlayer().getLocation());

		getGame().getWorldHandler().EntitySpawnOverride = false;

		pig.getBukkitEntity().setPassenger(getPlayer());

		IM.cooldown().use(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Active"), _active * getMagic());
		IM.hud().getPlayer(getPlayer()).sendActionBar(" ");
	}

	public void throwPig()
	{
		if (!getGame().inGame())
			return;

		if (!IM.cooldown().use(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Throw"), _throw))
			return;

		getGame().getWorldHandler().EntitySpawnOverride = true;
		Pig pig = getPlayer().getWorld().spawn(getPlayer().getLocation().clone().add(0, 1, 0), Pig.class);
		getGame().getWorldHandler().EntitySpawnOverride = false;

		UtilEntity.clearGoals(pig);

		pig.setBaby();
		pig.setAgeLock(true);

		pig.setCustomName(C.yellow + UtilMath.getRandom(_names));
		pig.setCustomNameVisible(true);

		Vector vec = getPlayer().getLocation().getDirection();
		vec.add(new Vector(0, .2, 0));
		vec.multiply(1.2);

		pig.setVelocity(vec);

		for (Player player : getGame().getPlayers())
		{
			UtilPlayer.sendPacket(player, new PacketPlayOutEntityVelocity(((CraftEntity) pig).getHandle()));
		}

		_pigs.add(pig);
	}

	@EventHandler
	public void onTickRidingPigs(CooldownEvent event)
	{
		if (!getGame().inGame())
			return;

		if (event.getPlayer() != getPlayer())
			return;

		if (!event.getName().equals(recharge("Active")))
			return;

		if (!(getPlayer().getVehicle() instanceof Pig))
			return;

		IM.cooldown().use(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Recharge"), _recharge);

		event.getPlayer().getVehicle().remove();
	}

	@EventHandler
	public void onVehicleLeave(VehicleExitEvent event)
	{
		if (!getGame().inGame())
			return;

		if (getPlayer() == event.getExited() && event.getVehicle() instanceof Pig)
		{
			event.getVehicle().remove();

			IM.cooldown().getPlayer(getPlayer()).removeCooldown(CooldownPool.BATTLE_ARENA, recharge("Active"));
			IM.cooldown().use(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Recharge"), _recharge);
		}
	}

	@EventHandler
	public void onTickPigs(TimeEvent event)
	{
		if (event.getType() != UpdateType.TICK)
			return;

		if (!getGame().inGame())
			return;

		for (Pig pig : new HashSet<Pig>(_pigs))
		{
			if (!pig.isValid())
			{
				_pigs.remove(pig);
				explode(pig);
				continue;
			}

			boolean exploded = false;
			for (Player player : getGame().getPlayers())
			{
				if (player == getPlayer())
					continue;

				Location center = player.getLocation().clone().add(0, 1, 0);

				if (pig.getLocation().distance(center) > 1.3)
					continue;

				exploded = true;
				break;
			}

			if (exploded || (pig.getTicksLived() > 5 && pig.isOnGround()))
			{
				_pigs.remove(pig);
				explode(pig);
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void explode(Pig pig)
	{
		pig.remove();

		pig.getWorld().spigot().playEffect(pig.getLocation(), Effect.EXPLOSION_LARGE, 0, 0, 0F, 0F, 0F, 0F, 3, 30);
		pig.getWorld().spigot().playEffect(pig.getLocation(), Effect.FLAME, 0, 0, .5F, .5F, .5F, 0F, 10, 30);

		Map<Player, Double> near = UtilPlayer.getNearby(pig, 5);
		for (Player player : near.keySet())
		{
			if (player == getPlayer())
				continue;

			EntityDamageByEntityEvent event = new EntityDamageByEntityEvent(getPlayer(), player,
					DamageCause.ENTITY_ATTACK, 20 - (4 * near.get(player)));
			Bukkit.getPluginManager().callEvent(event);
		}
	}

	@EventHandler
	public void onDamage(CustomDamageEvent event)
	{
		if (_pigs.contains(event.getDamagee()))
			event.setCancelled(true);
	}

	@EventHandler
	public void onIgnite(EntityCombustEvent event)
	{
		if (_pigs.contains(event.getEntity()))
			event.setCancelled(true);
	}

	@EventHandler
	public void onDeath(EntityDeathEvent event)
	{
		if (event.getEntity() instanceof Pig)
		{
			if (event.getEntity() == getPlayer().getVehicle())
			{
				event.getEntity().remove();

				IM.cooldown().getPlayer(getPlayer()).removeCooldown(CooldownPool.BATTLE_ARENA, recharge("Active"));
				IM.cooldown().use(CooldownPool.BATTLE_ARENA, getPlayer(), recharge("Recharge"), _recharge);
			}
		}
	}
}
