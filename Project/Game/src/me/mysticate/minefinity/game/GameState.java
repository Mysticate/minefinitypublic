package me.mysticate.minefinity.game;

public enum GameState
{
	LOBBY(10), COUNTDOWN(10), TELEPORTING(1), PREGAME(1), INGAME(1), POSTGAME(1), CLEANING(4);

	private int _data;

	private GameState(int data)
	{
		_data = data;
	}

	public byte getFlagData()
	{
		return (byte) _data;
	}

	@Override
	public String toString()
	{
		return name().subSequence(0, 1) + name().substring(1).toLowerCase();
	}
}