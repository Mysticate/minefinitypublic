package me.mysticate.minefinity.game.commands;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameState;

public class CommandStart extends CommandBase<GameManager>
{
	public CommandStart(CommandManager commandManager, GameManager manager)
	{
		super(commandManager, manager, new String[] { "start" }, Rank.ADMIN, null);

		setInvisible(true);
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		Game game = getManager().getGame(player);
		if (game == null)
			return false;

		if (game.getState() == GameState.LOBBY)
		{
			game.sendMessage(
					F.manager(game.getName(), C.yellow + player.getName() + C.gray + " has started the game!"));
			game.forceStart();
		} else if (game.getState() == GameState.COUNTDOWN)
		{
			game.sendMessage(
					F.manager(game.getName(), "Wait time shortened to " + C.yellow + "30 Seconds" + C.gray + "!"));
			game.forceStart();
		} else
		{
			sendMessage(player, "You cannot start the game currently.");
		}
		return true;
	}
}
