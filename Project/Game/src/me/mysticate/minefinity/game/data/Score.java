package me.mysticate.minefinity.game.data;

public class Score implements Comparable<Score>
{
	private double _score = 0;

	public Score()
	{
		this(0);
	}

	public Score(double score)
	{
		_score = score;
	}

	public double value()
	{
		return _score;
	}

	public void set(double score)
	{
		_score = score;
	}

	public void add(double score)
	{
		_score += score;
	}

	public void subtract(double score)
	{
		_score -= score;
	}

	public void multiply(double mult)
	{
		_score *= mult;
	}

	public void divide(double mult)
	{
		_score /= mult;
	}

	@Override
	public int compareTo(Score o)
	{
		if (o.value() == value())
			return 0;

		return o.value() > value() ? 1 : -1;
	}
}
