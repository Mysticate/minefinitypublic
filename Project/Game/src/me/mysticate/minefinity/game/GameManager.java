package me.mysticate.minefinity.game;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.backpack.BackpackManager;
import me.mysticate.minefinity.core.chat.ChatManager;
import me.mysticate.minefinity.core.combat.PlayerCombatManager;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.currency.CurrencyManager;
import me.mysticate.minefinity.core.energy.EnergyManager;
import me.mysticate.minefinity.core.game.RequestPlayerOpenGameJoinerEvent;
import me.mysticate.minefinity.core.game.RequestPlayerOpenGameOptionsEvent;
import me.mysticate.minefinity.core.item.ItemManager;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.core.sector.Sector;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.core.settings.SettingsManager;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.core.world.WorldManager;
import me.mysticate.minefinity.game.commands.CommandEnd;
import me.mysticate.minefinity.game.commands.CommandHub;
import me.mysticate.minefinity.game.commands.CommandStart;
import me.mysticate.minefinity.game.data.GamePlayerDataManager;
import me.mysticate.minefinity.game.gui.gameoptions.battlearena.GuiOptionsBA;
import me.mysticate.minefinity.game.gui.gameselect.GuiGameSelect;
import me.mysticate.minefinity.game.kit.PlayerActiveKitManager;
import me.mysticate.minefinity.game.map.Map;
import me.mysticate.minefinity.game.map.MapManager;

public class GameManager extends Manager
{
	private SectorManager _sectorManager;
	private RankManager _rankManager;
	private CurrencyManager _currencyManager;
	private PerkManager _perkManager;
	private WorldManager _worldManager;
	private ItemManager _itemManager;
	private BackpackManager _backpackManager;
	private ChatManager _chatManager;
	private SettingsManager _settingsManager;
	private EnergyManager _energyManager;
	private PlayerCombatManager _combatManager;

	private MapManager _mapManager;
	private PlayerActiveKitManager _kitManager;
	private GamePlayerDataManager _scoreManager;

	private HashMap<GameType, List<Game>> _activeGames = new HashMap<GameType, List<Game>>();
	private final Object _activeLock = new Object();

	public GameManager(JavaPlugin plugin, SectorManager sectorManager, RankManager rankManager,
			CurrencyManager currencyManager, PerkManager perkManager, WorldManager worldManager,
			ItemManager itemManager, BackpackManager backpackManager, ChatManager chatManager,
			SettingsManager settingsManager, EnergyManager energyManager, PlayerCombatManager combatManager)
	{
		super(plugin, "Game Manager");

		_sectorManager = sectorManager;
		_rankManager = rankManager;
		_currencyManager = currencyManager;
		_perkManager = perkManager;
		_worldManager = worldManager;
		_itemManager = itemManager;
		_backpackManager = backpackManager;
		_chatManager = chatManager;
		_settingsManager = settingsManager;
		_energyManager = energyManager;
		_combatManager = combatManager;

		_mapManager = new MapManager(plugin, _sectorManager);
		_kitManager = new PlayerActiveKitManager(plugin);
		_scoreManager = new GamePlayerDataManager(plugin);

		registerSectorHandlers();
		onGameCheck(new TimeEvent(UpdateType.MIN_01));
	}

	@Override
	public void registerCommands(CommandManager commandManager)
	{
		commandManager.registerCommand(new CommandHub(commandManager, this));
		commandManager.registerCommand(new CommandStart(commandManager, this));
		commandManager.registerCommand(new CommandEnd(commandManager, this));
	}

	private void registerSectorHandlers()
	{
		for (GameType game : GameType.values())
		{
			GameSectorHandler manager = game.newSectorHandler(this);
			if (manager != null)
				getSectorManager().registerSectorHandler(manager);
		}
	}

	public SectorManager getSectorManager()
	{
		return _sectorManager;
	}

	public RankManager getRankManager()
	{
		return _rankManager;
	}

	public CurrencyManager getCurrencyManager()
	{
		return _currencyManager;
	}

	public PerkManager getPerkManager()
	{
		return _perkManager;
	}

	public WorldManager getWorldManager()
	{
		return _worldManager;
	}

	public ItemManager getItemManager()
	{
		return _itemManager;
	}

	public BackpackManager getBackpackManager()
	{
		return _backpackManager;
	}

	public ChatManager getChatManager()
	{
		return _chatManager;
	}

	public MapManager getMapManager()
	{
		return _mapManager;
	}

	public PlayerActiveKitManager getKitManager()
	{
		return _kitManager;
	}

	public GamePlayerDataManager getScoreManager()
	{
		return _scoreManager;
	}

	public SettingsManager getSettingsManager()
	{
		return _settingsManager;
	}

	public EnergyManager getEnergyManager()
	{
		return _energyManager;
	}

	public PlayerCombatManager getCombatManager()
	{
		return _combatManager;
	}

	public Game getGame(Player player)
	{
		synchronized (_activeLock)
		{
			for (List<Game> games : _activeGames.values())
			{
				for (Game game : games)
				{
					if (game.hasPlayer(player))
						return game;
				}
			}
			return null;
		}
	}

	public Game getGame(Player player, GameType game)
	{
		synchronized (_activeLock)
		{
			if (!_activeGames.containsKey(game))
				_activeGames.put(game, new ArrayList<Game>());

			for (Game active : _activeGames.get(game))
			{
				if (active.hasPlayer(player))
					return active;
			}

			return null;
		}
	}

	public List<Game> getGames(GameType game)
	{
		synchronized (_activeLock)
		{
			if (!_activeGames.containsKey(game))
				_activeGames.put(game, new ArrayList<Game>());

			return new ArrayList<Game>(_activeGames.get(game));
		}
	}

	@EventHandler
	public void onGameCheck(TimeEvent event)
	{
		if (event.getType() != UpdateType.MIN_01)
			return;

		synchronized (_activeLock)
		{
			for (GameType type : GameType.values())
			{
				if (!_activeGames.containsKey(type))
					_activeGames.put(type, new ArrayList<Game>());

				int current = _activeGames.get(type).size();

				if (current >= getRequiredGames(type))
					continue;

				createGame(type);
			}
		}
	}

	private int getRequiredGames(GameType game)
	{
		try
		{
			File dir = new File("config");
			dir.mkdirs();
			File file = new File(dir.getAbsoluteFile() + File.separator + "games");
			if (!file.exists())
				file.createNewFile();

			if (!file.exists())
				return 0;

			YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

			if (!config.contains(game.getAbv()))
			{
				config.set(game.getAbv(), 0);
				config.save(file);
				return 0;
			}

			return config.getInt(game.getAbv());
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return 0;
		}
	}

	public void createGame(GameType type)
	{
		int id = getSectorManager().getNextId(type.getSector());
		if (id == -1)
			return;

		Game game = type.newInstance(this, id);
		Map map = getMapManager().getNextMap(type);
		if (map == null)
		{
			killGame(game);
			return;
		}

		game.setMap(map);

		synchronized (_activeLock)
		{
			if (!_activeGames.containsKey(type))
				_activeGames.put(type, new ArrayList<Game>());

			_activeGames.get(type).add(game);
		}
	}

	public void endGame(Game game, boolean restart)
	{
		if (game.getState() != GameState.CLEANING)
			return;

		synchronized (_activeLock)
		{
			if (!_activeGames.get(game.getType()).remove(game))
				return;
		}

		for (Player player : new HashSet<Player>(game.getPlayers()))
		{
			game.removePlayer(player);

			if (getSectorManager().sendToSector(player, Sector.HUB))
			{
				game.periodMessage(player, C.whiteBold + "Returned to Hub", C.red + "The game has ended.");
			} else
			{
				player.kickPlayer(F.kick(C.red + "An error occured while connecting you to a Hub."));
			}
		}

		killGame(game);

		if (restart)
			createGame(game.getType());
	}

	private void killGame(Game game)
	{
		if (game.getMap() != null)
			getMapManager().killWorld(game.getMap());

		game.setEnabled(false);
	}

	@EventHandler
	public void onGameSelect(RequestPlayerOpenGameJoinerEvent event)
	{
		GameType game = GameType.getByAbv(event.getGame());

		if (game != null)
			new GuiGameSelect(getPlugin(), event.getPlayer(), this, game).openInventory();
	}

	@EventHandler
	public void onGameOptions(RequestPlayerOpenGameOptionsEvent event)
	{
		GameType game = GameType.getByAbv(event.getGame());

		if (game == GameType.BATTLE_ARENA) // TODO Look for any better way of
											// doing this ._.
		{
			new GuiOptionsBA(getPlugin(), event.getPlayer(), this).openInventory();
		}
	}
}
