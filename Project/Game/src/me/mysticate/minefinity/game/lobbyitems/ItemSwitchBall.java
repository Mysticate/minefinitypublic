package me.mysticate.minefinity.game.lobbyitems;

import org.bukkit.Bukkit;
import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;

import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.item.ItemCallback;
import me.mysticate.minefinity.core.item.UseableItem;
import me.mysticate.minefinity.core.projectile.IProjectileCallback;
import me.mysticate.minefinity.core.projectile.ProjectileSignature;

public class ItemSwitchBall extends UseableItem implements Listener
{
	public ItemSwitchBall()
	{
		super(Material.SNOW_BALL, new ItemCallback()
		{
			@Override
			public void onClick(Player player, Action type)
			{
				Snowball ball = player.launchProjectile(Snowball.class);
				ProjectileSignature.instance().sign(ball, new IProjectileCallback()
				{
					@Override
					public void onLand(Projectile proj, Entity ent)
					{
						if (ent == null)
							return;

						if (!(ent instanceof Player))
							return;

						Location a = player.getLocation();
						Location b = ent.getLocation();

						player.teleport(b);
						ent.teleport(a);

						ent.playEffect(EntityEffect.HURT);
					}
				});
			}
		});

		UtilItemStack.name(this, C.green + "Switch Ball");
		UtilItemStack.setLore(this, new String[] { " ", C.gray + "Mess around before the game",
				C.gray + "by trading places with that", C.gray + "other guy over there. Ew." });

		Bukkit.getPluginManager().registerEvents(this, MinefinityPlugin.getPlugin());
	}
}
