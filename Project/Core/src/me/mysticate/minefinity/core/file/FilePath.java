package me.mysticate.minefinity.core.file;

import java.io.File;
import java.io.IOException;

public class FilePath
{
	private String _folder;
	private String _name;

	public FilePath(String folder, String name)
	{
		_folder = folder;
		_name = name;
	}

	public File makeFolder()
	{
		return new File(_folder);
	}

	public File makeFile()
	{
		return new File(_folder, _name);
	}

	public File attemptCreate()
	{
		File file = makeFile();

		if (!file.exists())
		{
			try
			{
				File folder = makeFolder();
				folder.mkdirs();

				file.createNewFile();
			} catch (IOException ex)
			{
				ex.printStackTrace();
			}
		}

		return file;
	}
}
