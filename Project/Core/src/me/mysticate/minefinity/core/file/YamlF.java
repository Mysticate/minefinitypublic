package me.mysticate.minefinity.core.file;

public class YamlF
{
	public static String path(Object... keys)
	{
		String key = "";
		for (int i = 0; i < keys.length; i++)
			key += keys[i] + (i == keys.length - 1 ? "" : ".");

		return key;
	}
}
