package me.mysticate.minefinity.core.file;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public abstract class ConfigData<Id>
{
	private Id _id;

	private FilePath _path;

	private File _file;
	private FileConfiguration _config;

	private boolean _loaded = false;

	public ConfigData(Id id)
	{
		this(id, null);
	}

	public ConfigData(Id id, FilePath path)
	{
		_id = id;

		_path = path;

		attemptCreate();
	}

	public Id getIdentifier()
	{
		return _id;
	}

	protected File getFile()
	{
		return _file;
	}

	protected FileConfiguration getConfig()
	{
		return _config;
	}

	public abstract void load();

	public abstract void save();

	public void setLoaded()
	{
		_loaded = true;
	}

	public boolean isLoaded()
	{
		return _loaded;
	}

	public String read(String path, String fallback)
	{
		try
		{
			if (!_config.contains(path))
				return fallback;

			String read = _config.getString(path);

			if (read == null)
				return fallback;

			return read;
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return fallback;
		}
	}

	public Set<String> readLines(String path)
	{
		try
		{
			if (!_config.contains(path))
				return new HashSet<String>();

			return _config.getConfigurationSection(path).getKeys(false);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return new HashSet<String>();
		}
	}

	public void write(String value, String path)
	{
		try
		{
			_config.set(path, value + "");
			_config.save(_file);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void attemptCreate()
	{
		if (_path == null)
			return;

		File file = _path.attemptCreate();

		YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

		_file = file;
		_config = config;
	}
}
