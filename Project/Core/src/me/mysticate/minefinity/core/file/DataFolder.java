package me.mysticate.minefinity.core.file;

import java.io.File;
import java.util.UUID;

public final class DataFolder
{
	private static final String _ranks = "ranks";
	private static final String _currency = "currency";
	private static final String _perks = "perks";
	private static final String _punishments = "punishments";
	private static final String _backpack = "backpack";
	private static final String _loadout = "loadout";

	private static final String _existingItems = "items";
	private static final String _existingGames = "games";
	private static final String _existingWorlds = "keepLoaded";

	public static FilePath getRank(UUID uuid)
	{
		return new FilePath(home(uuid), _ranks);
	}

	public static FilePath getCurrency(UUID uuid)
	{
		return new FilePath(home(uuid), _currency);
	}

	public static FilePath getPerks(UUID uuid)
	{
		return new FilePath(home(uuid), _perks);
	}

	public static FilePath getPunishments(UUID uuid)
	{
		return new FilePath(home(uuid), _punishments);
	}

	public static FilePath getBackpack(UUID uuid)
	{
		return new FilePath(home(uuid), _backpack);
	}

	public static FilePath getLoadout(UUID uuid)
	{
		return new FilePath(home(uuid), _loadout);
	}

	public static FilePath getExistingItems()
	{
		return new FilePath(config(), _existingItems);
	}

	public static FilePath getExistingGames()
	{
		return new FilePath(config(), _existingGames);
	}

	public static FilePath getExistingWorlds()
	{
		return new FilePath(config(), _existingWorlds);
	}

	public static String home(UUID uuid)
	{
		return "players" + File.separator + uuid.toString();
	}

	public static String config()
	{
		return "config";
	}
}
