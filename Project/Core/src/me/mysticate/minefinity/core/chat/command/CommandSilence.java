package me.mysticate.minefinity.core.chat.command;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.chat.ChatManager;
import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandSilence extends CommandBase<ChatManager>
{
	public CommandSilence(CommandManager commandManager, ChatManager manager)
	{
		super(commandManager, manager, new String[] { "silence", "shh" }, Rank.MOD, null);
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		getManager().setSilenced(player.getWorld(), !getManager().isSilenced(player.getWorld()));
		return true;
	}
}
