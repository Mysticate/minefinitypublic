package me.mysticate.minefinity.core.chat;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.PlayerManager;
import me.mysticate.minefinity.core.chat.ChatEvent.MessageTarget;
import me.mysticate.minefinity.core.chat.command.CommandBroadcast;
import me.mysticate.minefinity.core.chat.command.CommandMessage;
import me.mysticate.minefinity.core.chat.command.CommandReply;
import me.mysticate.minefinity.core.chat.command.CommandSilence;
import me.mysticate.minefinity.core.chat.reader.ChatReader;
import me.mysticate.minefinity.core.chat.reader.ChatReaderResponse;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilWorld;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.core.rank.RankPlayer;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;

public class ChatManager extends PlayerManager<ChatPlayer>
{
	private HashSet<World> _silenced = new HashSet<World>();

	private RankManager _rankManager;

	private HashSet<ChatReader> _chatReaders = new HashSet<ChatReader>();

	public ChatManager(JavaPlugin plugin, RankManager rankManager)
	{
		super(plugin, "Chat");

		_rankManager = rankManager;
	}

	@Override
	public void registerCommands(CommandManager commandManager)
	{
		commandManager.registerCommand(new CommandSilence(commandManager, this));
		commandManager.registerCommand(new CommandBroadcast(commandManager, this));

		commandManager.registerCommand(new CommandMessage(commandManager, this));
		commandManager.registerCommand(new CommandReply(commandManager, this));
	}

	@Override
	public void onEnable()
	{

	}

	@Override
	public void onDisable()
	{

	}

	public RankManager getRankManager()
	{
		return _rankManager;
	}

	public boolean addChatReader(ChatReader reader, String reason)
	{
		for (ChatReader other : _chatReaders)
		{
			if (other.getPlayer() == reader.getPlayer())
				return false;
		}

		reader.getPlayer().sendMessage(C.dGray + C.strike + "--------------------------------------");
		reader.getPlayer().sendMessage(C.redBold + "CHAT READER");
		reader.getPlayer().sendMessage(" ");
		reader.getPlayer().sendMessage(C.white + "Enter your response to the question");
		reader.getPlayer().sendMessage(C.yellow + reason);
		reader.getPlayer().sendMessage(" ");
		reader.getPlayer().sendMessage(C.gray + "Type " + C.white + "\"cancel\"" + C.gray + " to abort.");
		reader.getPlayer().sendMessage(C.dGray + C.strike + "--------------------------------------");

		_chatReaders.add(reader);
		return true;
	}

	public boolean removeChatReader(ChatReader reader)
	{
		return _chatReaders.remove(reader);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onChat(AsyncPlayerChatEvent event)
	{
		event.setCancelled(true);

		for (ChatReader reader : new HashSet<ChatReader>(_chatReaders))
		{
			ChatReaderResponse response = reader.onChat(event.getPlayer(), event.getMessage());
			if (response != null)
			{
				removeChatReader(reader);

				reader.getPlayer().sendMessage(C.dGray + C.strike + "--------------------------------------");
				reader.getPlayer().sendMessage(C.redBold + "CHAT READER");
				reader.getPlayer().sendMessage(" ");

				if (response == ChatReaderResponse.SUCCES)
				{
					reader.getPlayer().sendMessage(C.white + "Registered response");
					reader.getPlayer().sendMessage(C.yellow + event.getMessage());
				}

				if (response == ChatReaderResponse.FAILED_CANCEL)
					reader.getPlayer().sendMessage(C.aqua + "You cancelled your chat reader");

				if (response == ChatReaderResponse.FAILED_TIMEOUT)
					reader.getPlayer().sendMessage(C.aqua + "Your chat reader timed out");

				reader.getPlayer().sendMessage(" ");
				reader.getPlayer().sendMessage(C.dGray + C.strike + "--------------------------------------");

				reader.callback(response, event.getMessage());

				return;
			}
		}

		if (isSilenced(event.getPlayer().getWorld()))
		{
			if (!_rankManager.getPlayer(event.getPlayer()).getStaff().isOwned(Rank.MOD))
			{
				sendMessage(event.getPlayer(), "The chat is currently silenced!");
				return;
			}
		}

		ChatEvent chatEvent = new ChatEvent(event.getPlayer(), event.getMessage());
		Bukkit.getPluginManager().callEvent(chatEvent);

		if (chatEvent.isCancelled())
			return;

		RankPlayer rank = _rankManager.getPlayer(event.getPlayer());

		String prefix = chatEvent.shouldDisplayRank() && rank.getDominant() != Rank.DEFAULT
				? (C.dGray + "(" + rank.getDominant().buildDisplay(false, true) + C.dGray + ")") : "";
		String message = (chatEvent.getPrefix() + " " + prefix + " " + chatEvent.getNameColor()
				+ event.getPlayer().getName() + C.gray + ": " + chatEvent.getMessageColor() + chatEvent.getMessage())
						.trim();

		if (chatEvent.getTarget() == MessageTarget.NONE)
		{
			UtilWorld.sendMessage(event.getPlayer().getWorld(), message);
		} else if (chatEvent.getTarget() == MessageTarget.WHITELIST)
		{
			for (Player player : chatEvent.getPlayers())
			{
				if (player.getWorld() != event.getPlayer().getWorld())
					continue;

				player.sendMessage(message);
			}
		} else if (chatEvent.getTarget() == MessageTarget.BLACKLIST)
		{
			for (Player player : event.getPlayer().getWorld().getPlayers())
			{
				if (chatEvent.getPlayers().contains(player))
					continue;

				player.sendMessage(message);
			}
		}
	}

	@EventHandler
	public void tickReaders(TimeEvent event)
	{
		if (event.getType() != UpdateType.SEC_HALF)
			return;

		for (ChatReader reader : new HashSet<ChatReader>(_chatReaders))
		{
			if (reader.tick())
			{
				reader.getPlayer().sendMessage(C.dGray + C.strike + "--------------------------------------");
				reader.getPlayer().sendMessage(C.redBold + "CHAT READER");
				reader.getPlayer().sendMessage(" ");
				reader.getPlayer().sendMessage(C.white + "You never responded! Cancelling reader.");
				reader.getPlayer().sendMessage(" ");
				reader.getPlayer().sendMessage(C.dGray + C.strike + "--------------------------------------");

				removeChatReader(reader);
			}
		}
	}

	public boolean isSilenced(World world)
	{
		return _silenced.contains(world);
	}

	public void setSilenced(World world, boolean silenced)
	{
		if (_silenced.contains(world))
		{
			_silenced.remove(world);
			UtilWorld.sendMessage(world, F.manager(getName(), "Chat silenced: " + F.trueFalse(false)));
		} else
		{
			_silenced.add(world);
			UtilWorld.sendMessage(world, F.manager(getName(), "Chat silenced: " + F.trueFalse(true)));
		}
	}

	@Override
	protected ChatPlayer newData(Player player)
	{
		return new ChatPlayer(player.getUniqueId());
	}
}