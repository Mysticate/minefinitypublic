package me.mysticate.minefinity.core.chat.reader;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.common.util.UtilPlayer;

public class ChatReader
{
	private Player _player;

	private final long _timestamp = System.currentTimeMillis();
	private long _timeout;

	private IChatReaderCallback _callback;

	public ChatReader(Player player, long timeout, IChatReaderCallback callback)
	{
		_player = player;
		_timeout = timeout;

		_callback = callback;
	}

	public Player getPlayer()
	{
		return _player;
	}

	public boolean tick()
	{
		if (!UtilPlayer.validatePlayer(_player))
		{
			_callback.onRead(ChatReaderResponse.FAILED_LOGOUT, null);
			return true;
		}

		if (_timeout != -1 && System.currentTimeMillis() > _timestamp + _timeout)
		{
			_callback.onRead(ChatReaderResponse.FAILED_TIMEOUT, null);
			return true;
		}

		return false;
	}

	public ChatReaderResponse onChat(Player player, String message)
	{
		if (_player != player)
			return null;

		if (message.equalsIgnoreCase("cancel"))
			return ChatReaderResponse.FAILED_CANCEL;

		return ChatReaderResponse.SUCCES;
	}

	public void callback(ChatReaderResponse response, String message)
	{
		_callback.onRead(response, message);
	}
}
