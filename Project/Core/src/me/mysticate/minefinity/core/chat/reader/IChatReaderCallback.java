package me.mysticate.minefinity.core.chat.reader;

public interface IChatReaderCallback
{
	public void onRead(ChatReaderResponse response, String message);
}
