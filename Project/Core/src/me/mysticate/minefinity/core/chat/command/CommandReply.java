package me.mysticate.minefinity.core.chat.command;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.chat.ChatManager;
import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandReply extends CommandBase<ChatManager>
{
	public CommandReply(CommandManager commandManager, ChatManager manager)
	{
		super(commandManager, manager, new String[] { "r", "reply", "idontwantyourdrugs" }, Rank.DEFAULT, "<Message>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		UUID uuid = getManager().getPlayer(player).getRepliesTo();
		if (uuid == null)
		{
			sendMessage(player, "You have nobody to reply to!");
			return true;
		}

		Player target = Bukkit.getPlayer(uuid);
		if (target == null)
		{
			sendMessage(player, "That player is no longer online!");
			getManager().getPlayer(player).setRepliesTo(null);
			return true;
		}

		if (args.length == 0)
			return false;

		String message = "";
		for (int i = 0; i < args.length; i++)
			message += (args[i] + " ");

		message.trim();

		getManager().getPlayer(player).setRepliesTo(target.getUniqueId());
		getManager().getPlayer(target).setRepliesTo(player.getUniqueId());

		target.sendMessage(C.dAquaBold + "PM " + C.underline + "from" + C.dAquaBold + " " + player.getName() + ": "
				+ C.aquaBold + message);
		player.sendMessage(C.dAquaBold + "PM " + C.underline + "to" + C.dAquaBold + " " + target.getName() + ": "
				+ C.aquaBold + message);
		return true;
	}
}
