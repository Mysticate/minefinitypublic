package me.mysticate.minefinity.core.chat.command;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.chat.ChatManager;
import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandBroadcast extends CommandBase<ChatManager>
{
	public CommandBroadcast(CommandManager commandManager, ChatManager manager)
	{
		super(commandManager, manager, new String[] { "broadcast", "tellall", "mlg" }, Rank.ADMIN, "<Message>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length == 0)
			return false;

		String message = "";

		message += getManager().getRankManager().getPlayer(player).getDominant().buildDisplay(true, true).toUpperCase()
				+ " ";
		message += C.white + player.getName();
		message += C.yellow + " ";

		for (int i = 0; i < args.length; i++)
			message += (args[i] + (i == args.length - 1 ? "" : " "));

		Bukkit.broadcastMessage(message);
		return true;
	}
}
