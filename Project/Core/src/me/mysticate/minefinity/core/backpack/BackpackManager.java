package me.mysticate.minefinity.core.backpack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.DataPlayerManager;
import me.mysticate.minefinity.core.backpack.command.CommandAddItem;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.file.DataFolder;

public class BackpackManager extends DataPlayerManager<BackpackPlayer>
{
	private Set<String> _usedIds = new HashSet<String>();

	private File _file;
	private YamlConfiguration _config;

	public BackpackManager(JavaPlugin plugin)
	{
		super(plugin, "Backpack");
	}

	@Override
	public void registerCommands(CommandManager commandManager)
	{
		commandManager.registerCommand(new CommandAddItem(commandManager, this));
	}
	
	@Override
	public void onEnable()
	{
		startCache();
	}

	@Override
	public void onDisable()
	{
		saveCache();
	}

	private void startCache()
	{
		File file = DataFolder.getExistingItems().attemptCreate();

		_file = file;
		_config = YamlConfiguration.loadConfiguration(file);

		if (!_config.contains("uuids"))
		{
			_config.set("uuids", new ArrayList<String>());

			try
			{
				_config.save(file);
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}

		_usedIds.addAll(_config.getStringList("uuids"));
	}

	private void saveCache()
	{
		try
		{
			_config.set("uuids", new ArrayList<String>(_usedIds));
			_config.save(_file);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public UUID nextUUID()
	{
		while (true)
		{
			UUID uuid = UUID.randomUUID();
			if (hasUUID(uuid))
				continue;

			return uuid;
		}
	}

	public boolean hasUUID(UUID uuid)
	{
		return _usedIds.contains(uuid.toString());
	}

	public void addUUID(UUID uuid)
	{
		_usedIds.add(uuid.toString());
	}

	@Override
	protected BackpackPlayer newData(Player player)
	{
		return new BackpackPlayer(player.getUniqueId());
	}

	@Override
	protected BackpackPlayer newOfflineData(UUID uuid)
	{
		return new BackpackPlayer(uuid);
	}
}
