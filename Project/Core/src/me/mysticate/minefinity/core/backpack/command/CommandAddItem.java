package me.mysticate.minefinity.core.backpack.command;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.DataPlayerManager.OfflineDataCallback;
import me.mysticate.minefinity.core.backpack.BackpackManager;
import me.mysticate.minefinity.core.backpack.BackpackPlayer;
import me.mysticate.minefinity.core.backpack.ItemPool;
import me.mysticate.minefinity.core.backpack.MetaValue;
import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandAddItem extends CommandBase<BackpackManager>
{
	public CommandAddItem(CommandManager commandManager, BackpackManager manager)
	{
		super(commandManager, manager, new String[]
				{
						"additem",
						"add"
				}, Rank.ADMIN, "<Player> <Pool> <Name> <Metadata>");		
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length != 4)
			return false;

		String playerName = args[0];
		
		ItemPool pool = ItemPool.getByName(args[1]);
		if (pool == null)
		{
			sendMessage(player, C.redBold + "Invalid pool!");
			return true;
		}
		
		String name = args[2];
		
		HashMap<String, MetaValue> meta = new HashMap<String, MetaValue>();
		String unparsed = args[3];
		try
		{
			for (String pair : unparsed.split(";"))
			{
				String key = pair.split(":")[0];
				String value = pair.split(":")[1];
				
				meta.put(key, new MetaValue(value));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			sendMessage(player, C.redBold + "Invalid meta.");
			return true;
		}

		sendMessage(player, "Processing request...");

		try
		{
			getManager().loadOfflineData(playerName, new OfflineDataCallback<BackpackPlayer>()
			{
				@Override
				public void onLoad(BackpackPlayer data, boolean offline)
				{
					data.addItem(name, pool, meta);

					sendMessage(player, "You added an item to " + C.yellow + playerName);

					if (offline)
					{
						data.save();
					} else
					{
						sendMessage(Bukkit.getPlayer(playerName), "You recieved an item from " + C.yellow + player.getName() + C.gray + ".");
					}
				}
			});
		} catch (Exception ex)
		{
			sendMessage(player, C.red + "An error occured while processing your request. Is that the correct name?");
		}

		return true;
	}
}
