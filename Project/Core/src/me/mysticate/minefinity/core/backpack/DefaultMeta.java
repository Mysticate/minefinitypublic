package me.mysticate.minefinity.core.backpack;

public class DefaultMeta
{
	public static final String MATERIAL = "material";
	public static final String DATA = "variantData";
	public static final String LORE = "lore";
	public static final String AMOUNT = "amount";
}
