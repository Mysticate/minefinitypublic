package me.mysticate.minefinity.core.backpack;

import java.util.HashMap;
import java.util.UUID;

public class AquiredItem
{
	private UUID _uuid;
	private String _id;
	private ItemPool _pool;
	private HashMap<String, MetaValue> _meta = new HashMap<String, MetaValue>();

	public AquiredItem(UUID uuid, String id, ItemPool pool, HashMap<String, MetaValue> meta)
	{
		_uuid = uuid;
		_id = id;
		_pool = pool;
		_meta = meta;
	}

	public UUID getUUID()
	{
		return _uuid;
	}

	public String getId()
	{
		return _id;
	}

	public ItemPool getPool()
	{
		return _pool;
	}

	public MetaValue getValue(String meta)
	{
		return _meta.get(meta);
	}
	
	public MetaValue getValue(String meta, Object def)
	{
		if (!hasValue(meta))
			return new MetaValue(def.toString());
		
		return getValue(meta);
	}

	public boolean hasValue(String meta)
	{
		return _meta.containsKey(meta);
	}

	public void setValue(String value, MetaValue meta)
	{
		_meta.put(value, meta);
	}

	public HashMap<String, MetaValue> getMeta()
	{
		return _meta;
	}

	public boolean hasDisplayName()
	{
		return hasValue("displayName") && getValue("displayName").isString();
	}

	public String getDisplayName()
	{
		if (!hasDisplayName())
			return null;

		return getValue("displayName").asString();
	}

	public void setDisplayName(String name)
	{
		setValue("displayName", new MetaValue(name));
	}

	public boolean hasGlow()
	{
		return hasValue("glow") && getValue("glow").isInt() && getValue("glow").asInt() >= 1;
	}
}
