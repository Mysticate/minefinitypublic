package me.mysticate.minefinity.core.world;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.combat.event.CustomDamageEvent;
import me.mysticate.minefinity.core.common.util.UtilInv;
import me.mysticate.minefinity.core.common.util.UtilPlayer;
import me.mysticate.minefinity.core.common.util.UtilWorld;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;

public abstract class WorldHandler implements Listener
{
	private JavaPlugin _plugin;

	private World _world;

	public boolean BlockBreak = true;
	public Set<Material> BlockBreakOverride = new HashSet<Material>();

	public boolean BlockPlace = true;
	public Set<Material> BlockPlaceOverride = new HashSet<Material>();

	public boolean BlockBurn = true;
	public Set<Material> BlockBurnOverride = new HashSet<Material>();

	public boolean BlockFade = true;
	public Set<Material> BlockFadeOverride = new HashSet<Material>();

	public boolean BlockDamage = true;
	public Set<Material> BlockDamageOverride = new HashSet<Material>();

	public boolean LiquidFlow = true;

	public boolean BlockIgnite = true;
	public Set<Material> BlockIgniteOverride = new HashSet<Material>();

	public boolean BlockRedstone = true;
	public Set<Material> BlockRedstoneOverride = new HashSet<Material>();

	public boolean BlockChangeByEntity = true;

	public boolean EntityDamage = true;
	public boolean EntityDamageByEntity = true;

	public boolean PlayerDismount = true;
	public boolean EntityDismount = true;

	public boolean EntityIgnite = true;
	public boolean PlayerIgnite = true;

	public boolean EntityShootBow = true;
	public boolean PlayerShootBow = true;

	public boolean PaintingBreak = true;

	public boolean InventoryOpen = true;
	public Set<InventoryType> InventoryOpenOverride = new HashSet<InventoryType>();

	public boolean BucketFill = true;
	public boolean BucketEmpty = true;

	public int HungerSet = 20;
	public boolean HungerChange = true;

	public float SaturationSet = 1;
	public boolean SaturationChange = true;

	public boolean ItemDrop = true;
	public boolean ItemPickup = true;

	public boolean RemoveArrows = false;

	public boolean EntitySpawn = true;
	public boolean EntitySpawnOverride = false;

	public long TimeSet = -1;

	public WorldHandler(JavaPlugin plugin, World world)
	{
		_plugin = plugin;
		_world = world;
	}

	public void stop()
	{
		HandlerList.unregisterAll(this);
	}

	public JavaPlugin getPlugin()
	{
		return _plugin;
	}

	public World getWorld()
	{
		return _world;
	}

	public boolean inWorld(Player player)
	{
		return _world.getPlayers().contains(player);
	}

	public abstract void onJoin(Player player);

	public abstract void onQuit(Player player);

	public abstract Location getJoinLocation(Player player);

	public void sendMessage(String message)
	{
		UtilWorld.sendMessage(_world, message);
	}

	@EventHandler
	public void deregister(WorldUnloadEvent event)
	{
		if (event.getWorld() == _world)
		{
			HandlerList.unregisterAll(this);
		}
	}

	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent event)
	{
		if (event.getFrom() == _world)
		{
			onQuit(event.getPlayer());
		}

		if (event.getPlayer().getWorld() == _world)
		{
			if (getJoinLocation(event.getPlayer()) != null)
				event.getPlayer().teleport(getJoinLocation(event.getPlayer()));

			onJoin(event.getPlayer());
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBlockBreak(BlockBreakEvent event)
	{
		if (event.getBlock().getWorld() != _world)
			return;

		if (event.getPlayer().getGameMode() == GameMode.CREATIVE)
			return;

		if (BlockBreak)
			return;

		if (!BlockBreakOverride.contains(event.getBlock().getType()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBlockPlace(BlockPlaceEvent event)
	{
		if (event.getBlock().getWorld() != _world)
			return;

		if (event.getPlayer().getGameMode() == GameMode.CREATIVE)
			return;

		if (BlockPlace)
			return;

		if (!BlockPlaceOverride.contains(event.getBlock().getType()))
			event.setCancelled(true);

		UtilInv.update(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBlockBurn(BlockBurnEvent event)
	{
		if (event.getBlock().getWorld() != _world)
			return;

		if (BlockBurn)
			return;

		if (!BlockBurnOverride.contains(event.getBlock().getType()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBlockFade(BlockFadeEvent event)
	{
		if (event.getBlock().getWorld() != _world)
			return;

		if (BlockFade)
			return;

		if (!BlockFadeOverride.contains(event.getBlock().getType()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBlockDamage(BlockDamageEvent event)
	{
		if (event.getBlock().getWorld() != _world)
			return;

		if (event.getPlayer().getGameMode() == GameMode.CREATIVE)
			return;

		if (!BlockDamage)
			return;

		if (BlockDamageOverride.contains(event.getBlock().getType()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onLiquidFlow(BlockFromToEvent event)
	{
		if (event.getBlock().getWorld() != _world)
			return;

		if (LiquidFlow)
			return;

		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBlockIgnite(BlockIgniteEvent event)
	{
		if (event.getBlock().getWorld() != _world)
			return;

		if (BlockIgnite)
			return;

		if (!BlockIgniteOverride.contains(event.getBlock().getType()))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBlockRedstone(BlockRedstoneEvent event)
	{
		if (event.getBlock().getWorld() != _world)
			return;

		if (BlockRedstone)
			return;

		if (!BlockRedstoneOverride.contains(event.getBlock().getType()))
			event.setNewCurrent(0);
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.LOW)
	public void onBlockChangeByEntity(EntityChangeBlockEvent event)
	{
		if (event.getBlock().getWorld() != _world)
			return;

		if (BlockChangeByEntity)
			return;

		event.setCancelled(true);

		if (event.getEntity() instanceof FallingBlock)
			event.getBlock().getWorld().playEffect(event.getBlock().getLocation(), Effect.STEP_SOUND,
					((FallingBlock) event.getEntity()).getMaterial().getId());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onEntityDamage(CustomDamageEvent event)
	{
		if (event.getDamagee().getWorld() != _world)
			return;

		if (event.isAttack())
		{
			if (!EntityDamageByEntity)
			{
				event.setCancelled(true);
				return;
			}
		}

		if (EntityDamage)
			return;

		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onVehicleExit(VehicleExitEvent event)
	{
		if (event.getExited().getWorld() != _world)
			return;

		if (event.getExited() instanceof Player)
		{
			if (PlayerDismount)
				return;

			event.setCancelled(true);
			return;
		}

		if (EntityDismount)
			return;

		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onEntityCombust(EntityCombustEvent event)
	{
		if (event.getEntity().getWorld() != _world)
			return;

		if (event.getEntity() instanceof Player)
		{
			if (PlayerIgnite)
				return;

			event.setCancelled(true);
			return;
		}

		if (EntityIgnite)
			return;

		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBowShoot(EntityShootBowEvent event)
	{
		if (event.getEntity().getWorld() != _world)
			return;

		if (event.getEntity() instanceof Player)
		{
			if (PlayerShootBow)
				return;

			UtilInv.update(event.getEntity());
			event.setCancelled(true);
			return;
		}

		if (EntityShootBow)
			return;

		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPaintingBreak(HangingBreakEvent event)
	{
		if (event.getEntity().getWorld() != _world)
			return;

		if (PaintingBreak)
			return;

		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onInventoryOpen(InventoryOpenEvent event)
	{
		if (event.getPlayer().getWorld() != _world)
			return;

		if (InventoryOpen)
			return;

		if (InventoryOpenOverride.contains(event.getInventory().getType()))
			return;

		if (event.getInventory().getType() == InventoryType.CHEST)
		{
			if (event.getInventory().getHolder() instanceof Chest
					|| event.getInventory().getHolder() instanceof DoubleChest)
			{
				event.setCancelled(true);
			}
			return;
		}

		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBucketFill(PlayerBucketFillEvent event)
	{
		if (event.getPlayer().getWorld() != _world)
			return;

		if (event.getPlayer().getGameMode() == GameMode.CREATIVE)
			return;

		if (BucketFill)
			return;

		event.setCancelled(true);
		UtilInv.update(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBucketEmpty(PlayerBucketEmptyEvent event)
	{
		if (event.getPlayer().getWorld() != _world)
			return;

		if (event.getPlayer().getGameMode() == GameMode.CREATIVE)
			return;

		if (BucketEmpty)
			return;

		event.setCancelled(true);
		UtilInv.update(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onHungerSet(TimeEvent event)
	{
		if (event.getType() != UpdateType.SEC_01)
			return;

		if (HungerChange)
			return;

		for (Player player : Bukkit.getOnlinePlayers())
		{
			if (player.getWorld() != _world)
				return;

			UtilPlayer.setFood(player, HungerSet);
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onHungerChange(FoodLevelChangeEvent event)
	{
		if (event.getEntity().getWorld() != _world)
			return;

		if (HungerChange)
			return;

		UtilPlayer.setFood((Player) event.getEntity(), HungerSet);
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onSaturationSet(TimeEvent event)
	{
		if (event.getType() != UpdateType.SEC_01)
			return;

		if (SaturationChange)
			return;

		for (Player player : Bukkit.getOnlinePlayers())
		{
			if (player.getWorld() != _world)
				return;

			UtilPlayer.setSaturation(player, SaturationSet);
			player.setSaturation(SaturationSet);
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onItemDrop(PlayerDropItemEvent event)
	{
		if (event.getPlayer().getWorld() != _world)
			return;

		if (ItemDrop)
			return;

		event.setCancelled(true);
		UtilInv.update(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onItemPickup(PlayerPickupItemEvent event)
	{
		if (event.getPlayer().getWorld() != _world)
			return;

		if (ItemPickup)
			return;

		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onArrowHit(ProjectileHitEvent event)
	{
		if (event.getEntity() instanceof Arrow)
			event.getEntity().remove();
	}

	@EventHandler
	public void onTimeSet(TimeEvent event)
	{
		if (TimeSet == -1)
			return;

		getWorld().setTime(TimeSet);
	}

	@EventHandler
	public void onEntitySpawn(EntitySpawnEvent event)
	{
		if (getWorld() != event.getEntity().getWorld())
			return;

		if (EntitySpawn)
			return;

		if (EntitySpawnOverride)
			return;

		if (!(event.getEntity() instanceof LivingEntity) && !(event.getEntity() instanceof Creature)
				&& !(event.getEntity() instanceof Monster))
			return;

		if (event.getEntity().getType() == EntityType.ARMOR_STAND)
			return;

		event.getEntity().remove();
	}
}