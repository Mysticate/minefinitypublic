package me.mysticate.minefinity.core.world;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.Manager;

public class WorldManager extends Manager
{
	private HashMap<World, WorldHandler> _handlerMap = new HashMap<World, WorldHandler>();

	public WorldManager(JavaPlugin plugin)
	{
		super(plugin, "World");
	}

	public void setHandler(World world, WorldHandler handler)
	{
		removeHandler(world);

		Bukkit.getPluginManager().registerEvents(handler, getPlugin());
		_handlerMap.put(world, handler);
	}

	public void removeHandler(World world)
	{
		WorldHandler previous = _handlerMap.remove(world);
		if (previous != null)
			previous.stop();
	}

	public void removeHandler(WorldHandler handler)
	{
		for (World world : new HashSet<World>())
		{
			if (getHandler(world) == handler)
			{
				removeHandler(world);
				return;
			}
		}
	}

	public WorldHandler getHandler(World world)
	{
		return _handlerMap.get(world);
	}

	@EventHandler
	public void onUnload(WorldUnloadEvent event)
	{
		WorldHandler handler = _handlerMap.remove(event.getWorld());
		if (handler != null)
			handler.stop();
	}
}
