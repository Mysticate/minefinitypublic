package me.mysticate.minefinity.core.updater;

public enum UpdateType
{
	MIN_01(60000L), SEC_01(1000L), SEC_HALF(500L), SEC_QUARTER(250L), SEC_EIGHTH(125L), TICK(49L);

	;

	private long _last = System.currentTimeMillis();
	private long _delay;

	private UpdateType(long delay)
	{
		_delay = delay;
	}

	public boolean elapsed(UpdaterManager manager)
	{
		if (!manager.isDoingTiming())
			return false;

		if (System.currentTimeMillis() - _last >= _delay)
		{
			_last = System.currentTimeMillis();
			return true;
		}
		return false;
	}

	public void reset()
	{
		_last = System.currentTimeMillis();
	}
}
