package me.mysticate.minefinity.core.combat;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.UUID;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.combat.event.CustomDamageEvent;
import me.mysticate.minefinity.core.combat.logging.LoggedDamageEvent;
import me.mysticate.minefinity.core.common.util.UtilTime;
import me.mysticate.minefinity.core.player.PlayerData;

public class CombatPlayer extends PlayerData
{
	public static class LogWrapper
	{
		public LoggedDamageEvent Event;
		public LinkedList<LoggedDamageEvent> Assists;
	}

	private LinkedList<LoggedDamageEvent> _damage = new LinkedList<LoggedDamageEvent>();

	public CombatPlayer(UUID id)
	{
		super(id);
	}

	public void clearLog()
	{
		_damage.clear();
	}

	public void logEvent(CustomDamageEvent event, double damage)
	{
		_damage.addFirst(new LoggedDamageEvent(event, damage));
	}

	public void expire()
	{
		for (LoggedDamageEvent event : new ArrayList<LoggedDamageEvent>(_damage))
		{
			if (UtilTime.hasPassed(event.getTimeStamp(), 15000))
			{
				_damage.remove(event);
			}
		}
	}

	public LogWrapper killed(Player killer)
	{
		LogWrapper wrapper = new LogWrapper();

		LoggedDamageEvent bestEvent = null;
		for (LoggedDamageEvent event : _damage)
		{
			if (event.isByEntity())
			{
				if (killer == null || event.getDamager() == killer)
				{
					bestEvent = event;
					break;
				}
			}
		}

		if (bestEvent == null && !_damage.isEmpty())
			bestEvent = _damage.getFirst();

		_damage.remove(bestEvent);

		wrapper.Event = bestEvent;
		wrapper.Assists = new LinkedList<LoggedDamageEvent>(_damage);

		_damage.clear();
		return wrapper;
	}
}
