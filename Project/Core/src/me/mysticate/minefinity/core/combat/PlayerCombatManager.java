package me.mysticate.minefinity.core.combat;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.PlayerManager;
import me.mysticate.minefinity.core.combat.CombatPlayer.LogWrapper;
import me.mysticate.minefinity.core.combat.event.CustomDamageEvent;
import me.mysticate.minefinity.core.combat.event.CustomDeathEvent;
import me.mysticate.minefinity.core.common.util.UtilEntity;
import me.mysticate.minefinity.core.common.util.UtilPlayer;
import me.mysticate.minefinity.core.common.util.UtilProjectile;

public class PlayerCombatManager extends PlayerManager<CombatPlayer>
{
	public PlayerCombatManager(JavaPlugin plugin)
	{
		super(plugin, "Combat");
	}

	@EventHandler
	public void onShootBow(EntityShootBowEvent event)
	{
		UtilProjectile.setShotWithBow(event.getProjectile());
		UtilProjectile.setAttachedItem(event.getProjectile(), event.getBow());
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDamage(EntityDamageEvent event)
	{
		Entity damagee = event.getEntity();
		DamageCause cause = event.getCause();

		Entity directDamager = null;
		Entity damager = null;
		Projectile proj = null;
		if (event instanceof EntityDamageByEntityEvent)
		{
			directDamager = ((EntityDamageByEntityEvent) event).getDamager();
			damager = ((EntityDamageByEntityEvent) event).getDamager();
			if (damager instanceof Projectile)
			{
				proj = (Projectile) damager;
				damager = proj.getShooter() instanceof Entity ? (Entity) proj.getShooter() : null;
			} else
			{
				UUID tag = UtilEntity.readTag(damager);
				if (tag != null)
				{
					Player tagged = Bukkit.getPlayer(tag);
					if (UtilPlayer.validatePlayer(tagged))
					{
						damager = tagged;
					}
				}
			}
		}

		HashMap<DamageModifier, Double> modifiers = new HashMap<DamageModifier, Double>();
		for (DamageModifier modifier : DamageModifier.values())
		{
			if (event.isApplicable(modifier))
				modifiers.put(modifier, event.getDamage(modifier));
		}

		ItemStack item = null;

		if (UtilProjectile.wasShotWithBow(proj))
		{
			item = UtilProjectile.getAttachedItem(proj);
		} else if (damager instanceof HumanEntity)
		{
			item = ((HumanEntity) damager).getInventory().getItemInMainHand();
		} else if (damager instanceof LivingEntity)
		{
			item = ((LivingEntity) damager).getEquipment().getItemInMainHand();
		}

		if (item == null)
			item = new ItemStack(Material.AIR);

		CustomDamageEvent damageEvent = new CustomDamageEvent(damagee, cause, directDamager, damager, proj, item,
				modifiers);
		Bukkit.getPluginManager().callEvent(damageEvent);

		event.setCancelled(damageEvent.isCancelled());

		for (DamageModifier modifier : DamageModifier.values())
		{
			if (event.isApplicable(modifier))
				event.setDamage(modifier, damageEvent.getDamage(modifier));
		}

		if (damageEvent.isPlayer())
		{
			getPlayer((Player) damageEvent.getDamagee()).logEvent(damageEvent, event.getDamage());
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onDeath(PlayerDeathEvent event)
	{
		event.getEntity().setHealth(event.getEntity().getMaxHealth());

		event.setDeathMessage(null);

		event.setKeepInventory(true);
		event.setKeepLevel(true);

		event.getEntity().setFoodLevel(20);
		event.getEntity().setExhaustion(1F);
		event.getEntity().setSaturation(1F);

		event.getEntity().setFireTicks(0);
		event.getEntity().setFallDistance(0F);

		((CraftPlayer) event.getEntity()).getHandle().o(0);

		Bukkit.getScheduler().runTaskLater(getPlugin(), new Runnable()
		{
			@Override
			public void run()
			{
				event.getEntity().setFireTicks(0);
				event.getEntity().setFallDistance(0F);
			}
		}, 1);

		Player killed = event.getEntity();
		CombatPlayer combat = getPlayer(killed);

		LogWrapper wrapper = combat.killed(killed.getKiller());

		CustomDeathEvent deathEvent = new CustomDeathEvent(killed, wrapper.Event, wrapper.Assists, event);
		Bukkit.getPluginManager().callEvent(deathEvent);
	}

	@Override
	protected CombatPlayer newData(Player player)
	{
		return new CombatPlayer(player.getUniqueId());
	}
}
