package me.mysticate.minefinity.core.combat.logging;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.combat.event.CustomDamageEvent;

public class LoggedDamageEvent
{
	private final long _timestamp = System.currentTimeMillis();

	private Entity _damagee;
	private DamageCause _cause;

	private Entity _directDamager;
	private Entity _damager;
	private Projectile _proj;

	private ItemStack _item;

	private double _damage;

	public LoggedDamageEvent(CustomDamageEvent event, double damage)
	{
		_damagee = event.getDamagee();
		_cause = event.getCause();

		_directDamager = event.getDirectDamager();
		_damager = event.getDamager();
		_proj = event.getProjectile();

		_damage = damage;

		_item = event.getItem().clone();
	}

	public boolean isByEntity()
	{
		return _directDamager != null || _damager != null || _proj != null;
	}

	public long getTimeStamp()
	{
		return _timestamp;
	}

	public Entity getDamagee()
	{
		return _damagee;
	}

	public DamageCause getCause()
	{
		return _cause;
	}

	public Entity getDirectDamager()
	{
		return _directDamager;
	}

	public Entity getDamager()
	{
		return _damager;
	}

	public Projectile getProjectile()
	{
		return _proj;
	}

	public double getDamage()
	{
		return _damage;
	}

	public boolean isPlayer()
	{
		return getDamagee() instanceof Player;
	}

	public boolean isAttack()
	{
		return getDirectDamager() != null;
	}

	public boolean byPlayer()
	{
		return getDamager() instanceof Player;
	}

	public boolean hasProjectile()
	{
		return getProjectile() != null;
	}

	public ItemStack getItem()
	{
		return _item;
	}
}
