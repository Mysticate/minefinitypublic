package me.mysticate.minefinity.core.combat.event;

import java.util.LinkedList;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDeathEvent;

import me.mysticate.minefinity.core.combat.logging.LoggedDamageEvent;

public class CustomDeathEvent extends Event
{
	private static HandlerList _handlers = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	private Player _killed;

	private LoggedDamageEvent _loggedEvent;
	private LinkedList<LoggedDamageEvent> _assists;

	private EntityDeathEvent _event;

	public CustomDeathEvent(Player killed, LoggedDamageEvent loggedEvent, LinkedList<LoggedDamageEvent> assists,
			EntityDeathEvent event)
	{
		_killed = killed;

		_loggedEvent = loggedEvent;
		_assists = assists;

		_event = event;
	}

	public Player getKilled()
	{
		return _killed;
	}

	public LoggedDamageEvent getKillingEvent()
	{
		return _loggedEvent;
	}

	public LinkedList<LoggedDamageEvent> getAssists()
	{
		return _assists;
	}

	public EntityDeathEvent getEvent()
	{
		return _event;
	}
}
