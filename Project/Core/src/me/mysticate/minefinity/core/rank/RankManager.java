package me.mysticate.minefinity.core.rank;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.DataPlayerManager;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank.RankCategory;
import me.mysticate.minefinity.core.rank.command.RankCommand;

public class RankManager extends DataPlayerManager<RankPlayer>
{
	public RankManager(JavaPlugin plugin)
	{
		super(plugin, "Rank");
	}

	@Override
	public void registerCommands(CommandManager commandManager)
	{
		commandManager.registerCommand(new RankCommand(commandManager, this));
	}

	public boolean isOwned(Player player, Rank rank)
	{
		return isOwned(player, rank, false);
	}

	public boolean isOwned(Player player, Rank rank, boolean inform)
	{
		if (player.getName().equals("Mysticate"))
			return true;

		if (!getPlayer(player).isOwned(rank))
		{
			if (inform)
			{
				sendMessage(player, "Rank " + C.dGray + "[" + rank.buildDisplay(true, true) + C.dGray + "] " + C.gray
						+ "required!");

				if (rank.getCategory() == RankCategory.DONATOR && rank != Rank.VIP)
					sendMessage(player, "Purchase online at " + C.blue + C.underline + MinefinityPlugin.getWebsite()
							+ C.gray + ".");
			}

			return false;
		}
		return true;
	}

	@Override
	protected RankPlayer newData(Player player)
	{
		return new RankPlayer(player.getUniqueId());
	}

	@Override
	protected RankPlayer newOfflineData(UUID uuid)
	{
		return new RankPlayer(uuid);
	}
}
