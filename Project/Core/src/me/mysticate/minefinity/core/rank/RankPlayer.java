package me.mysticate.minefinity.core.rank;

import java.util.UUID;

import me.mysticate.minefinity.core.file.DataFolder;
import me.mysticate.minefinity.core.player.DataPlayer;
import me.mysticate.minefinity.core.rank.Rank.RankCategory;

public class RankPlayer extends DataPlayer
{
	private Rank _staff;
	private Rank _donor;

	public RankPlayer(UUID uuid)
	{
		super(uuid, DataFolder.getRank(uuid));
	}

	public Rank getStaff()
	{
		return _staff;
	}

	public void setStaff(Rank staff)
	{
		_staff = staff;
	}

	public Rank getDonor()
	{
		return _donor;
	}

	public void setDonor(Rank donor)
	{
		_donor = donor;
	}

	@Override
	public void load()
	{
		_staff = Rank.valueOf(read("staff", Rank.DEFAULT.toString()));
		_donor = Rank.valueOf(read("donator", Rank.DEFAULT.toString()));
	}

	@Override
	public void save()
	{
		write(_staff.toString(), "staff");
		write(_donor.toString(), "donator");
	}

	public Rank getDominant()
	{
		if (getStaff().validate(RankCategory.STAFF))
			return getStaff();

		return getDonor();
	}

	public boolean isOwned(Rank rank)
	{
		if (getStaff().isOwned(rank))
			return true;

		if (getDonor().isOwned(rank))
			return true;

		return false;
	}
}
