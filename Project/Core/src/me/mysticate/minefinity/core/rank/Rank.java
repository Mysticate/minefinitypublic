package me.mysticate.minefinity.core.rank;

import org.bukkit.ChatColor;

import me.mysticate.minefinity.core.common.text.C;

public enum Rank
{
	OWNER(RankCategory.STAFF, "Owner", ChatColor.DARK_RED), ADMIN(RankCategory.STAFF, "Admin", ChatColor.RED), MOD(
			RankCategory.STAFF, "Mod", ChatColor.LIGHT_PURPLE), HELPER(RankCategory.STAFF, "Helper", ChatColor.GREEN),

	VIP(RankCategory.DONATOR, "VIP", ChatColor.GREEN), MASTER(RankCategory.DONATOR, "Master",
			ChatColor.AQUA), CHAMPION(RankCategory.DONATOR, "Champion", ChatColor.GOLD),

	DEFAULT(RankCategory.NONE, "", "Default", ChatColor.WHITE)

	;

	private RankCategory _category;

	private String _name;
	private String _display;

	private ChatColor _color;

	private Rank(RankCategory category, String name, ChatColor color)
	{
		this(category, name, name, color);
	}

	private Rank(RankCategory category, String name, String display, ChatColor color)
	{
		_category = category;

		_name = name;
		_display = display;

		_color = color;
	}

	public RankCategory getCategory()
	{
		return _category;
	}

	public String getName()
	{
		return _name;
	}

	public String getDisplayName()
	{
		return _display;
	}

	public ChatColor getColor()
	{
		return _color;
	}

	public String buildDisplay(boolean bold, boolean color)
	{
		return (color ? getColor() : "") + (bold ? C.bold : "") + getName();
	}

	public boolean isOwned(Rank rank)
	{
		if (this == rank)
			return true;

		if (rank == Rank.DEFAULT)
			return true;

		if (getCategory() != rank.getCategory())
		{
			if (getCategory()._power >= rank.getCategory()._power)
				return true;
		}

		if (ordinal() <= rank.ordinal())
			return true;

		return false;
	}

	public boolean validate(RankCategory category)
	{
		return getCategory() == category;
	}

	public static Rank parseName(String name)
	{
		for (Rank type : Rank.values())
		{
			if (type.toString().equalsIgnoreCase(name))
			{
				return type;
			}
		}
		return null;
	}

	public static enum RankCategory
	{
		NONE(0), STAFF(2), DONATOR(1), OTHER(999);

		private int _power;

		private RankCategory(int power)
		{
			_power = power;
		}
	}

	public static Rank getByName(String name)
	{
		for (Rank type : values())
		{
			if (type.toString().equalsIgnoreCase(name))
				return type;
		}

		return null;
	}
}
