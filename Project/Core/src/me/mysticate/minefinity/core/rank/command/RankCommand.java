package me.mysticate.minefinity.core.rank.command;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.DataPlayerManager.OfflineDataCallback;
import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.core.rank.RankPlayer;
import me.mysticate.minefinity.core.rank.Rank.RankCategory;

public class RankCommand extends CommandBase<RankManager>
{
	public RankCommand(CommandManager commandManager, RankManager manager)
	{
		super(commandManager, manager, new String[] { "rank", "setrank" }, Rank.ADMIN, "<Player> <Rank Type> <Rank>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length != 3)
		{
			return false;
		}

		String playerName = args[0];

		if (!args[1].equalsIgnoreCase("staff") && !args[1].equalsIgnoreCase("donator"))
		{
			sendMessage(player, C.redBold + "Invalid rank type");
			return true;
		}

		RankCategory type = args[1].equalsIgnoreCase("staff") ? RankCategory.STAFF : RankCategory.DONATOR;

		Rank r = null;
		try
		{
			r = Rank.getByName(args[2].toUpperCase());
		} catch (Exception ex)
		{
			sendMessage(player, C.redBold + "Invalid rank");
			return true;
		}

		final Rank rank = r;
		if (rank == null)
		{
			sendMessage(player, C.redBold + "Invalid rank");
			return true;
		}

		if (rank != Rank.DEFAULT && !rank.validate(type))
		{
			sendMessage(player, C.redBold + "That rank isn't compatible with the rank type inputted!");
			return true;
		}

		sendMessage(player, "Processing request...");

		try
		{
			if (!getManager().loadOfflineData(playerName, new OfflineDataCallback<RankPlayer>()
			{
				@Override
				public void onLoad(RankPlayer data, boolean offline)
				{
					if (type == RankCategory.STAFF)
					{
						data.setStaff(rank);
					} else
					{
						data.setDonor(rank);
					}

					sendMessage(player, "You set " + C.yellow + playerName + C.gray + " to "
							+ rank.buildDisplay(false, true) + C.gray + ".");

					if (offline)
					{
						data.save();
					} else
					{
						sendMessage(Bukkit.getPlayer(playerName),
								C.yellow + player.getName() + C.gray + " set you to " + rank.buildDisplay(false, true));
					}
				}
			}))
			{
				sendMessage(player, "Unknown player " + C.yellow + playerName + C.gray + ".");
			}
		} catch (Exception ex)
		{
			sendMessage(player, C.red + "An error occured while processing your request. Is that the correct name?");
		}

		return true;
	}
}
