package me.mysticate.minefinity.core.sector.event;

import org.bukkit.World;
import org.bukkit.event.HandlerList;

import me.mysticate.minefinity.core.sector.SectorWorld;

public class WorldSectorChangeEvent extends SectorEvent
{
	private static HandlerList _handlers = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	public WorldSectorChangeEvent(World world, SectorWorld sector)
	{
		super(sector.getWorld(), sector);
	}
}
