package me.mysticate.minefinity.core.sector.command;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.core.sector.SectorWorld;

public class CommandLocate extends CommandBase<SectorManager>
{
	public CommandLocate(CommandManager commandManager, SectorManager manager)
	{
		super(commandManager, manager, new String[] { "locate", "find" }, Rank.ADMIN, "<Player>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length == 0)
			return false;

		Player target = Bukkit.getPlayer(args[0]);
		if (target == null)
		{
			sendMessage(player, "That player is " + C.red + "Offline" + C.gray + ".");
			return true;
		}

		SectorWorld world = getManager().getWorld(target.getWorld());
		sendMessage(player, "That player is " + C.green + "Online " + C.gray + "at " + C.yellow
				+ world.getSector().getName() + "-" + world.getId() + C.gray + ".");
		return true;
	}
}
