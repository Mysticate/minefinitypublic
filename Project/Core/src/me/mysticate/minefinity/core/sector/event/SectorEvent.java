package me.mysticate.minefinity.core.sector.event;

import org.bukkit.World;
import org.bukkit.event.Event;

import me.mysticate.minefinity.core.sector.SectorWorld;

public abstract class SectorEvent extends Event
{
	private SectorWorld _sector;

	public SectorEvent(World world, SectorWorld sector)
	{
		_sector = sector;
	}

	public SectorWorld getWorld()
	{
		return _sector;
	}
}
