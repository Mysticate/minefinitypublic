package me.mysticate.minefinity.core.sector.command;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.core.sector.SectorManager;

public class CommandTp extends CommandBase<SectorManager>
{
	public CommandTp(CommandManager commandManager, SectorManager manager)
	{
		super(commandManager, manager, new String[] { "tp", "teleport" }, Rank.ADMIN, "<Player>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length == 0)
			return false;

		Player target = Bukkit.getPlayer(args[0]);
		if (target == null)
		{
			sendMessage(player, "That player is not online!");
			return true;
		}

		if (target.getWorld() != player.getWorld())
		{
			sendMessage(player, "You may only teleport to players in the same world.");
			return true;
		}

		player.teleport(target);
		sendMessage(player, "Teleported to " + C.yellow + target.getName() + C.gray + ".");
		return true;
	}
}
