package me.mysticate.minefinity.core.sector;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.world.WorldHandler;

public class SectorWorld implements Comparable<SectorWorld>
{
	private World _world;

	private Sector _sector;
	private int _id;

	public SectorWorld(World world, Sector sector, int id)
	{
		_world = world;

		_sector = sector;
		_id = id;
	}

	public World getWorld()
	{
		return _world;
	}

	public Sector getSector()
	{
		return _sector;
	}

	public int getId()
	{
		return _id;
	}

	public void join(Player player)
	{
		if (IM.world() != null)
		{
			WorldHandler handler = IM.world().getHandler(_world);
			if (handler != null)
			{
				Location loc = handler.getJoinLocation(player);
				if (loc != null)
				{
					player.teleport(loc);
					return;
				}
			}
		}

		player.teleport(_world.getSpawnLocation());
	}

	@Override
	public int compareTo(SectorWorld o)
	{
		return o._id == _id ? 0 : o._id < _id ? 1 : -1;
	}
}
