package me.mysticate.minefinity.core.sector.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import me.mysticate.minefinity.core.sector.Sector;

public class SectorChangeEvent extends PlayerEvent
{
	private static HandlerList _handlers = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	private Sector _from;
	private Sector _to;

	public SectorChangeEvent(Player who, Sector from, Sector to)
	{
		super(who);

		_from = from;
		_to = to;
	}

	public Sector getFrom()
	{
		return _from;
	}

	public Sector getTo()
	{
		return _to;
	}
}
