package me.mysticate.minefinity.core.sector;

public enum Sector
{
	UNDEFINED("Unknown"), HUB("Hub"), BATTLE_ARENA("BA"), CRYSTAL_JUMP("CJ");

	private String _name;

	private Sector(String name)
	{
		_name = name;
	}

	public String getName()
	{
		return _name;
	}

	public static Sector getByName(String name)
	{
		for (Sector sector : Sector.values())
		{
			if (sector.getName().equals(name))
				return sector;
		}
		return null;
	}
}
