package me.mysticate.minefinity.core.sector;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.PlayerManager;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilMath;
import me.mysticate.minefinity.core.common.util.UtilPlayer;
import me.mysticate.minefinity.core.file.DataFolder;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.core.sector.command.CommandList;
import me.mysticate.minefinity.core.sector.command.CommandLocate;
import me.mysticate.minefinity.core.sector.command.CommandTp;
import me.mysticate.minefinity.core.sector.event.SectorChangeEvent;
import me.mysticate.minefinity.core.sector.event.WorldSectorChangeEvent;

public class SectorManager extends PlayerManager<ServerPlayer>
{
	private RankManager _rankManager;

	private HashMap<Sector, List<SectorWorld>> _worldMap = new HashMap<Sector, List<SectorWorld>>();
	private HashSet<ISectorHandler> _handlers = new HashSet<ISectorHandler>();

	public SectorManager(JavaPlugin plugin, RankManager rankManager)
	{
		super(plugin, "Sector");

		_rankManager = rankManager;
	}

	@Override
	public void registerCommands(CommandManager commandManager)
	{
		commandManager.registerCommand(new CommandTp(commandManager, this));
		commandManager.registerCommand(new CommandList(commandManager, this, _rankManager));
		commandManager.registerCommand(new CommandLocate(commandManager, this));
	}

	@Override
	public void onEnable()
	{

		loadPermanentWorlds();

		for (World world : Bukkit.getWorlds())
		{
			attemptLoad(world);
		}
	}

	public HashSet<Player> getPlayers(Sector sector)
	{
		HashSet<Player> players = new HashSet<Player>();

		for (Player player : getData().keySet())
		{
			if (getPlayer(player).getSector() == sector)
			{
				players.add(player);
			}
		}

		return players;
	}

	public SectorWorld getWorld(World world)
	{
		for (Sector sector : Sector.values())
		{
			if (!_worldMap.containsKey(sector))
				_worldMap.put(sector, new ArrayList<SectorWorld>());

			for (SectorWorld sworld : _worldMap.get(sector))
			{
				if (sworld.getWorld() == world)
					return sworld;
			}
		}

		return null;
	}

	public SectorWorld getWorld(Sector sector, int id)
	{
		if (!_worldMap.containsKey(sector))
			_worldMap.put(sector, new ArrayList<SectorWorld>());

		for (SectorWorld sworld : _worldMap.get(sector))
		{
			if (sworld.getId() == id)
				return sworld;
		}
		return null;
	}

	public Sector getSector(World world)
	{
		SectorWorld sworld = getWorld(world);
		if (sworld == null)
			return Sector.UNDEFINED;

		return sworld.getSector();
	}

	public void setSector(World world, Sector sector)
	{
		if (getSector(world) != Sector.UNDEFINED || getSector(world) == sector)
			return;

		wipe(world);

		int id = getNextId(sector);
		if (id == -1)
			throw new IllegalArgumentException("Could not generate a valid sector ID!");

		SectorWorld sworld = new SectorWorld(world, sector, id);
		_worldMap.get(sector).add(sworld);

		for (ISectorHandler handler : _handlers)
		{
			if (handler.getSector() == sector)
				handler.onWorldJoinSector(sworld);
		}

		Bukkit.getPluginManager().callEvent(new WorldSectorChangeEvent(world, sworld));
	}

	public void wipe(World world)
	{
		for (Sector cur : Sector.values())
		{
			if (!_worldMap.containsKey(cur))
				_worldMap.put(cur, new ArrayList<SectorWorld>());

			for (SectorWorld curw : new ArrayList<SectorWorld>(_worldMap.get(cur)))
			{
				if (curw.getWorld() == world)
				{
					_worldMap.get(cur).remove(curw);

					for (ISectorHandler handler : _handlers)
					{
						if (handler.getSector() == cur)
						{
							handler.onWorldExitSector(curw);
						}
					}
				}
			}
		}
	}

	public int getNextId(Sector sector)
	{
		if (!_worldMap.containsKey(sector))
			_worldMap.put(sector, new ArrayList<SectorWorld>());

		for (int i = 0; i <= _worldMap.get(sector).size();)
		{
			i++;

			boolean matches = false;
			for (SectorWorld world : _worldMap.get(sector))
			{
				if (world.getId() == i)
					matches = true;
			}

			if (!matches)
				return i;
		}

		return -1;
	}

	public int getId(World world)
	{
		SectorWorld sworld = getWorld(world);
		if (sworld == null)
			return -1;

		return sworld.getId();
	}

	public List<SectorWorld> getWorlds(Sector sector)
	{
		if (!_worldMap.containsKey(sector))
			return new ArrayList<SectorWorld>();

		return new ArrayList<SectorWorld>(_worldMap.get(sector));
	}

	public void registerSectorHandler(ISectorHandler handler)
	{
		_handlers.add(handler);
	}

	public void unregisterSectorHandler(ISectorHandler handler)
	{
		_handlers.remove(handler);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onSectorJoin(PlayerJoinEvent event)
	{
		ServerPlayer player = getPlayer(event.getPlayer());

		player.setSector(getSector(event.getPlayer().getWorld()));

		if (player.getSector() != Sector.HUB)
		{
			if (!sendToSector(event.getPlayer(), Sector.HUB))
			{
				event.getPlayer()
						.kickPlayer(F.kick(C.red + "We were not able to connect you to a hub.", "Please try again."));
				return;
			}
		}

		getWorld(event.getPlayer().getWorld()).join(event.getPlayer());
		reset(event.getPlayer());

		for (ISectorHandler handler : _handlers)
		{
			if (handler.getSector() == player.getSector())
			{
				handler.onSectorJoin(event.getPlayer());
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onSectorQuit(PlayerQuitEvent event)
	{
		ServerPlayer player = getPlayer(event.getPlayer());

		for (ISectorHandler handler : _handlers)
		{
			if (handler.getSector() == player.getSector())
			{
				handler.onSectorQuit(event.getPlayer());
			}
		}

		updateVisibility();
	}

	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent event)
	{
		ServerPlayer player = getPlayer(event.getPlayer());

		Sector from = getSector(event.getFrom());
		Sector to = getSector(event.getPlayer().getWorld());

		player.setSector(to);

		System.out.println("World Change: " + event.getPlayer().getName() + "  Previous World: "
				+ event.getFrom().getName() + "  Current World: " + event.getPlayer().getWorld().getName()
				+ "  Current Sector: " + player.getSector().getName());

		reset(event.getPlayer());

		if (from == to)
		{
			for (ISectorHandler handler : _handlers)
			{
				handler.onSectorWorldChange(event.getPlayer(), event.getPlayer().getWorld(), event.getFrom());
			}

			return;
		}

		for (ISectorHandler handler : _handlers)
		{
			if (handler.getSector() == player.getLastSector())
			{
				handler.onSectorQuit(event.getPlayer());
			}

			if (handler.getSector() == player.getSector())
			{
				handler.onSectorJoin(event.getPlayer());
			}
		}

		Bukkit.getPluginManager()
				.callEvent(new SectorChangeEvent(event.getPlayer(), player.getLastSector(), player.getSector()));
	}

	public void reset(Player player)
	{
		updateVisibility();
		UtilPlayer.reset(player);

		HudManager.get(player).setHeader("\n" + "       " + C.dPurpleBold + "MINEFINITY NETWORK       \n");
		HudManager.get(player).setFooter("\n" + C.green + C.underline + getPlayer(player).getSector().getName() + "-"
				+ getWorld(player.getWorld()).getId());
	}

	public void updateVisibility()
	{
		for (Player player : Bukkit.getOnlinePlayers())
		{
			for (Player cur : Bukkit.getOnlinePlayers())
			{
				if (player.getWorld() == cur.getWorld())
				{
					player.showPlayer(cur);
				} else
				{
					player.hidePlayer(cur);
				}
			}
		}
	}

	public boolean sendToSector(Player player, Sector sector)
	{
		System.out.println("Attempting to send " + player.getName() + " to sector " + sector.getName());

		ArrayList<SectorWorld> worlds = new ArrayList<SectorWorld>();
		worlds.addAll(getWorlds(sector));

		if (worlds.isEmpty())
			return false;

		UtilMath.getRandom(worlds).join(player);
		return true;
	}

	public void attemptLoad(World world)
	{
		if (getSector(world) != Sector.UNDEFINED)
			return;

		File file = new File(world.getWorldFolder() + File.separator + "sector.yml");

		if (!file.exists())
		{
			try
			{
				file.createNewFile();
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}

		if (!file.exists())
			return;

		YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

		if (!config.contains("sector"))
		{
			config.set("sector", getSector(world).toString());

			try
			{
				config.save(file);
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}

		Sector sector = null;

		try
		{
			sector = Sector.valueOf(config.getString("sector"));
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}

		if (sector == null)
			return;

		System.out.println("Successfully loaded " + world.getName() + " to sector " + sector.getName());

		setSector(world, sector);
	}

	public void loadPermanentWorlds()
	{
		File file = DataFolder.getExistingWorlds().attemptCreate();

		if (!file.exists())
			return;

		YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
		if (!config.contains("worlds"))
		{
			config.set("worlds", new ArrayList<String>());

			try
			{
				config.save(file);
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}
			return;
		}

		List<String> list = config.getStringList("worlds");
		for (String name : list)
		{
			if (Bukkit.getWorld(name) != null)
				continue;

			File worldFile = new File(name);
			if (!worldFile.exists())
				continue;

			File worldConfig = new File(name, "sector.yml");
			if (!worldConfig.exists())
				continue;

			Bukkit.createWorld(new WorldCreator(name).type(WorldType.FLAT));
		}
	}

	@EventHandler
	public void onLoad(WorldLoadEvent event)
	{
		attemptLoad(event.getWorld());
	}

	@EventHandler
	public void onUnload(WorldUnloadEvent event)
	{
		wipe(event.getWorld());
	}

	@Override
	protected ServerPlayer newData(Player player)
	{
		return new ServerPlayer(player.getUniqueId());
	}
}
