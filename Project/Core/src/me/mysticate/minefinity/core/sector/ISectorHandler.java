package me.mysticate.minefinity.core.sector;

import org.bukkit.World;
import org.bukkit.entity.Player;

public interface ISectorHandler
{
	public SectorManager getSectorManager();

	public Sector getSector();

	public void onSectorJoin(Player player);

	public void onSectorQuit(Player player);

	public void onSectorWorldChange(Player player, World to, World from);

	public void onWorldJoinSector(SectorWorld sector);

	public void onWorldExitSector(SectorWorld sector);
}
