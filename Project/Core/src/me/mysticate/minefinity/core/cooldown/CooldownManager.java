package me.mysticate.minefinity.core.cooldown;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.PlayerManager;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;

public class CooldownManager extends PlayerManager<CooldownPlayer>
{
	public CooldownManager(JavaPlugin plugin)
	{
		super(plugin, "Cooldown");
	}

	public boolean canUse(Player player, String name)
	{
		return canUse(CooldownPool.ALL, player, name);
	}

	public boolean canUse(CooldownPool pool, Player player, String name)
	{
		return getPlayer(player).hasCooldown(pool, name);
	}

	public boolean use(Player player, String name, long time)
	{
		return use(CooldownPool.ALL, player, name, time);
	}

	public boolean use(CooldownPool pool, Player player, String name, long time)
	{
		return getPlayer(player).addCooldown(pool, name, new Cooldown(time));
	}

	public void reset(Player player, String name)
	{
		reset(CooldownPool.ALL, player, name);
	}

	public void reset(CooldownPool pool, Player player, String name)
	{
		getPlayer(player).removeCooldown(pool, name);
	}

	@EventHandler
	public void tickCooldowns(TimeEvent event)
	{
		if (event.getType() != UpdateType.TICK)
			return;

		for (Player player : getData().keySet())
		{
			getPlayer(player).tick();
		}
	}

	@Override
	protected CooldownPlayer newData(Player player)
	{
		return new CooldownPlayer(player);
	}
}
