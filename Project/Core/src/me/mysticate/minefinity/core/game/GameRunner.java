package me.mysticate.minefinity.core.game;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class GameRunner
{
	public static void openGameJoiner(Player player, String game)
	{
		Bukkit.getPluginManager().callEvent(new RequestPlayerOpenGameJoinerEvent(player, game));
	}

	public static void openGameMenu(Player player)
	{
		Bukkit.getPluginManager().callEvent(new RequestPlayerOpenGameMenuEvent(player));
	}

	public static void openGameOptions(Player player, String game)
	{
		Bukkit.getPluginManager().callEvent(new RequestPlayerOpenGameOptionsEvent(player, game));
	}

	public static void openOptionsMenu(Player player)
	{
		Bukkit.getPluginManager().callEvent(new RequestPlayerOpenOptionsMenuEvent(player));
	}
}
