package me.mysticate.minefinity.core.item;

import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.PlayerManager;
import me.mysticate.minefinity.core.common.util.UtilInv;

public class ItemManager extends PlayerManager<ItemPlayer>
{
	public ItemManager(JavaPlugin core)
	{
		super(core, "Item");
	}

	public void setItem(Player player, int slot, UseableItem item)
	{
		setItem("all", player, slot, item);
	}

	public void setItem(String key, Player player, int slot, UseableItem item)
	{
		player.getInventory().setItem(slot, item);
		getPlayer(player).addItem(key, item);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onInteract(PlayerInteractEvent event)
	{
		if (event.getAction() == Action.PHYSICAL)
			return;

		if (event.getItem() == null)
			return;

		for (Set<UseableItem> set : getPlayer(event.getPlayer()).getItems().values())
		{
			for (UseableItem item : set)
			{
				if (!item.isSimilar(event.getItem()))
					continue;

				if (item.getActionType() != null && item.getActionType() != getAction(event))
					continue;

				if (!item.isAllowInteract())
				{
					event.setCancelled(true);
					UtilInv.update(event.getPlayer());
				}

				if (!IM.cooldown().use(event.getPlayer(), item.toString(), 250))
					continue;

				item.onClick(event.getPlayer(), event.getAction());
				break;
			}
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onDrop(PlayerDropItemEvent event)
	{
		for (Set<UseableItem> set : getPlayer(event.getPlayer()).getItems().values())
		{
			for (UseableItem item : set)
			{
				if (!item.isSimilar(event.getItemDrop().getItemStack()))
					continue;

				if (!item.isDroppable())
				{
					event.setCancelled(true);
					UtilInv.update(event.getPlayer());
					return;
				}
			}
		}
	}

	private InteractAction getAction(PlayerInteractEvent event)
	{
		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
			return InteractAction.RIGHT;

		if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)
			return InteractAction.LEFT;

		return InteractAction.ACTION;
	}

	@Override
	protected ItemPlayer newData(Player player)
	{
		return new ItemPlayer(player.getUniqueId());
	}
}
