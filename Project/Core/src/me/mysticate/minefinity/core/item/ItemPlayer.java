package me.mysticate.minefinity.core.item;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import me.mysticate.minefinity.core.player.PlayerData;

public class ItemPlayer extends PlayerData
{
	private HashMap<String, Set<UseableItem>> _items = new HashMap<String, Set<UseableItem>>();

	public ItemPlayer(UUID id)
	{
		super(id);
	}

	public void addItem(UseableItem item)
	{
		addItem("all", item);
	}

	public void addItem(String key, UseableItem item)
	{
		if (!_items.containsKey(key))
			_items.put(key, new HashSet<UseableItem>());

		_items.get(key).add(item);
	}

	public void removeItem(UseableItem item)
	{
		for (String key : _items.keySet())
		{
			if (_items.get(key).contains(item))
				_items.get(key).remove(item);
		}
	}

	public HashMap<String, Set<UseableItem>> getItems()
	{
		return _items;
	}

	public Set<UseableItem> getItems(String key)
	{
		if (!_items.containsKey(key))
			return new HashSet<UseableItem>();

		return _items.get(key);
	}
}
