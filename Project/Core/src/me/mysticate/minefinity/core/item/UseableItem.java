package me.mysticate.minefinity.core.item;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;

public class UseableItem extends ItemStack
{
	private final ItemCallback _action;

	private boolean _droppable = true;
	private boolean _allowInteract = false;

	private InteractAction _type = null;

	public UseableItem(Material mat, ItemCallback action)
	{
		this(mat, (byte) 0, action);
	}

	public UseableItem(Material mat, int amount, ItemCallback action)
	{
		this(mat, amount, (byte) 0, action);
	}

	public UseableItem(Material mat, byte data, ItemCallback action)
	{
		this(mat, 1, data, action);
	}

	public UseableItem(Material mat, int amount, byte data, ItemCallback action)
	{
		super(mat, amount, data);

		_action = action;
	}

	@SuppressWarnings("deprecation")
	public UseableItem(ItemStack item, ItemCallback action)
	{
		this(item.getType(), item.getAmount(), item.getData().getData(), action);

		setItemMeta(item.getItemMeta());
		setDurability(item.getDurability());

		addUnsafeEnchantments(item.getEnchantments());
	}

	public void onClick(Player player, Action action)
	{
		_action.onClick(player, action);
	}

	public void setDroppable(boolean flag)
	{
		_droppable = flag;
	}

	public boolean isDroppable()
	{
		return _droppable;
	}

	public void setAllowInteract(boolean flag)
	{
		_allowInteract = flag;
	}

	public boolean isAllowInteract()
	{
		return _allowInteract;
	}

	public void setType(InteractAction type)
	{
		_type = type;
	}

	public InteractAction getActionType()
	{
		return _type;
	}
}
