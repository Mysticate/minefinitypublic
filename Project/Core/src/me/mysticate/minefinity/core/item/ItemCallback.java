package me.mysticate.minefinity.core.item;

import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

public interface ItemCallback
{
	public void onClick(Player player, Action type);
}
