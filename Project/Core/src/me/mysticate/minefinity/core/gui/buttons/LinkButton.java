package me.mysticate.minefinity.core.gui.buttons;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;

public class LinkButton implements Button
{
	private ItemStack _item;
	private String _link;

	public LinkButton(ItemStack item, String link)
	{
		_item = item;
		_link = link;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return _item;
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		player.closeInventory();
		player.sendMessage(F.manager("Shop", "Click to open website " + C.blue + C.underline + _link));
	}
}
