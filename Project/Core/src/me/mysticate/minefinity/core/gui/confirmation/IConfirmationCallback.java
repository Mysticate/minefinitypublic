package me.mysticate.minefinity.core.gui.confirmation;

public interface IConfirmationCallback
{
	public void run(ConfirmationResponse response);
}
