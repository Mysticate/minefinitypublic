package me.mysticate.minefinity.core.gui.buttons;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;

public class ItemButton implements Button
{
	private ItemStack _item;

	public ItemButton(ItemStack item)
	{
		_item = item;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return _item;
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{

	}
}
