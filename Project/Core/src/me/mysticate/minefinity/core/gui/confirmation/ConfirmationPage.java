package me.mysticate.minefinity.core.gui.confirmation;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.buttons.ItemButton;

public class ConfirmationPage extends Gui
{
	private IConfirmationCallback _callback;

	private String _displayName;
	private String _withdrawl;

	private boolean _finished = false;

	private boolean _closeOnRespond = true;

	public ConfirmationPage(JavaPlugin plugin, Player player, IConfirmationCallback callback, String displayName,
			String withdrawl, boolean closeOnRespond)
	{
		super(plugin, player, "Confirmation", 9);

		_callback = callback;

		_displayName = displayName;
		_withdrawl = withdrawl;

		_closeOnRespond = closeOnRespond;
	}

	private void respond(ConfirmationResponse response)
	{
		if (_finished)
			return;

		_finished = true;

		if (_closeOnRespond)
			getPlayer().closeInventory();

		_callback.run(response);
	}

	@Override
	protected void addItems()
	{
		for (int i = 0; i < 4; i++)
		{
			setItem(i, new Button()
			{
				@Override
				public ItemStack getItem(Gui gui, Player player)
				{
					return UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 5, 1, C.greenBold + "CONFIRM",
							new String[] { " ", C.gray + "Click to confirm", C.gray + "the purchase of", " ",
									C.yellow + _displayName + C.gray + " for", C.gold + _withdrawl });
				}

				@Override
				public void onClick(Gui gui, Player player, ClickType type)
				{
					respond(ConfirmationResponse.CONFIRM);
				}
			});
		}

		setItem(4, new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 15, 1, " ")));

		for (int i = 5; i < 9; i++)
		{
			setItem(i, new Button()
			{
				@Override
				public ItemStack getItem(Gui gui, Player player)
				{
					return UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 14, 1, C.redBold + "DENY",
							new String[] { " ", C.gray + "Click to deny", C.gray + "the purchase of", " ",
									C.yellow + _displayName + C.gray + " for", C.gold + _withdrawl });
				}

				@Override
				public void onClick(Gui gui, Player player, ClickType type)
				{
					respond(ConfirmationResponse.CANCEL_CLICK);
				}
			});
		}
	}

	@Override
	protected void onOpen()
	{

	}

	@Override
	protected void onClose()
	{
		if (_finished)
			return;

		respond(ConfirmationResponse.CANCEL_CLOSE);
	}
}
