package me.mysticate.minefinity.core.gui.buttons;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;

public class NextButton implements Button
{
	public static interface ButtonForeward
	{
		public void onForeward(Gui gui, Player player, ClickType type);
	}

	private ButtonForeward _back;
	private String _name;

	public NextButton(ButtonForeward back, String name)
	{
		_back = back;
		_name = name;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.WOOD_BUTTON, (byte) 0, 1,
				C.white + "Go to " + C.purple + _name + C.green + " \u25B6");
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		_back.onForeward(gui, player, type);
	}
}
