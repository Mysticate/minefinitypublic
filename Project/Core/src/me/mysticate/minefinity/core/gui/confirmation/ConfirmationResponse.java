package me.mysticate.minefinity.core.gui.confirmation;

public enum ConfirmationResponse
{
	CANCEL_CLOSE, CANCEL_CLICK, CONFIRM
}
