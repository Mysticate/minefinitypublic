package me.mysticate.minefinity.core.perk;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.DataPlayerManager;

public class PerkManager extends DataPlayerManager<PerkPlayer>
{
	public PerkManager(JavaPlugin plugin)
	{
		super(plugin, "Perk");
	}

	@Override
	protected PerkPlayer newData(Player player)
	{
		return new PerkPlayer(player.getUniqueId());
	}

	@Override
	protected PerkPlayer newOfflineData(UUID uuid)
	{
		return new PerkPlayer(uuid);
	}
}
