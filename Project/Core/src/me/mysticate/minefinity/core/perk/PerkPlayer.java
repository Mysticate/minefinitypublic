package me.mysticate.minefinity.core.perk;

import java.util.HashSet;
import java.util.UUID;

import me.mysticate.minefinity.core.file.DataFolder;
import me.mysticate.minefinity.core.file.YamlF;
import me.mysticate.minefinity.core.player.DataPlayer;

public class PerkPlayer extends DataPlayer
{
	private HashSet<Perk> _perks = new HashSet<Perk>();

	public PerkPlayer(UUID id)
	{
		super(id, DataFolder.getPerks(id));
	}

	@Override
	public void load()
	{
		for (String key : readLines("perks"))
		{
			setLevel(key, Integer.valueOf(read(YamlF.path("perks", key), "1")));
		}
	}

	@Override
	public void save()
	{
		if (!_perks.isEmpty())
			write("", "perks");

		for (Perk perk : _perks)
		{
			write(perk.Level + "", YamlF.path("perks", perk.Name));
		}
	}

	public HashSet<Perk> getPerks()
	{
		return _perks;
	}

	public boolean hasPerk(String name)
	{
		Perk perk = getPerk(name);
		if (perk == null)
			return false;

		return perk.Level > 0;
	}

	public void buyPerk(String name)
	{
		buyPerk(name, 1);
	}

	public void buyPerk(String name, int amount)
	{
		if (!hasPerk(name))
		{
			_perks.add(new Perk(name, amount));
		} else
		{
			getPerk(name).Level += amount;
		}
	}

	public void sellPerk(String name)
	{
		sellPerk(name, 1);
	}

	public void sellPerk(String name, int amount)
	{
		if (!hasPerk(name))
			_perks.add(new Perk(name));

		Perk perk = getPerk(name);
		perk.Level = Math.max(0, perk.Level - amount);
	}

	public int getLevel(String name)
	{
		if (!hasPerk(name))
			return 0;

		return getPerk(name).Level;
	}

	public void setLevel(String name, int level)
	{
		if (getPerk(name) == null)
			_perks.add(new Perk(name));

		getPerk(name).Level = level;
	}

	public Perk getPerk(String name)
	{
		for (Perk perk : _perks)
		{
			if (perk.Name.equals(name))
				return perk;
		}

		return null;
	}
}
