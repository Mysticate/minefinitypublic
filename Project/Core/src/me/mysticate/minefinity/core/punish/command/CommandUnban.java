package me.mysticate.minefinity.core.punish.command;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.DataPlayerManager.OfflineDataCallback;
import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.punish.PunishManager;
import me.mysticate.minefinity.core.punish.PunishPlayer;
import me.mysticate.minefinity.core.punish.Punishment;
import me.mysticate.minefinity.core.punish.PunishmentType;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandUnban extends CommandBase<PunishManager>
{
	public CommandUnban(CommandManager commandManager, PunishManager manager)
	{
		super(commandManager, manager, new String[] { "unban", "pardon", }, Rank.MOD, "<Player> <Reason>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length < 2)
		{
			return false;
		}

		final String playerName = args[0];

		String m = "";
		for (int i = 1; i < args.length; i++)
			m += (args[i] + (i == args.length - 1 ? "" : " "));

		final String message = m;

		sendMessage(player, "Processing request...");

		try
		{
			if (!getManager().loadOfflineData(playerName, new OfflineDataCallback<PunishPlayer>()
			{
				@Override
				public void onLoad(PunishPlayer data, boolean offline)
				{
					int removed = 0;
					for (Punishment punishment : data.getActive())
					{
						if (punishment.getType() == PunishmentType.Ban)
						{
							punishment.remove(player.getName(), message);
							removed++;
						}
					}

					sendMessage(player, "Removed " + C.yellow + removed + " Ban(s)" + C.gray + " from player "
							+ C.yellow + playerName + C.gray + ".");

					if (offline)
					{
						data.save();
					}
				}
			}))
			{
				sendMessage(player, "Unknown player " + C.yellow + playerName + C.gray + ".");
			}
		} catch (Exception ex)
		{
			sendMessage(player, C.red + "An error occured while processing your request. Is that the correct name?");
		}

		return true;
	}
}
