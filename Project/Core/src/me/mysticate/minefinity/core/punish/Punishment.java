package me.mysticate.minefinity.core.punish;

public class Punishment
{
	private PunishmentType _type;

	private String _punisher;
	private String _reason;

	private long _creation;
	private long _duration;

	private Removal _removed;

	public Punishment(PunishmentType type, String punisher, String reason, long creation, long duration,
			Removal removed)
	{
		_type = type;

		_punisher = punisher;
		_reason = reason;

		_creation = creation;
		_duration = duration;

		_removed = removed;
	}

	public PunishmentType getType()
	{
		return _type;
	}

	public String getPunisher()
	{
		return _punisher;
	}

	public String getReason()
	{
		return _reason;
	}

	public long getCreationTime()
	{
		return _creation;
	}

	public long getDuration()
	{
		return _duration;
	}

	public long getExpiration()
	{
		return _creation + _duration;
	}

	public long getRemainder()
	{
		return getExpiration() - System.currentTimeMillis();
	}

	public Removal getRemoval()
	{
		return _removed;
	}

	public void remove(String name, String reason)
	{
		_removed = new Removal(name, reason, System.currentTimeMillis());
	}

	public boolean isActive()
	{
		return _removed == null && _creation + _duration >= System.currentTimeMillis();
	}
}
