package me.mysticate.minefinity.core.punish.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.DataPlayerManager.OfflineDataCallback;
import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.common.util.UtilTime;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.buttons.ItemButton;
import me.mysticate.minefinity.core.punish.PunishManager;
import me.mysticate.minefinity.core.punish.PunishPlayer;
import me.mysticate.minefinity.core.punish.Punishment;
import me.mysticate.minefinity.core.punish.PunishmentType;
import me.mysticate.minefinity.core.rank.Rank;

public class GuiPunishments extends Gui implements Runnable
{
	private PunishManager _punishManager;
	
	private String _target;
	private PunishmentType _type = PunishmentType.Mute;
	
	private int _taskId;
	
	public GuiPunishments(JavaPlugin plugin, PunishManager punishManager, String target, Player player)
	{
		super(plugin, player, "Punishments", 36);
		
		_target = target;
		_punishManager = punishManager;
	}
	
	public PunishmentType getFocus()
	{
		return _type;
	}

	@Override
	protected void addItems()
	{
		clear();
		
		setItem(0, new ItemButton(UtilItemStack.create(Material.NAME_TAG, (byte) 0, 1, C.greenBold + _target)));
		setItem(9, new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 15, 1, " ")));
		setItem(18, new ButtonPunishmentType(this, PunishmentType.Mute, Material.BOOK));
		setItem(27, new ButtonPunishmentType(this, PunishmentType.Ban, Material.REDSTONE_BLOCK));
		
		setItem(1, new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 15, 1, " ")));
		setItem(10, new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 15, 1, " ")));
		setItem(19, new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 15, 1, " ")));
		setItem(28, new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 15, 1, " ")));
	
		updatePunishments();
	}
	
	private void updatePunishments()
	{
		final List<Punishment> active = new ArrayList<Punishment>();

		try
		{
			if (!_punishManager.loadOfflineData(_target, new OfflineDataCallback<PunishPlayer>()
			{
				@Override
				public void onLoad(PunishPlayer data, boolean offline)
				{
					active.addAll(data.getAll());
				}
			}))
			{
				getPlayer().closeInventory();
				
				_punishManager.sendMessage(getPlayer(), "Unknown player " + C.yellow + _target + C.gray + ".");
				return;
			}
		} catch (Exception ex)
		{
			getPlayer().closeInventory();
			
			_punishManager.sendMessage(getPlayer(), "An error occured while processing your request. Is that the correct player name?");
			return;
		}
		
		List<Punishment> sorted = new ArrayList<Punishment>();

		for (Punishment punishment : active)
		{
			if (punishment.getType() == _type)
			{
				sorted.add(punishment);
			}
		}
		
		Collections.sort(sorted, _punishSort);
		
		int count = 0;
		for (Punishment punish : sorted)
		{
			if (count > 28)
				break;
			
			if (!IM.rank().isOwned(getPlayer(), Rank.MOD))
				if (!punish.isActive())
					continue;
			
			count++;
			
			List<String> lore = new ArrayList<String>();
			
			lore.add(" ");
			lore.add(C.aquaBold + "Reason " + C.white + punish.getReason());
			lore.add(C.aquaBold + "Expiration " + (punish.isActive() ? (C.white + UtilTime.getLongestTime(punish.getRemainder()).display()) : (C.red + "Expired")));
			
			if (IM.rank().isOwned(getPlayer(), Rank.HELPER))
			{
				lore.add(" ");
				lore.add(C.dAquaBold + "Staff " + C.white + punish.getPunisher());
				lore.add(C.dAquaBold + "Duration " + C.white + UtilTime.getLongestTime(punish.getDuration()).display());
			}
			
			if (punish.getRemoval() != null)
			{
				lore.add(" ");
				lore.add(C.redBold + "Removal " + C.white + punish.getRemoval().getReason());
				lore.add(C.redBold + "Remover " + C.white + punish.getRemoval().getRemover());
			}
			
			addItem(new ItemButton(UtilItemStack.create(punish.isActive() ? Material.WRITTEN_BOOK : Material.BOOK, (byte) 0, 1, C.greenBold + _type, lore.toArray(new String[0]))));
		}
		
		updateInventory();
	}
	
	public void shiftFocus(PunishmentType type)
	{
		if (_type == type)
			return;
		
		_type = type;
		addItems();
	}

	@Override
	protected void onOpen()
	{
		_taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(MinefinityPlugin.getPlugin(), this, 0L, 20L);
	}

	@Override
	protected void onClose()
	{
		Bukkit.getScheduler().cancelTask(_taskId);
	}

	private final Comparator<Punishment> _punishSort = new Comparator<Punishment>()
	{
		@Override
		public int compare(Punishment o1, Punishment o2)
		{
			if (o1.isActive() || o2.isActive())
			{
				if (o2.isActive() && o1.isActive())
				{
					return o1.getCreationTime() == o2.getCreationTime() ? 0 : (o1.getCreationTime() > o2.getCreationTime() ? -1 : 1);
				}
				
				return o1.isActive() ? -1 : 1;
			}
			
			return o1.getCreationTime() == o2.getCreationTime() ? 0 : (o1.getCreationTime() > o2.getCreationTime() ? -1 : 1);
		}
	};

	@Override
	public void run()
	{
		addItems();
	}
}
