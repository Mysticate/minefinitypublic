package me.mysticate.minefinity.core.punish;

public class Removal
{
	private String _remover;
	private String _reason;
	private long _timestamp;

	public Removal(String remover, String reason, long timestamp)
	{
		_remover = remover;
		_reason = reason;
		_timestamp = timestamp;
	}

	public String getRemover()
	{
		return _remover;
	}

	public String getReason()
	{
		return _reason;
	}

	public long getTimestamp()
	{
		return _timestamp;
	}
}
