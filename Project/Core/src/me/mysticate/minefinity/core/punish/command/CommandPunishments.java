package me.mysticate.minefinity.core.punish.command;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.punish.PunishManager;
import me.mysticate.minefinity.core.punish.gui.GuiPunishments;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandPunishments extends CommandBase<PunishManager>
{
	public CommandPunishments(CommandManager commandManager, PunishManager manager)
	{
		super(commandManager, manager, new String[] { "punishments", "pls", "minefinitysucks" }, Rank.DEFAULT, null);
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		String target = "";
		
		if (args.length == 0)
		{
			target = player.getName();
		} else
		{
			target = args[0];

			sendMessage(player, "Loading punishments for " + C.yellow + target + C.gray + ".");
		}
		
		new GuiPunishments(getManager().getPlugin(), getManager(), target, player).openInventory();
		return true;
	}
}
