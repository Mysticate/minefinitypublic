package me.mysticate.minefinity.core.punish.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerBannedLoginEvent extends PlayerEvent
{
	private static HandlerList _handlers = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	public PlayerBannedLoginEvent(Player who)
	{
		super(who);
	}
}
