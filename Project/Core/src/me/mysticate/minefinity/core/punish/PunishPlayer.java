package me.mysticate.minefinity.core.punish;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import me.mysticate.minefinity.core.file.DataFolder;
import me.mysticate.minefinity.core.file.YamlF;
import me.mysticate.minefinity.core.player.DataPlayer;

public class PunishPlayer extends DataPlayer
{
	private List<Punishment> _punishments = new ArrayList<Punishment>();

	public PunishPlayer(UUID id)
	{
		super(id, DataFolder.getPunishments(id));
	}

	public void addBan(String punisher, String reason, long duration)
	{
		_punishments
				.add(new Punishment(PunishmentType.Ban, punisher, reason, System.currentTimeMillis(), duration, null));
	}

	public void addMute(String punisher, String reason, long duration)
	{
		_punishments
				.add(new Punishment(PunishmentType.Mute, punisher, reason, System.currentTimeMillis(), duration, null));
	}

	public List<Punishment> getActive()
	{
		List<Punishment> punishments = new ArrayList<Punishment>();
		for (Punishment punishment : _punishments)
		{
			if (punishment.isActive())
				punishments.add(punishment);
		}

		Collections.sort(punishments, _dateComparator);

		return punishments;
	}

	public List<Punishment> getAll()
	{
		Collections.sort(_punishments, _dateComparator);
		return _punishments;
	}

	public boolean isMuted()
	{
		return getBestMute() != null;
	}

	public boolean isBanned()
	{
		return getBestBan() != null;
	}

	public Punishment getBestMute()
	{
		Collections.sort(_punishments, _dateComparator);

		for (Punishment punishment : _punishments)
		{
			if (punishment.getType() == PunishmentType.Mute && punishment.isActive())
				return punishment;
		}

		return null;
	}

	public Punishment getBestBan()
	{
		Collections.sort(_punishments, _dateComparator);

		for (Punishment punishment : _punishments)
		{
			if (punishment.getType() == PunishmentType.Ban && punishment.isActive())
				return punishment;
		}

		return null;
	}

	@Override
	public void load()
	{
		for (String key : readLines("punishments"))
		{
			PunishmentType type = PunishmentType.valueOf(read(YamlF.path("punishments", key, "type"), null));

			String punisher = read(YamlF.path("punishments", key, "punisher"), null);
			String reason = read(YamlF.path("punishments", key, "reason"), null);

			long timestamp = Long.valueOf(read(YamlF.path("punishments", key, "timestamp"), "0"));
			long expiration = Long.valueOf(read(YamlF.path("punishments", key, "expire"), "0"));

			Removal remove = null;
			if (getConfig().contains(YamlF.path("punishments", key, "removal")))
			{
				String remover = read(YamlF.path("punishments", key, "removal", "remover"), null);
				String removeReason = read(YamlF.path("punishments", key, "removal", "reason"), null);
				long removeTimestamp = Long.valueOf(read(YamlF.path("punishments", key, "removal", "timestamp"), "0"));

				remove = new Removal(remover, removeReason, removeTimestamp);
			}

			_punishments.add(new Punishment(type, punisher, reason, timestamp, expiration, remove));
		}

		Collections.sort(_punishments, _dateComparator);
	}

	@Override
	public void save()
	{
		if (!_punishments.isEmpty())
			write("", "punishments");

		for (int i = 0; i < _punishments.size(); i++)
		{
			Punishment punishment = _punishments.get(i);

			write(punishment.getType().toString(), YamlF.path("punishments", i, "type"));

			write(punishment.getPunisher(), YamlF.path("punishments", i, "punisher"));
			write(punishment.getReason(), YamlF.path("punishments", i, "reason"));

			write(punishment.getCreationTime() + "", YamlF.path("punishments", i, "timestamp"));
			write(punishment.getDuration() + "", YamlF.path("punishments", i, "expire"));

			if (punishment.getRemoval() != null)
			{
				write(punishment.getRemoval().getRemover(), YamlF.path("punishments", i, "removal", "remover"));
				write(punishment.getRemoval().getReason(), YamlF.path("punishments", i, "removal", "reason"));
				write(punishment.getRemoval().getTimestamp() + "",
						YamlF.path("punishments", i, "removal", "timestamp"));
			}
		}
	}

	private static Comparator<Punishment> _dateComparator = new Comparator<Punishment>()
	{
		@Override
		public int compare(Punishment o1, Punishment o2)
		{
			if (o1.getCreationTime() == o2.getCreationTime())
				return 0;

			return o1.getCreationTime() < o2.getCreationTime() ? 1 : -1;
		}
	};
}
