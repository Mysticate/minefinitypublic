package me.mysticate.minefinity.core.punish;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.DataPlayerManager;
import me.mysticate.minefinity.core.chat.ChatEvent;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilTime;
import me.mysticate.minefinity.core.punish.command.CommandBan;
import me.mysticate.minefinity.core.punish.command.CommandMute;
import me.mysticate.minefinity.core.punish.command.CommandPunishments;
import me.mysticate.minefinity.core.punish.command.CommandUnban;
import me.mysticate.minefinity.core.punish.command.CommandUnmute;
import me.mysticate.minefinity.core.punish.events.PlayerBannedLoginEvent;
import me.mysticate.minefinity.core.punish.events.PlayerMutedChatEvent;

public class PunishManager extends DataPlayerManager<PunishPlayer>
{
	public PunishManager(JavaPlugin plugin)
	{
		super(plugin, "Punish");
	}

	@Override
	public void registerCommands(CommandManager commandManager)
	{
		commandManager.registerCommand(new CommandPunishments(commandManager, this));
		commandManager.registerCommand(new CommandBan(commandManager, this));
		commandManager.registerCommand(new CommandMute(commandManager, this));
		commandManager.registerCommand(new CommandUnban(commandManager, this));
		commandManager.registerCommand(new CommandUnmute(commandManager, this));
	}

	@EventHandler
	public void onMuted(ChatEvent event)
	{
		if (getPlayer(event.getPlayer()).isMuted())
		{
			event.setCancelled(true);
			sendMessage(event.getPlayer(),
					"You are currently muted. Type " + C.purple + "/punishments" + C.gray + " for information!");

			Bukkit.getPluginManager().callEvent(new PlayerMutedChatEvent(event.getPlayer(), event));
		}
	}

	@EventHandler
	public void onBanned(PlayerLoginEvent event)
	{
		PunishPlayer player = getPlayer(event.getPlayer());

		Punishment ban = player.getBestBan();
		if (ban == null)
			return;

		event.disallow(Result.KICK_BANNED,
				F.kick(C.redBold + "You have been banned from Minefinity",
						C.yellowBold + "Reason " + C.white + ban.getReason(),
						C.greenBold + "Remaining " + C.white + UtilTime.getLongestTime(ban.getRemainder()).display()));

		Bukkit.getPluginManager().callEvent(new PlayerBannedLoginEvent(event.getPlayer()));
	}

	public void enforceBans()
	{
		for (Player player : Bukkit.getOnlinePlayers())
		{
			Punishment ban = getPlayer(player).getBestBan();
			if (ban == null)
				continue;

			player.kickPlayer(F.kick(C.redBold + "You have been banned from Minefinity",
					C.yellowBold + "Reason " + C.white + ban.getReason(),
					C.greenBold + "Remaining " + C.white + UtilTime.getLongestTime(ban.getRemainder()).display()));
		}
	}

	@Override
	protected PunishPlayer newData(Player player)
	{
		return new PunishPlayer(player.getUniqueId());
	}

	@Override
	protected PunishPlayer newOfflineData(UUID uuid)
	{
		return new PunishPlayer(uuid);
	}
}
