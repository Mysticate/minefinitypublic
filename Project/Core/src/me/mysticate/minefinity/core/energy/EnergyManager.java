package me.mysticate.minefinity.core.energy;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.PlayerManager;
import me.mysticate.minefinity.core.updater.UpdateType;
import me.mysticate.minefinity.core.updater.event.TimeEvent;

public class EnergyManager extends PlayerManager<EnergyPlayer>
{
	public EnergyManager(JavaPlugin plugin)
	{
		super(plugin, "Energy");
	}

	@EventHandler
	public void onTick(TimeEvent event)
	{
		if (event.getType() != UpdateType.TICK)
			return;

		for (Player player : getData().keySet())
		{
			getPlayer(player).tick();
		}
	}

	@Override
	protected EnergyPlayer newData(Player player)
	{
		return new EnergyPlayer(player);
	}
}
