package me.mysticate.minefinity.core.energy;

public interface IEnergyDisplay
{
	public float getExpValue();

	public boolean shouldFlashWhenFull();
}
