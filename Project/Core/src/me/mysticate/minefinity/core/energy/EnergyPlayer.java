package me.mysticate.minefinity.core.energy;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.player.PlayerData;

public class EnergyPlayer extends PlayerData
{
	private boolean _enabled = true;

	private Player _player;
	private HashMap<ItemStack, IEnergyDisplay> _energy = new HashMap<ItemStack, IEnergyDisplay>();

	private boolean _flash = false;
	private int _flashTick = 0;

	public EnergyPlayer(Player player)
	{
		super(player.getUniqueId());

		_player = player;
	}

	public void setEnabled(boolean enabled)
	{
		_enabled = enabled;
	}

	public void tick()
	{
		if (!_enabled)
			return;

		ItemStack current = _player.getInventory().getItem(_player.getInventory().getHeldItemSlot());

		if (current == null)
		{
			_player.setExp(0);
			return;
		}

		boolean changed = false;
		for (ItemStack item : _energy.keySet())
		{
			if (item.isSimilar(current))
			{
				IEnergyDisplay display = _energy.get(item);

				if (display.getExpValue() >= .999)
				{
					if (display.shouldFlashWhenFull())
					{
						if (_flashTick >= 5)
						{
							_player.setExp(_flash ? .999F : 0F);

							_flashTick = 0;
							_flash = !_flash;
						}

						_flashTick++;
					}
				} else
				{
					_player.setExp(Math.max(Math.min(display.getExpValue(), .999F), 0));
				}

				changed = true;
				break;
			}
		}

		if (!changed)
		{
			_player.setExp(0F);
		}
	}

	public void add(ItemStack item, IEnergyDisplay display)
	{
		for (ItemStack other : new HashSet<ItemStack>(_energy.keySet()))
		{
			if (other.isSimilar(item))
			{
				_energy.remove(other);
			}
		}

		_energy.put(item, display);
	}

	public void clear()
	{
		_energy.clear();
	}
}
