package me.mysticate.minefinity.core;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.common.util.UniqueIdFetcher;
import me.mysticate.minefinity.core.player.DataPlayer;

public abstract class DataPlayerManager<PData extends DataPlayer> extends PlayerManager<PData>
{
	public static interface OfflineDataCallback<PData>
	{
		public void onLoad(PData data, boolean offline);
	}

	public DataPlayerManager(JavaPlugin plugin, String name)
	{
		super(plugin, name);
	}

	@Override
	public void onEnable()
	{
		super.onEnable();

		for (Player player : getData().keySet())
		{
			PData data = getPlayer(player);

			try
			{
				data.attemptCreate();
				data.load();
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}

			data.setLoaded();
		}
	}

	@Override
	public void onDisable()
	{
		synchronized (getLock())
		{
			for (Player player : getData().keySet())
			{
				PData data = getPlayer(player);

				try
				{
					data.attemptCreate();
					data.save();
				} catch (Exception ex)
				{
					ex.printStackTrace();
				}
			}
		}

		super.onDisable();
	}

	public boolean loadOfflineData(final String name, final OfflineDataCallback<PData> callback) throws Exception
	{
		PData local = getPlayer(name);
		if (local != null)
		{
			callback.onLoad(local, false);
			return true;
		}

		UUID uuid = null;

		try
		{
			uuid = UniqueIdFetcher.getUUIDOf(name);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}

		if (uuid == null)
			return false;

		PData data = newOfflineData(uuid);

		if (data == null)
			return false;

		try
		{
			data.attemptCreate();
			data.load();
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}

		data.setLoaded();

		callback.onLoad(data, true);
		return true;
	}

	@Override
	@EventHandler(priority = EventPriority.LOWEST)
	public void onLogin(PlayerLoginEvent event)
	{
		PData data = newData(event.getPlayer());

		try
		{
			data.attemptCreate();
			data.load();
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}

		data.setLoaded();

		synchronized (getLock())
		{
			getData().put(event.getPlayer(), data);
		}
	}

	@Override
	@EventHandler(priority = EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent event)
	{
		synchronized (getLock())
		{
			PData data = getData().remove(event.getPlayer());

			try
			{
				data.attemptCreate();
				data.save();
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	protected abstract PData newOfflineData(UUID uuid);
}