package me.mysticate.minefinity.core.command;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.rank.Rank;

public abstract class CommandBase<M extends Manager>
{
	private CommandManager _commandManager;
	private M _manager;

	private String[] _aliases = new String[0];

	private Rank _rank = Rank.DEFAULT;

	private String _help;

	private boolean _invisible = false;
	private boolean _pass = false;

	public CommandBase(CommandManager commandManager, M manager, String[] aliases, Rank rank, String help)
	{
		_commandManager = commandManager;
		_manager = manager;

		_aliases = aliases;

		_rank = rank;

		_help = help;
	}

	public CommandManager getCommandManager()
	{
		return _commandManager;
	}

	public M getManager()
	{
		return _manager;
	}

	public String[] getAliases()
	{
		return _aliases;
	}

	public Rank getRank()
	{
		return _rank;
	}

	public String getHelp()
	{
		return _help;
	}

	public boolean isInvisible()
	{
		return _invisible;
	}

	protected void setInvisible(boolean invisible)
	{
		_invisible = invisible;
	}

	public boolean isPass()
	{
		return _pass;
	}

	protected void setPass(boolean pass)
	{
		_pass = pass;
	}

	public void sendMessage(String manager, Player player, String message)
	{
		player.sendMessage(F.manager(manager, message));
	}

	public void sendMessage(Player player, String message)
	{
		sendMessage(getManager().getName(), player, message);
	}

	public abstract boolean execute(Player player, String alias, String[] args);
}
