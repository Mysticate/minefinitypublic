package me.mysticate.minefinity.core.command;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.command.command.CommandRestrict;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.core.rank.RankManager;

public class CommandManager extends Manager
{
	private RankManager _rankManager;

	private Set<CommandBase<? extends Manager>> _commands = new HashSet<CommandBase<? extends Manager>>();
	private Set<String> _restricted = new HashSet<String>();

	public CommandManager(JavaPlugin plugin)
	{
		super(plugin, "Command");

		IM.add(this);
	}

	@Override
	public void registerCommands(CommandManager manager)
	{
		registerCommand(new CommandRestrict(this));
	}

	public void setRankManager(RankManager rankManager)
	{
		_rankManager = rankManager;
	}

	public RankManager getRankManager()
	{
		return _rankManager;
	}

	public void registerCommand(CommandBase<? extends Manager> base)
	{
		_commands.add(base);
	}

	public void restrictCommand(String command)
	{
		_restricted.add(command.trim());
	}

	public boolean isRestricted(String command)
	{
		command.trim();
		for (String restricted : _restricted)
		{
			if (restricted.equalsIgnoreCase(command))
				return true;
		}

		return false;
	}

	public void unRestrictCommand(String command)
	{
		command.trim();
		for (String restricted : new HashSet<String>(_restricted))
		{
			if (restricted.equalsIgnoreCase(command))
				_restricted.remove(restricted);
		}
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event)
	{
		event.setCancelled(true);

		Player player = event.getPlayer();

		String[] parts = event.getMessage().split(" ");

		String cmd = parts[0].substring(1);
		String[] args = new String[parts.length - 1];

		for (int i = 0; i < args.length; i++)
			args[i] = parts[i + 1];

		String fkCmd = new String(cmd);
		fkCmd.toLowerCase();
		if (fkCmd.startsWith("minecraft:") || fkCmd.startsWith("bukkit:") || fkCmd.startsWith("me")
				|| fkCmd.startsWith("say"))
		{
			sendMessage(player, "Once, little Timmy tried to do that.");
			return;
		}

		for (String restricted : _restricted)
		{
			if (cmd.equalsIgnoreCase(restricted) && !_rankManager.isOwned(player, Rank.OWNER))
			{
				sendMessage(event.getPlayer(), C.red + "That command has been restricted!");
				return;
			}
		}

		for (CommandBase<? extends Manager> command : _commands)
		{
			for (String alias : command.getAliases())
			{
				if (!cmd.equalsIgnoreCase(alias))
					continue;

				if (!_rankManager.isOwned(player, command.getRank(), !command.isInvisible()))
				{
					if (command.isInvisible())
						event.setCancelled(false);

					return;
				}

				if (!command.execute(player, alias, args))
				{
					if (command.getHelp() != null)
					{
						player.sendMessage(F.manager("Help", C.red + "/" + alias + " " + command.getHelp()));
					} else
					{
						command.sendMessage(player, C.red + "Failed to execute that command.");
					}
				} else
				{
					if (command.isPass())
						event.setCancelled(false);
				}

				return;
			}
		}

		event.setCancelled(false);
	}
}
