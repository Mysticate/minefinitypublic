package me.mysticate.minefinity.core.command.command;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandRestrict extends CommandBase<CommandManager>
{
	public CommandRestrict(CommandManager commandManager)
	{
		super(commandManager, commandManager,
				new String[] { "restrictcommand", "removecommand", "restrict", "command" }, Rank.OWNER,
				"/restrict <Command>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length == 0)
			return false;

		String command = args[0];

		if (getManager().isRestricted(command))
		{
			getManager().unRestrictCommand(command);
			sendMessage(player, "You unrestricted " + C.yellow + command + C.gray + "!");
		} else
		{
			getManager().restrictCommand(command);
			sendMessage(player, "You restricted " + C.yellow + command + C.gray + "!");
		}

		return true;
	}
}
