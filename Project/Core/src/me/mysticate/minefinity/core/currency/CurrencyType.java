package me.mysticate.minefinity.core.currency;

import org.bukkit.ChatColor;

public enum CurrencyType
{
	IRON("Iron", ChatColor.GRAY), GOLD("Gold", ChatColor.YELLOW), GEMS("Gems", ChatColor.RED);

	private String _name;
	private ChatColor _color;

	private CurrencyType(String name, ChatColor color)
	{
		_name = name;
		_color = color;
	}

	public String getName()
	{
		return _name;
	}

	public ChatColor getColor()
	{
		return _color;
	}

	public String getFormatted()
	{
		return getColor() + getName();
	}

	public static CurrencyType getByName(String name)
	{
		for (CurrencyType type : values())
		{
			if (type.getName().equalsIgnoreCase(name))
				return type;
		}

		return null;
	}
}
