package me.mysticate.minefinity.core.currency;

import java.util.HashMap;
import java.util.UUID;

import me.mysticate.minefinity.core.file.DataFolder;
import me.mysticate.minefinity.core.player.DataPlayer;

public class CurrencyPlayer extends DataPlayer
{
	private HashMap<CurrencyType, Integer> _currency = new HashMap<CurrencyType, Integer>();

	public CurrencyPlayer(UUID uuid)
	{
		super(uuid, DataFolder.getCurrency(uuid));
	}

	public boolean canAfford(CurrencyType type, int amount)
	{
		return getCurrency(type) >= amount;
	}

	public int getCurrency(CurrencyType type)
	{
		if (!_currency.containsKey(type))
			return 0;

		return _currency.get(type);
	}

	public void setCurrency(CurrencyType type, int amount)
	{
		_currency.put(type, amount);
	}

	public void addCurrency(CurrencyType type, int amount)
	{
		setCurrency(type, getCurrency(type) + amount);
	}

	public boolean removeCurrency(CurrencyType type, int amount)
	{
		if (!canAfford(type, amount))
			return false;

		addCurrency(type, -amount);
		return true;
	}

	@Override
	public void load()
	{
		_currency.put(CurrencyType.IRON, Integer.valueOf(read("money", "0")));
		_currency.put(CurrencyType.GOLD, Integer.valueOf(read("tokens", "0")));
		_currency.put(CurrencyType.GEMS, Integer.valueOf(read("infinium", "0")));
	}

	@Override
	public void save()
	{
		write(getCurrency(CurrencyType.IRON) + "", "money");
		write(getCurrency(CurrencyType.GOLD) + "", "tokens");
		write(getCurrency(CurrencyType.GEMS) + "", "infinium");
	}
}
