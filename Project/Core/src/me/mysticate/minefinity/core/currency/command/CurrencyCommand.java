package me.mysticate.minefinity.core.currency.command;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.DataPlayerManager.OfflineDataCallback;
import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.currency.CurrencyManager;
import me.mysticate.minefinity.core.currency.CurrencyPlayer;
import me.mysticate.minefinity.core.currency.CurrencyType;
import me.mysticate.minefinity.core.rank.Rank;

public class CurrencyCommand extends CommandBase<CurrencyManager>
{
	public CurrencyCommand(CommandManager commandManager, CurrencyManager manager)
	{
		super(commandManager, manager, new String[] { "currency" }, Rank.ADMIN, "<Player> <Currency> <Amount>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length != 3)
		{
			return false;
		}

		String playerName = args[0];

		CurrencyType currency = CurrencyType.getByName(args[1]);
		if (currency == null)
		{
			sendMessage(player, C.redBold + "Invalid currency!");
			return true;
		}

		int a = 0;
		try
		{
			a = Integer.valueOf(args[2]);
		} catch (IllegalArgumentException ex)
		{
			sendMessage(player, C.redBold + "Invalid amount");
			return true;
		}

		final int amount = a;

		sendMessage(player, "Processing request...");

		try
		{
			if (!getManager().loadOfflineData(playerName, new OfflineDataCallback<CurrencyPlayer>()
			{
				@Override
				public void onLoad(CurrencyPlayer data, boolean offline)
				{
					data.addCurrency(currency, amount);

					sendMessage(player, "You gave " + amount + " " + currency.getColor() + currency.getName() + C.gray
							+ " to " + C.yellow + playerName);

					if (offline)
					{
						data.save();
					} else
					{
						sendMessage(Bukkit.getPlayer(playerName), "You recieved " + amount + " " + currency.getColor()
								+ currency.getName() + C.gray + " from " + C.yellow + player.getName() + C.gray + ".");
					}
				}
			}))
			{
				sendMessage(player, "Unknown player " + C.yellow + playerName + C.gray + ".");
			}
		} catch (Exception ex)
		{
			sendMessage(player, C.red + "An error occured while processing your request. Is that the correct name?");
		}

		return true;
	}
}
