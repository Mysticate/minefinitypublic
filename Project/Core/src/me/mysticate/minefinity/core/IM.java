package me.mysticate.minefinity.core;

import java.util.HashMap;

import me.mysticate.minefinity.core.backpack.BackpackManager;
import me.mysticate.minefinity.core.chat.ChatManager;
import me.mysticate.minefinity.core.combat.PlayerCombatManager;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.cooldown.CooldownManager;
import me.mysticate.minefinity.core.currency.CurrencyManager;
import me.mysticate.minefinity.core.energy.EnergyManager;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.item.ItemManager;
import me.mysticate.minefinity.core.loadout.LoadoutManager;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.punish.PunishManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.core.settings.SettingsManager;
import me.mysticate.minefinity.core.world.WorldManager;

public class IM
{
	private static HashMap<String, Manager> _instances = new HashMap<String, Manager>();

	public static void add(Manager manager)
	{
		_instances.put(manager.getName(), manager);
	}

	public static void remove(Manager manager)
	{
		_instances.remove(manager.getName());
	}

	public static Manager getInstance(String name)
	{
		return _instances.get(name);
	}

	@SuppressWarnings("unchecked")
	private static <C extends Manager> C fish(String name)
	{
		try
		{
			return (C) getInstance(name);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	public static BackpackManager backpack()
	{
		return fish("Backpack");
	}

	public static ChatManager chat()
	{
		return fish("Chat");
	}

	public static PlayerCombatManager combat()
	{
		return fish("Combat");
	}

	public static CommandManager command()
	{
		return fish("Command");
	}

	public static CooldownManager cooldown()
	{
		return fish("Cooldown");
	}

	public static CurrencyManager currency()
	{
		return fish("Currency");
	}

	public static EnergyManager energy()
	{
		return fish("Energy");
	}

	public static HudManager hud()
	{
		return fish("Hud");
	}

	public static ItemManager item()
	{
		return fish("Item");
	}

	public static LoadoutManager loadout()
	{
		return fish("Loadout");
	}

	public static PerkManager perk()
	{
		return fish("Perk");
	}

	public static PunishManager punish()
	{
		return fish("Punish");
	}

	public static RankManager rank()
	{
		return fish("Rank");
	}

	public static SectorManager sector()
	{
		return fish("Sector");
	}

	public static SettingsManager settings()
	{
		return fish("Settings");
	}

	public static WorldManager world()
	{
		return fish("World");
	}
}
