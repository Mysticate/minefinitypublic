package me.mysticate.minefinity.core.teleport;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.Manager;

public class TeleportManager extends Manager
{
	private Location _join = null;

	public TeleportManager(JavaPlugin plugin)
	{
		super(plugin, "Teleport");
	}

	public void setJoinLocation(Location loc)
	{
		_join = loc;
	}

	public Location getJoinLocation()
	{
		return _join;
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onJoin(PlayerJoinEvent event)
	{
		if (_join == null)
			return;

		event.getPlayer().teleport(_join);
	}

	public void teleportAll(Location loc)
	{
		for (Player player : Bukkit.getOnlinePlayers())
		{
			teleport(player, loc);
		}
	}

	public void teleport(Player player, Location location)
	{
		player.eject();
		player.teleport(location);
	}

	public void teleportAll(Collection<Player> players, Location location)
	{
		for (Player player : players)
		{
			player.teleport(location);
		}
	}
}
