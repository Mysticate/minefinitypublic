package me.mysticate.minefinity.core.common.util;

import java.util.Set;
import java.util.UUID;

import org.bukkit.craftbukkit.v1_9_R1.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.metadata.FixedMetadataValue;

import me.mysticate.minefinity.MinefinityPlugin;
import net.minecraft.server.v1_9_R1.EntityInsentient;
import net.minecraft.server.v1_9_R1.EntityTypes;
import net.minecraft.server.v1_9_R1.PathfinderGoalSelector;

public class UtilEntity
{
	public static void tag(Entity ent, UUID uuid)
	{
		ent.setMetadata("owner", new FixedMetadataValue(MinefinityPlugin.getPlugin(), uuid));
	}

	public static UUID readTag(Entity ent)
	{
		if (ent.hasMetadata("owner"))
		{
			Object value = ent.getMetadata("owner").get(0).value();
			if (value == null || !(value instanceof UUID))
				return null;

			return (UUID) value;
		}
		return null;
	}

	public static void ignoreClear(Entity ent)
	{
		ent.setMetadata("ignoreClear", new FixedMetadataValue(MinefinityPlugin.getPlugin(), "Lmao"));
	}

	public static boolean isHologram(Entity ent)
	{
		return (ent instanceof ArmorStand) && (ent.hasMetadata("hologram"));
	}

	public static boolean canClear(Entity ent)
	{
		return !ent.hasMetadata("ignoreClear");
	}

	public static double getOffset(Entity a, Entity b)
	{
		return a.getLocation().distance(b.getLocation());
	}

	public static void clearGoals(Entity ent)
	{
		net.minecraft.server.v1_9_R1.Entity nmsEnt = ((CraftEntity) ent).getHandle();

		if (!(nmsEnt instanceof EntityInsentient))
			return;

		EntityInsentient entIns = (EntityInsentient) nmsEnt;
		PathfinderGoalSelector goals = entIns.goalSelector;
		PathfinderGoalSelector targets = entIns.targetSelector;

		try
		{
			((Set<?>) UtilReflection.getField(goals, "b")).clear();
			((Set<?>) UtilReflection.getField(goals, "c")).clear();

			((Set<?>) UtilReflection.getField(targets, "b")).clear();
			((Set<?>) UtilReflection.getField(targets, "c")).clear();
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void registerEntity(String name, Class<? extends net.minecraft.server.v1_9_R1.Entity> clazz)
	{
		if (EntityTypes.getName(clazz) != null)
			return;

		UtilReflection.callMethod(EntityTypes.class, "a", new Class<?>[] { Class.class, String.class, Integer.TYPE },
				new Object[] { clazz, name, getNewEntityId() });
	}

	public static int getNewEntityId()
	{
		int id = UtilMath.getRandom().nextInt();
		while (EntityTypes.a(id) != null)
			id = UtilMath.getRandom().nextInt();

		return id;
	}
}
