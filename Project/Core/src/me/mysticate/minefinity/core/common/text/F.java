package me.mysticate.minefinity.core.common.text;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.currency.CurrencyType;

public class F
{
	public static String manager(String name, String body)
	{
		return C.dGray + " # " + C.gold + name + C.dGray + " # " + C.gray + body;
	}

	public static String name(Player player)
	{
		return C.yellowBold + player.getName() + C.gray;
	}

	public static String trueFalse(boolean flag)
	{
		if (flag)
		{
			return C.greenBold + "True";
		} else
		{
			return C.redBold + "False";
		}
	}

	public static String onOff(boolean flag)
	{
		if (flag)
		{
			return C.greenBold + "On";
		} else
		{
			return C.redBold + "Off";
		}
	}

	public static String kick(String... lines)
	{
		String message = "";
		for (String line : lines)
			message += line + "\n";

		return message;
	}

	public static String perk(String... args)
	{
		String message = "";
		for (int i = 0; i < args.length; i++)
			message += args[i] + (i == args.length - 1 ? "" : "-");

		return message;
	}

	public static String string(Object obj)
	{
		return obj.toString();
	}

	public static String gamemode(GameMode mode)
	{
		return mode.toString().subSequence(0, 1) + mode.toString().substring(1).toLowerCase();
	}

	public static String[] cost(String name, int click, CurrencyType currency, int cost, int current)
	{
		String action = "";

		if (click == -1)
			action = "Left Click";
		if (click == 1)
			action = "Right Click";
		if (click == 0)
			action = "Click";

		return new String[] {
				(current >= cost ? (C.greenBold) : (C.redBold + C.strike)) + action + " to purchase" + C.white + " "
						+ name,
				C.aqua + "  Cost " + C.white + currency.getColor() + cost + " " + currency.getFormatted(),
				C.aqua + "  Current " + C.white + current, C.dAqua + "  New " + C.white + (current - cost) };
	}
}
