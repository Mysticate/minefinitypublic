package me.mysticate.minefinity.core.common.util;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import me.mysticate.minefinity.MinefinityPlugin;

public class UtilProjectile
{
	public static boolean wasShotWithBow(Projectile proj)
	{
		return proj instanceof Arrow && proj.hasMetadata("shotWithBow");
	}

	public static void setShotWithBow(Entity proj)
	{
		if (proj instanceof Arrow)
			proj.setMetadata("shotWithBow", new FixedMetadataValue(MinefinityPlugin.getPlugin(), "derp"));
	}

	public static ItemStack getAttachedItem(Entity ent)
	{
		if (ent.hasMetadata("attachedItem"))
		{
			Object obj = ent.getMetadata("attachedItem").get(0).value();
			if (obj instanceof ItemStack)
				return (ItemStack) obj;
		}
		return null;
	}

	public static void setAttachedItem(Entity ent, ItemStack item)
	{
		ent.setMetadata("attachedItem", new FixedMetadataValue(MinefinityPlugin.getPlugin(), item));
	}
}
