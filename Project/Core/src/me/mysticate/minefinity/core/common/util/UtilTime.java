package me.mysticate.minefinity.core.common.util;

public class UtilTime
{
	public static enum TimeUnit
	{
		Year(1000 * 60 * 60 * 24 * 30 * 12), 
		Month(1000 * 60 * 60 * 24 * 30 * -1), 
		Day(1000 * 60 * 60 * 24), 
		Hour(1000 * 60 * 60), 
		Minute(1000 * 60), 
		Second(1000), 
		Millisecond(1);

		private long _millis;

		private TimeUnit(long millis)
		{			
			_millis = millis;
		}

		public long getMillis()
		{
			return _millis;
		}
	}

	public static class TimeInfo
	{
		public TimeUnit Unit;
		public int Amount;

		public String display()
		{
			return Amount + " " + Unit.toString() + (Amount == 1 ? "" : "s");
		}
	}

	public static TimeInfo getLongestTime(long time)
	{
		for (TimeUnit unit : TimeUnit.values())
		{
			if (time >= unit.getMillis())
			{
				int amount = (int) Math.floor(time / unit.getMillis());
				
				TimeInfo info = new TimeInfo();
				info.Unit = unit;
				info.Amount = amount;
				return info;
			}
		}

		TimeInfo info = new TimeInfo();
		info.Unit = TimeUnit.Millisecond;
		info.Amount = (int) time;

		return info;
	}

	public static boolean hasPassed(long start, long time)
	{
		return System.currentTimeMillis() > start + time;
	}

	public static String fromHoursToString(int hours)
	{
		return fromMinutesToString(hours * 60);
	}

	public static String fromMinutesToString(int mins)
	{
		return fromSecsToString(mins * 60);
	}

	public static String fromSecsToString(int secs)
	{
		return fromSecsToString(secs, false);
	}

	public static String fromSecsToString(int secs, boolean shorthand)
	{
		final int hours = (int) Math.floor(secs / 3600);
		secs -= hours * 3600;
		final int minutes = (int) Math.floor(secs / 60);
		secs -= minutes * 60;
		final int seconds = (int) secs;

		if (hours > 0)
		{
			return hours + (shorthand ? "h" : (" Hour" + (hours == 1 ? "" : "s"))) + " " + minutes
					+ (shorthand ? "m" : (" Minute" + (minutes == 0 ? "" : "s")));
		} else if (minutes > 0)
		{
			return minutes + (shorthand ? "m" : (" Minute" + (minutes == 1 ? "" : "s"))) + " " + seconds
					+ (shorthand ? "s" : (" Second" + (seconds == 1 ? "" : "s")));
		} else
		{
			return seconds + (shorthand ? "s" : (" Second" + (seconds == 1 ? "" : "s")));
		}
	}
}
