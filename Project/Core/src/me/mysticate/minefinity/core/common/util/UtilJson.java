package me.mysticate.minefinity.core.common.util;

import net.minecraft.server.v1_9_R1.IChatBaseComponent;

public class UtilJson
{
	public static IChatBaseComponent buildComponent(String text)
	{
		return IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + text + "\"}");
	}
}
