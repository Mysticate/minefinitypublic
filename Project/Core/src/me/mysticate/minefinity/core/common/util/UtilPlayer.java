package me.mysticate.minefinity.core.common.util;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import me.mysticate.minefinity.core.hud.HudManager;
import net.minecraft.server.v1_9_R1.Packet;

public class UtilPlayer
{
	public static void addHealth(Player player, double health)
	{
		player.setHealth(Math.min(player.getMaxHealth(), Math.max(0, player.getHealth() + health)));
	}

	public static void setHealth(Player player, double health)
	{
		player.setHealth(Math.max(0, Math.min(health, player.getMaxHealth())));
	}

	public static void addFood(Player player, double food)
	{
		player.setFoodLevel((int) Math.min(20, Math.max(0, player.getFoodLevel() + food)));
	}

	public static void setFood(Player player, double food)
	{
		player.setFoodLevel((int) Math.max(0, Math.min(food, 20)));
	}

	public static void addSaturation(Player player, float sat)
	{
		player.setSaturation(Math.max(0, Math.min(1, player.getSaturation() + sat)));
	}

	public static void setSaturation(Player player, float sat)
	{
		player.setSaturation(Math.max(0, Math.min(1, sat)));
	}

	public static Player searchExact(String name)
	{
		for (Player player : Bukkit.getOnlinePlayers())
		{
			if (player.getName().equalsIgnoreCase(name))
			{
				return player;
			}
		}
		return null;
	}

	public static boolean validatePlayer(Player player)
	{
		if (player == null)
			return false;

		if (!player.isOnline())
			return false;

		return true;
	}

	public static void sendPacket(Player player, Packet<?>... packets)
	{
		for (Packet<?> packet : packets)
		{
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
	}

	public static void reset(Player player)
	{
		reset(player, true);
	}

	public static void reset(Player player, boolean clearVisuals)
	{
		if (clearVisuals)
		{
			HudManager.get(player).sendTitle("", "");

			HudManager.get(player).setPrefix("");
			HudManager.get(player).setSuffix("");

			HudManager.get(player).scoreboardBuild();
			HudManager.get(player).scoreboardBuild();
		}

		player.setGameMode(GameMode.SURVIVAL);

		player.setMaxHealth(20);
		player.setHealth(player.getMaxHealth());

		player.setSneaking(false);
		player.setFlying(false);
		player.setAllowFlight(false);

		player.leaveVehicle();
		player.eject();

		player.setLevel(0);
		player.setExp(0.0F);
		player.setFoodLevel(20);
		player.setLastDamageCause(null);

		player.setFireTicks(0);
		player.setExhaustion(1F);

		player.closeInventory();
		player.getInventory().setHeldItemSlot(0);

		((CraftPlayer) player).getHandle().o(0);

		player.setWalkSpeed(.2F);
		player.setFlySpeed(.1F);

		player.setVelocity(new Vector());

		UtilInv.clearInvAbs(player);
	}

	public static void clearChat(Player player)
	{
		for (int i = 0; i < 30; i++)
		{
			player.sendMessage(" ");
		}
	}

	public static Map<Player, Double> getNearby(Entity base, double radius)
	{
		Map<Player, Double> map = new HashMap<Player, Double>();
		for (Player player : base.getWorld().getPlayers())
		{
			double offset = UtilEntity.getOffset(base, player);
			if (offset > radius)
				continue;

			map.put(player, offset);
		}
		return map;
	}
}
