package me.mysticate.minefinity.core.common.misc;

public enum ArmorType
{
	HELMET(3),
	CHESTPLATE(2),
	LEGGINGS(1),
	BOOTS(0);
	
	private int _ind;
	
	private ArmorType(int ind)
	{
		_ind = ind;
	}
	
	public int getArmorIndex()
	{
		return _ind;
	}
	
	@Override
	public String toString()
	{
		return super.toString().toLowerCase();
	}
}
