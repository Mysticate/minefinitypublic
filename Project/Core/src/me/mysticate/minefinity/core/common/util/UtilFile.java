package me.mysticate.minefinity.core.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class UtilFile
{
	public static void copy(File source, File target)
	{
		try
		{
			if (source.isDirectory())
			{
				if (!target.exists())
					target.mkdirs();

				String[] files = source.list();
				for (String file : files)
				{
					File srcFile = new File(source, file);
					File destFile = new File(target, file);
					copy(srcFile, destFile);
				}
			} else
			{
				InputStream in = new FileInputStream(source);
				OutputStream out = new FileOutputStream(target);

				byte[] buffer = new byte[1024];

				int length;
				while ((length = in.read(buffer)) > 0)
					out.write(buffer, 0, length);

				in.close();
				out.close();
			}
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static boolean delete(File path)
	{
		if (path.exists())
		{
			File[] files = path.listFiles();
			for (File file : files)
			{
				if (file.isDirectory())
				{
					delete(file);
				} else
				{
					file.delete();
				}
			}
		}

		return path.delete();
	}
}
