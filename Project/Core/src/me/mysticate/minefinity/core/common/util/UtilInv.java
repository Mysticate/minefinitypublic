package me.mysticate.minefinity.core.common.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.MinefinityPlugin;

public class UtilInv
{
	public static void clearInvAbs(Entity ent)
	{
		if (!(ent instanceof Player))
			return;

		Player player = (Player) ent;

		player.getInventory().clear();
		player.setItemOnCursor(null);
		player.getInventory().setArmorContents(null);
	}

	public static void update(Entity en)
	{
		if (!(en instanceof Player))
			return;

		final Player player = (Player) en;

		Bukkit.getScheduler().scheduleSyncDelayedTask(MinefinityPlugin.getPlugin(), new Runnable()
		{
			@Override
			public void run()
			{
				if (UtilPlayer.validatePlayer(player))
				{
					player.updateInventory();
				}
			}
		}, 1);
	}
}
