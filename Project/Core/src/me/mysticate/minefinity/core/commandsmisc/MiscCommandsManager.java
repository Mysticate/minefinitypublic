package me.mysticate.minefinity.core.commandsmisc;

import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.commandsmisc.commands.CommandClearDrops;
import me.mysticate.minefinity.core.commandsmisc.commands.CommandGamemode;
import me.mysticate.minefinity.core.commandsmisc.commands.CommandKillEntities;
import me.mysticate.minefinity.core.commandsmisc.commands.CommandOp;
import me.mysticate.minefinity.core.commandsmisc.commands.CommandStop;

public class MiscCommandsManager extends Manager
{
	public MiscCommandsManager(JavaPlugin plugin)
	{
		super(plugin, "Overrides");
	}

	@Override
	public void registerCommands(CommandManager commandManager)
	{
		commandManager.registerCommand(new CommandGamemode(commandManager, this));
		commandManager.registerCommand(new CommandClearDrops(commandManager, this));
		commandManager.registerCommand(new CommandKillEntities(commandManager, this));
		commandManager.registerCommand(new CommandOp(commandManager, this));
		commandManager.registerCommand(new CommandStop(commandManager, this));
	}
}
