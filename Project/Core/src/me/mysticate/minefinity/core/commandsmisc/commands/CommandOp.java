package me.mysticate.minefinity.core.commandsmisc.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.commandsmisc.MiscCommandsManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandOp extends CommandBase<MiscCommandsManager>
{
	public CommandOp(CommandManager commandManager, MiscCommandsManager manager)
	{
		super(commandManager, manager, new String[] { "op", "operator" }, Rank.OWNER, null);
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		Player target = player;

		if (args.length >= 1)
			target = Bukkit.getPlayer(args[0]);

		if (target == null)
		{
			sendMessage(player, "Failed to locate player " + C.yellow + args[0]);
			return true;
		}

		if (target.isOp())
		{
			target.setOp(false);
		} else
		{
			target.setOp(true);
		}

		if (player == target)
		{
			sendMessage("OP", player, "Operator status set to " + F.trueFalse(target.isOp()));
		} else
		{
			sendMessage("OP", player, "You set " + C.yellow + target.getName() + "'s" + C.gray + " operator status to "
					+ F.trueFalse(target.isOp()));
			sendMessage("OP", target, "Your operator status was set to " + F.trueFalse(target.isOp()) + " by "
					+ C.yellow + player.getName() + ".");
		}

		return true;
	}
}
