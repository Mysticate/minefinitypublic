package me.mysticate.minefinity.core.hud;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.PlayerManager;
import me.mysticate.minefinity.core.hud.name.PrefixSuffixChangeEvent;

public class HudManager extends PlayerManager<HudPlayer>
{
	public static HudPlayer get(Player player)
	{
		return IM.hud().getPlayer(player);
	}

	public HudManager(JavaPlugin plugin)
	{
		super(plugin, "Hud");
	}

	@EventHandler
	public void onPrefixSuffix(PrefixSuffixChangeEvent event)
	{
		for (HudPlayer other : getData().values())
		{
			other.setPrefix(event.getModule().getPlayer(), event.getModule().getPrefix());
			other.setSuffix(event.getModule().getPlayer(), event.getModule().getSuffix());
		}
	}

	@Override
	protected HudPlayer newData(Player player)
	{
		HudPlayer hud = new HudPlayer(player.getUniqueId(), player);

		for (HudPlayer other : getData().values())
		{
			hud.setPrefix(other.getPlayer(), other.getPrefix());
			hud.setSuffix(other.getPlayer(), other.getSuffix());
		}

		return hud;
	}
}
