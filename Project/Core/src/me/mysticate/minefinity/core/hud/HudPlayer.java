package me.mysticate.minefinity.core.hud;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import me.mysticate.minefinity.core.hud.name.PrefixSuffixModule;
import me.mysticate.minefinity.core.hud.scoreboard.ScoreboardModule;
import me.mysticate.minefinity.core.hud.title.TitleModule;
import me.mysticate.minefinity.core.player.PlayerData;

public class HudPlayer extends PlayerData
{
	private Player _player;

	private ScoreboardModule _scoreboard;
	private TitleModule _title;
	private PrefixSuffixModule _prefixSuffix;

	public HudPlayer(UUID id, Player player)
	{
		super(id);

		_player = player;

		_scoreboard = new ScoreboardModule(_player);
		_title = new TitleModule(_player);
		_prefixSuffix = new PrefixSuffixModule(player, _scoreboard);
	}

	public Player getPlayer()
	{
		return _player;
	}

	public void scoreboardSetTitle(String title)
	{
		_scoreboard.setTitle(title);
	}

	public void scoreboardAddBlank()
	{
		_scoreboard.addBlank();
	}

	public void scoreboardAddText(String string)
	{
		_scoreboard.addText(string);
	}

	public void scoreboardBuild()
	{
		_scoreboard.buildBoard();
	}

	public Scoreboard getScoreboard()
	{
		return _scoreboard.getBoard();
	}

	public void sendTitle(String title, String subtitle)
	{
		_title.sendTitle(title, subtitle);
	}

	public void sendTitle(String title, String subtitle, int in, int stay, int out)
	{
		_title.sendTitle(title, subtitle, in, stay, out);
	}

	public void setHeader(String header)
	{
		_title.setHeader(header);
	}

	public void setFooter(String footer)
	{
		_title.setFooter(footer);
	}

	public void sendActionBar(String message)
	{
		_title.sendActionBar(message);
	}

	public void setPrefix(String prefix)
	{
		_prefixSuffix.setPrefix(prefix);
	}

	public void setPrefix(Player player, String prefix)
	{
		_prefixSuffix.setPrefix(player, prefix);
	}

	public String getPrefix()
	{
		return _prefixSuffix.getPrefix();
	}

	public void setSuffix(String suffix)
	{
		_prefixSuffix.setSuffix(suffix);
	}

	public void setSuffix(Player player, String suffix)
	{
		_prefixSuffix.setSuffix(player, suffix);
	}

	public String getSuffix()
	{
		return _prefixSuffix.getSuffix();
	}
}
