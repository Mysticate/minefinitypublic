package me.mysticate.minefinity.core.hud.name;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import me.mysticate.minefinity.core.hud.HudModule;
import me.mysticate.minefinity.core.hud.scoreboard.ScoreboardModule;

public class PrefixSuffixModule extends HudModule
{
	private ScoreboardModule _scoreboard;

	private String _prefix = "";
	private String _suffix = "";

	public PrefixSuffixModule(Player player, ScoreboardModule scoreboard)
	{
		super(player);

		_scoreboard = scoreboard;
	}

	private Team registerTeam(String name)
	{
		Team team = _scoreboard.getBoard().getTeam(name);
		if (team == null)
			team = _scoreboard.getBoard().registerNewTeam(name);

		return team;
	}

	@SuppressWarnings("deprecation")
	public void setPrefix(Player player, String prefix)
	{
		Team team = registerTeam(player.getName());

		if (!team.hasPlayer(player))
			team.addPlayer(player);

		prefix = new String(prefix).substring(0, Math.min(prefix.length(), 15));
		team.setPrefix(prefix);
	}

	@SuppressWarnings("deprecation")
	public void setSuffix(Player player, String suffix)
	{
		Team team = registerTeam(player.getName());

		if (!team.hasPlayer(player))
			team.addPlayer(player);

		suffix = new String(suffix).substring(0, Math.min(suffix.length(), 15));
		team.setSuffix(suffix);
	}

	public void setPrefix(String prefix)
	{
		_prefix = prefix;
		Bukkit.getPluginManager().callEvent(new PrefixSuffixChangeEvent(this));
	}

	public String getPrefix()
	{
		return _prefix;
	}

	public void setSuffix(String suffix)
	{
		_suffix = suffix;
		Bukkit.getPluginManager().callEvent(new PrefixSuffixChangeEvent(this));
	}

	public String getSuffix()
	{
		return _suffix;
	}
}
