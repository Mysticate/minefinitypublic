package me.mysticate.minefinity.core.hud.scoreboard;

import java.util.ArrayList;
import java.util.LinkedList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.hud.HudModule;

public class ScoreboardModule extends HudModule
{
	private Scoreboard _board;
	private Objective _side;

	private Team[] _teams = new Team[16];
	private String[] _teamNames = new String[] { C.green, C.aqua, C.red, C.purple, C.yellow, C.white, C.blue, C.dGreen,
			C.dAqua, C.dRed, C.dPurple, C.gold, C.gray, C.dGray, C.dBlue, C.black };

	private final String _title = C.dPurpleBold + "      MINEFINITY      ";

	private String _nextTitle = _title;
	private LinkedList<BoardEntry> _lines = new LinkedList<BoardEntry>();

	public ScoreboardModule(Player player)
	{
		super(player);

		_board = Bukkit.getScoreboardManager().getNewScoreboard();
		_side = _board.registerNewObjective("Side", "dummy");
		_side.setDisplaySlot(DisplaySlot.SIDEBAR);
		_side.setDisplayName(_title);

		for (int i = 15; i >= 0; i--)
		{
			Team team = _board.registerNewTeam("Side " + i);
			team.addEntry(_teamNames[i]);

			_teams[i] = team;
		}
	}

	public Scoreboard getBoard()
	{
		return _board;
	}

	public void setTitle(String title)
	{
		_nextTitle = title;
	}

	public void addBlank()
	{
		_lines.add(new BoardEntry(""));
	}

	public void addText(String name)
	{
		_lines.add(new BoardEntry(name));
	}

	public void buildBoard()
	{
		LinkedList<BoardEntry> ordered = new LinkedList<BoardEntry>();
		ArrayList<Team> used = new ArrayList<Team>();

		for (BoardEntry entry : _lines)
		{
			if (ordered.contains(entry))
				continue;

			ordered.addLast(entry);
		}

		for (int i = 15; i >= 0; i--)
		{
			if (ordered.isEmpty())
				break;

			Team team = _teams[i];

			_side.getScore(_teamNames[i]).setScore(i);

			BoardEntry entry = ordered.removeFirst();

			team.setPrefix(entry.getPrefix());
			team.setSuffix(entry.getSuffix());

			used.add(team);
		}

		_side.setDisplayName(_nextTitle);

		for (Team team : _teams)
		{
			if (used.contains(team))
				continue;

			team.setPrefix("");
			team.setSuffix("");

			for (String entry : team.getEntries())
				_board.resetScores(entry);
		}

		_lines.clear();
		_nextTitle = _title;

		if (getPlayer().getScoreboard() != _board)
			getPlayer().setScoreboard(_board);
	}
}
