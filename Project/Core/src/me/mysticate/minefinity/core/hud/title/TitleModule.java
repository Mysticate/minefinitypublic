package me.mysticate.minefinity.core.hud.title;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.common.util.UtilJson;
import me.mysticate.minefinity.core.common.util.UtilPlayer;
import me.mysticate.minefinity.core.common.util.UtilReflection;
import me.mysticate.minefinity.core.hud.HudModule;
import net.minecraft.server.v1_9_R1.Packet;
import net.minecraft.server.v1_9_R1.PacketPlayOutChat;
import net.minecraft.server.v1_9_R1.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle.EnumTitleAction;

public class TitleModule extends HudModule
{
	private String _header;
	private String _footer;

	public TitleModule(Player player)
	{
		super(player);
	}

	public void sendTitle(String title, String subtitle)
	{
		sendTitle(title, subtitle, 20, 60, 20);
	}

	public void sendTitle(String title, String subtitle, int in, int stay, int out)
	{
		UtilPlayer.sendPacket(getPlayer(),
				new Packet<?>[] { new PacketPlayOutTitle(EnumTitleAction.TITLE, UtilJson.buildComponent(title), in,
						stay, out),
				new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, UtilJson.buildComponent(subtitle), in, stay, out) });
	}

	public void setHeader(String header)
	{
		_header = header;
		redisplayTab();
	}

	public void setFooter(String footer)
	{
		_footer = footer;
		redisplayTab();
	}

	private void redisplayTab()
	{
		PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();

		UtilReflection.setField(packet, "a", UtilJson.buildComponent(_header));
		UtilReflection.setField(packet, "b", UtilJson.buildComponent(_footer));

		UtilPlayer.sendPacket(getPlayer(), packet);
	}

	public void sendActionBar(String message)
	{
		UtilPlayer.sendPacket(getPlayer(), new PacketPlayOutChat(UtilJson.buildComponent(message), (byte) 2));
	}
}
