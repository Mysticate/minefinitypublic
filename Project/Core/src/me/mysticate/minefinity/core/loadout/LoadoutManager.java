package me.mysticate.minefinity.core.loadout;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.DataPlayerManager;

public class LoadoutManager extends DataPlayerManager<LoadoutPlayer>
{
	public LoadoutManager(JavaPlugin plugin)
	{
		super(plugin, "Loadout");
	}

	@Override
	protected LoadoutPlayer newData(Player player)
	{
		return new LoadoutPlayer(player.getUniqueId());
	}

	@Override
	protected LoadoutPlayer newOfflineData(UUID uuid)
	{
		return new LoadoutPlayer(uuid);
	}
}
