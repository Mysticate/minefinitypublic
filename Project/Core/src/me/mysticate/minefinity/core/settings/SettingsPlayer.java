package me.mysticate.minefinity.core.settings;

import java.util.HashMap;
import java.util.UUID;

import me.mysticate.minefinity.core.file.YamlF;
import me.mysticate.minefinity.core.player.DataPlayer;

public class SettingsPlayer extends DataPlayer
{
	private HashMap<SettingPool, HashMap<String, String>> _settings = new HashMap<SettingPool, HashMap<String, String>>();

	public SettingsPlayer(UUID id)
	{
		super(id, SettingsManager.getFilePath(id));
	}

	public void set(SettingPool pool, String key, String value)
	{
		if (!_settings.containsKey(pool))
			_settings.put(pool, new HashMap<String, String>());

		_settings.get(pool).put(key, value);
	}

	public boolean hasSetting(SettingPool pool, String key)
	{
		return _settings.containsKey(pool) && _settings.get(pool).containsKey(key)
				&& _settings.get(pool).get(key) != null;
	}

	public String getSetting(SettingPool pool, String key)
	{
		if (!hasSetting(pool, key))
			return null;

		return _settings.get(pool).get(key);
	}

	@Override
	public void load()
	{
		for (SettingPool pool : SettingPool.values())
		{
			for (String setting : readLines(pool.toString()))
			{
				String value = read(YamlF.path(pool.toString(), setting), null);
				if (value == null)
					continue;

				set(pool, setting, value);
			}
		}
	}

	@Override
	public void save()
	{
		for (SettingPool pool : _settings.keySet())
		{
			for (String setting : _settings.get(pool).keySet())
			{
				write(getSetting(pool, setting), YamlF.path(pool.toString(), setting));
			}
		}
	}
}
