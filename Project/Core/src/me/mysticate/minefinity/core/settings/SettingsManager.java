package me.mysticate.minefinity.core.settings;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.DataPlayerManager;
import me.mysticate.minefinity.core.file.DataFolder;
import me.mysticate.minefinity.core.file.FilePath;

public class SettingsManager extends DataPlayerManager<SettingsPlayer>
{
	private static final String _filePath = "settings";

	public SettingsManager(JavaPlugin plugin)
	{
		super(plugin, "Settings");
	}

	public static FilePath getFilePath(UUID uuid)
	{
		return new FilePath(DataFolder.home(uuid), _filePath);
	}

	@Override
	protected SettingsPlayer newData(Player player)
	{
		return new SettingsPlayer(player.getUniqueId());
	}

	@Override
	protected SettingsPlayer newOfflineData(UUID uuid)
	{
		return new SettingsPlayer(uuid);
	}
}
