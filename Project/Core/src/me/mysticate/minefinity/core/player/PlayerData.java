package me.mysticate.minefinity.core.player;

import java.util.UUID;

public abstract class PlayerData implements IPlayer
{
	private UUID _id;

	public PlayerData(UUID id)
	{
		_id = id;
	}

	public UUID getIdentifier()
	{
		return _id;
	}
}
