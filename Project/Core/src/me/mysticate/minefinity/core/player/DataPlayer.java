package me.mysticate.minefinity.core.player;

import java.util.UUID;

import me.mysticate.minefinity.core.file.ConfigData;
import me.mysticate.minefinity.core.file.FilePath;

public abstract class DataPlayer extends ConfigData<UUID> implements IPlayer
{
	public DataPlayer(UUID id, FilePath path)
	{
		super(id, path);
	}
}
