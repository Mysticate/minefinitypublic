package me.mysticate.minefinity;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.SpigotConfig;

import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.backpack.BackpackManager;
import me.mysticate.minefinity.core.chat.ChatManager;
import me.mysticate.minefinity.core.combat.PlayerCombatManager;
import me.mysticate.minefinity.core.combat.event.CustomDamageEvent;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.commandsmisc.MiscCommandsManager;
import me.mysticate.minefinity.core.common.util.UtilEntity;
import me.mysticate.minefinity.core.common.util.UtilInv;
import me.mysticate.minefinity.core.cooldown.CooldownManager;
import me.mysticate.minefinity.core.currency.CurrencyManager;
import me.mysticate.minefinity.core.energy.EnergyManager;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.item.ItemManager;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.punish.PunishManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.core.settings.SettingsManager;
import me.mysticate.minefinity.core.updater.UpdaterManager;
import me.mysticate.minefinity.core.world.WorldManager;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.hub.HubManager;

@SuppressWarnings("unused")
public class Main extends JavaPlugin implements Listener
{
	// Backend
	private UpdaterManager _updaterManager;
	private MiscCommandsManager _overrideManager;

	// World management
	private SectorManager _sectorManager;
	private WorldManager _worldManager;

	// Player data
	private PunishManager _punishManager;
	private RankManager _rankManager;
	private CurrencyManager _currencyManager;
	private PerkManager _perkManager;

	// Player management
	private HudManager _hudManager;
	private PlayerCombatManager _combatManager;
	private ItemManager _itemManager;
	private BackpackManager _backpackManager;
	private SettingsManager _settingManager;
	private EnergyManager _energyManager;

	// Player external interaction
	private ChatManager _chatManager;
	private CommandManager _commandManager;
	private CooldownManager _cooldownManager;

	// Sector management
	private HubManager _hubManager;
	private GameManager _gameManager;

	@Override
	public void onEnable()
	{
		SpigotConfig.restartScript = "./start.command";

		Bukkit.getPluginManager().registerEvents(this, this);
		MinefinityPlugin.initialize(this);

		_updaterManager = new UpdaterManager(this);

		_worldManager = new WorldManager(this);

		_commandManager = new CommandManager(this);
		_rankManager = new RankManager(this);
		_commandManager.setRankManager(_rankManager);

		_sectorManager = new SectorManager(this, _rankManager);

		_chatManager = new ChatManager(this, _rankManager);
		_cooldownManager = new CooldownManager(this);

		_overrideManager = new MiscCommandsManager(this);

		_punishManager = new PunishManager(this);
		_currencyManager = new CurrencyManager(this);
		_perkManager = new PerkManager(this);

		_hudManager = new HudManager(this);
		_combatManager = new PlayerCombatManager(this);
		_itemManager = new ItemManager(this);
		_backpackManager = new BackpackManager(this);
		_settingManager = new SettingsManager(this);
		_energyManager = new EnergyManager(this);

		_hubManager = new HubManager(this, _sectorManager, _itemManager, _worldManager, _rankManager, _currencyManager,
				_settingManager, _chatManager, _perkManager, _backpackManager);
		_gameManager = new GameManager(this, _sectorManager, _rankManager, _currencyManager, _perkManager,
				_worldManager, _itemManager, _backpackManager, _chatManager, _settingManager, _energyManager,
				_combatManager);
	}

	@Override
	public void onDisable()
	{
		Manager.killManagers();
	}

	@EventHandler
	public void onJoinMsg(PlayerJoinEvent event)
	{
		event.setJoinMessage(null);
		event.getPlayer().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16.0D);
		event.getPlayer().setCollidable(false);
	}

	@EventHandler
	public void onQuitMsg(PlayerQuitEvent event)
	{
		event.setQuitMessage(null);
		event.getPlayer().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16.0D);
		event.getPlayer().setCollidable(false);
	}

	@EventHandler
	public void onDeathMessage(PlayerDeathEvent event)
	{
		event.setDeathMessage(null);
		event.getEntity().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16.0D);
		event.getEntity().setCollidable(false);
	}

	@EventHandler
	public void onDamage(CustomDamageEvent event)
	{
		if (UtilEntity.isHologram(event.getDamagee()))
		{
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event)
	{
		event.setUseInteractedBlock(Result.DENY);
	}

	@EventHandler
	public void onClick(InventoryClickEvent event)
	{
		if (event.getSlot() == 40)
		{
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onSwap(PlayerSwapHandItemsEvent event)
	{
		event.setCancelled(true);
		UtilInv.update(event.getPlayer());
	}
}
