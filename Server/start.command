#!/bin/bash
cd "$( dirname "$0" )"

rm Spigot.jar
rm /plugins/Minefinity.jar

mkdir -p server-logs

java -Xmx1024M -jar Spigot.jar -o true

mkdir -p server-logs
mv logs/latest.log server-logs/serverLog_$(date +%F-%T).log

rm -r logs
rm help.yml
rm banned-ips.json
rm banned-players.json
rm usercache.json
rm commands.yml
rm ops.json
rm whitelist.json